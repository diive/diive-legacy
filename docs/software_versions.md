# Software Versions



## -indev

Basically a version that is **still actively developed** but already distributed to users, independent from version numbering. For example, version ```0.5.0-indev``` is still changed and changes are not documented with version numbering. If a bug is discovered in this version, the version name remains the same.

```-indev``` indicates that this version is actively developed and might change at any time.



## -alpha

**Frozen** version that is distributed to users. All changes to this version are indicated in version numbering. For example, if a bug occurrs in version ``0.5.0-alpha`` and this bug is removed, the new version will be ```0.5.1-alpha```. While working on the bug fix, it is possible a version ``0.5.1-indev`` is already distributed to users.

```-alpha``` indicates that new features might still be added to this version.



## -beta

**Frozen** version that is feature complete. The focus is on finding and removing bugs. For example, after bugs are removed from version ```0.5.0-beta```, the new version is ```0.5.1-beta```. If in this new version new bugs are found (which will surely happen…), then the new version becomes ```0.5.2-beta``` and so on.

``-beta`` indicates that the version is feature-complete and the focus is on bug fixing.



## -final

**Frozen** version that is feature complete and deemed stable, or let’s say stable enough, to be released to users. If no more major bugs are found in version ```0.5.2-beta```, then it is released as ```0.5.2-final```. However, it is still possible that new bugs are found in this version. In that case, a new version ```0.5.3-final``` is generated, which is potentially preceded by respective ```-alpha``` and ```-beta``` versions if further testing is necessary.



## The Version Number

The 1st number indicates a major release. It can contain new (major) features, changes and bug fixes. Potentially needs new Python packages to be installed.

The 2nd number indicates a minor release. It can contain new features, changes and bug fixes.

The 3rd number indicates a bug-fix release. It contains bug fixes only. Sometimes it might be necessary to introduce changes, which will be documented in the release notes.



