import gui.elements
import gui.plotfuncs
from gui import elements
from modboxes.eddycovariance._deprecated import _QualityControl_shared


class AddControls():

    def __init__(self, opt_stack, opt_frame, opt_layout, ref_stack):
        self.opt_stack = opt_stack
        self.opt_frame = opt_frame
        self.opt_layout = opt_layout
        self.ref_stack = ref_stack

        self.add_option()
        self.add_refinements()

    def add_option(self):
        self.opt_btn = elements.add_button_to_grid(grid_layout=self.opt_layout,
                                                   txt='u* Threshold (Manual)', css_id='',
                                                   row=3, col=0, rowspan=1, colspan=1)
        self.opt_stack.addWidget(self.opt_frame)

    def add_refinements(self):
        ref_frame, ref_layout = elements.add_frame_grid()
        self.ref_stack.addWidget(ref_frame)

        gui.elements.add_header_to_grid_top(layout=ref_layout, txt='QC: u* Threshold (Manual)')

        self.ref_drp_ustar_col = elements.grd_LabelDropdownPair(txt='u*',
                                                                css_ids=['', ''],
                                                                layout=ref_layout,
                                                                row=1, col=0,
                                                                orientation='horiz')

        self.ref_lne_upper_lim = elements.add_label_linedit_pair_to_grid(txt='Upper Limit',
                                                                         css_ids=['', 'cyan'], layout=ref_layout,
                                                                         row=2, col=0, orientation='horiz')

        self.ref_lne_lower_lim = elements.add_label_linedit_pair_to_grid(txt='Lower Limit',
                                                                         css_ids=['', 'cyan'], layout=ref_layout,
                                                                         row=3, col=0, orientation='horiz')

        self.ref_btn_show_in_plot = elements.add_button_to_grid(grid_layout=ref_layout,
                                                                txt='Show in Plot', css_id='',
                                                                row=4, col=0, rowspan=1, colspan=2)
        ref_layout.setRowStretch(5, 1)


class Call:

    def __init__(self, data_df, drp_ustar, col_list_pretty, col_dict_tuples, fig, ax, focus_df, focus_col,
                 lower_lim, upper_lim):
        self.data_df = data_df
        self.drp_ustar = drp_ustar
        self.col_list_pretty = col_list_pretty
        self.col_dict_tuples = col_dict_tuples

        self.fig = fig  # needed for redraw
        self.ax = ax  # adds to existing axis
        self.focus_df = focus_df.copy()
        # self.focus_df = focus_df[[measured_col]].copy()
        self.focus_col = focus_col

        # Marker for values in the outlier range
        self.marker_col = ('qc_manual_ustar_threshold', 'marker')

        self.lower_lim = lower_lim
        self.upper_lim = upper_lim

        self.get_data()
        self.ax = gui.plotfuncs.remove_prev_lines(ax=self.ax)
        self.manual_ustar_threshold()

    def get_data(self):
        selected_colname_pretty_string = self.drp_ustar.currentText()

        # index in list of all variables
        selected_colname_pretty_string_ix = \
            self.col_list_pretty.index(selected_colname_pretty_string)

        # full column name (tuple), can be used to directly access data in df
        self.selected_colname_tuple = self.col_dict_tuples[selected_colname_pretty_string_ix]

        # data column containing the qc flag needs to be added to focus_df
        self.focus_df[self.selected_colname_tuple] = self.data_df[self.selected_colname_tuple]

        # new_current_class_colname = (current_class_colname[0] + '_class', current_class_colname[1])

    def manual_ustar_threshold(self):
        if self.lower_lim >= self.upper_lim:
            self.lower_lim = self.upper_lim

        # self.focus_df[self.flag_col] = self.upper_limit

        self.focus_df[self.marker_col] = (self.focus_df[self.selected_colname_tuple] >= self.lower_lim) & \
                                         (self.focus_df[self.selected_colname_tuple] <= self.upper_lim)
        self.color = '#e24d42'  # orange red
        _QualityControl_shared.show_in_plot(self=self)

    def get_results(self):
        return self.focus_df, self.marker_col
