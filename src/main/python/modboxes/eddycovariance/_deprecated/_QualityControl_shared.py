import pandas as pd

import modboxes.plots._shared


def show_in_plot(self):
    # time focus_series are added to an existing plot, no new axis

    plot_df = pd.DataFrame()
    plot_df[self.focus_col] = self.focus_df[self.focus_col]
    plot_df[self.inclass_flag_col] = self.focus_df[self.inclass_flag_col]
    plot_df['filtered'] = plot_df[self.focus_col][plot_df[self.inclass_flag_col]]

    # Marker points
    self.prev_line = modboxes.Plots._shared.add_to_existing_ax(self.fig, add_to_ax=self.axes_dict, x=plot_df.index, y=plot_df['filtered'],
                                                               linestyle='', linewidth=0)

