"""
Currently inactive, import does not work with fbs
https://github.com/mherrmann/fbs
"""
# import pandas_profiling as pp
# from gui import GUI_Elements
# from gui import plotfuncs
#
#
# class AddControls():
#     """
#         Creates the gui control elements and their handles for usage.
#
#     """
#
#     def __init__(self, opt_stack, opt_frame, opt_layout, ref_stack):
#         self.opt_stack = opt_stack
#         self.opt_frame = opt_frame
#         self.opt_layout = opt_layout
#         self.ref_stack = ref_stack
#
#         self.add_option()
#         self.add_refinements()
#         # self.get_handles()
#
#     def get_handles(self):
#         return self.btn_ex_opt_ProfileReport, \
#                self.btn_ex_ref_ProfileReport_export
#
#     def add_option(self):
#         # Option Button: [3] Running Median
#         self.btn_ex_opt_ProfileReport = GUI_Elements.grd_Button(lyt=self.opt_layout,
#                                                              txt='Profile Report (pandas)', css_id='',
#                                                              row=3, col=0, rowspan=1, colspan=1)
#
#
#         self.opt_stack.addWidget(self.opt_frame)
#
#     def add_refinements(self):
#         ref_frame, ref_layout = GUI_Elements.frame_grid()
#         self.ref_stack.addWidget(ref_frame)
#
#         ModBoxes_shared.add_header(layout=ref_layout, header='EX: Profile Report')
#
#         GUI_Elements.grd_Label(lyt=ref_layout,
#                                 txt=self.info_txt(), css_id='',
#                                 row=1, col=0, rowspan=1, colspan=1)
#
#         self.btn_ex_ref_ProfileReport_export = GUI_Elements.grd_Button(lyt=ref_layout,
#                                                              txt='Export', css_id='',
#                                                              row=2, col=0, rowspan=1, colspan=2)
#
#         ref_layout.setRowStretch(3, 1)
#
#         return self
#
#     def info_txt(self):
#         txt = """
#         Generates a HTML file with lots of stats. Might take a while, minutes for large files.\n
#         Generates a HTML file with lots of stats. Might take a while, minutes for large files.
#         """
#         return txt
#
#
# class Execute():
#
#     def __init__(self, df, dir_out, id, button):
#         self.df = df.copy()
#         self.dir_out = dir_out
#         self.id = id
#         self.button = button
#         self.button_orig_text = button.text()
#
#     def export(self):
#         filename_out = 'ProfileReport_{}.html'.format(self.id)
#         filepath_out = self.dir_out / filename_out
#
#         self.df.reset_index(inplace=True)
#         self.df.columns = [' '.join(col).strip() for col in self.df.columns.values]
#
#         GUI_Elements.btn_txt_live_update(btn=self.button, txt='Generating Report ...', perc=-9999)
#
#         report = pp.ProfileReport(self.df, )
#         report.to_file(filepath_out)
#
#         GUI_Elements.btn_txt_live_update(btn=self.button, txt=self.button_orig_text, perc=-9999)
