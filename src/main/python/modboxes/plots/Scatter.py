import matplotlib.gridspec as gridspec
import pandas as pd
from PyQt5 import QtGui
from PyQt5 import QtWidgets as qw
import gui
import gui.base
from .styles import LightTheme as theme


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SP')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_plots))

        # Add settings menu contents
        self.drp_x, self.drp_y, self.drp_z, self.drp_abs_vals, self.drp_norm_vals, \
        self.drp_display_z, self.btn_show_in_plot, self.drp_display_bins, self.drp_display_bin_model, \
        self.drp_display_fit_per = \
            self.add_settings_fields()

        # Add variables required in settings
        self.populate_settings_fields()

        # Add plots
        self.axes_dict = self.populate_plot_area()

    def populate_plot_area(self):
        return self.add_axes()

    def populate_settings_fields(self):
        self.drp_x.clear()
        self.drp_y.clear()
        self.drp_z.clear()

        for ix, col in enumerate(self.col_list_pretty):
            self.drp_x.addItem(self.col_list_pretty[ix])
            self.drp_y.addItem(self.col_list_pretty[ix])
            self.drp_z.addItem(self.col_list_pretty[ix])

    def add_axes(self):
        gs = gridspec.GridSpec(1, 25)  # rows, cols
        gs.update(wspace=0.2, hspace=0.2, left=0.03, right=0.96, top=0.96, bottom=0.03)
        ax_main = self.fig.add_subplot(gs[0, 0:24])
        ax_cbar = self.fig.add_subplot(gs[0, 24])
        axes_dict = {'ax_main': ax_main}
        for key, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)
        return axes_dict

    def add_settings_fields(self):
        # ---------------------------------------------------------------------------
        # Select data
        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt='Select Data', row=0)

        drp_x = gui.elements.grd_LabelDropdownPair_dropEnabled(txt='X',
                                                               css_ids=['', ''],
                                                               layout=self.sett_layout,
                                                               row=1, col=0,
                                                               orientation='horiz')

        drp_y = gui.elements.grd_LabelDropdownPair(txt='Y',
                                                   css_ids=['', ''],
                                                   layout=self.sett_layout,
                                                   row=2, col=0,
                                                   orientation='horiz')

        drp_z = gui.elements.grd_LabelDropdownPair(txt='Z',
                                                   css_ids=['', ''],
                                                   layout=self.sett_layout,
                                                   row=3, col=0,
                                                   orientation='horiz')

        self.sett_layout.addWidget(qw.QLabel(), 4, 0, 1, 1)  # spacer

        # ---------------------------------------------------------------------------
        # Conversions
        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt='Conversions', row=5)

        drp_abs_vals = gui.elements.grd_LabelDropdownPair(txt='Convert to Absolute Values',
                                                          css_ids=['', ''],
                                                          layout=self.sett_layout,
                                                          row=6, col=0,
                                                          orientation='horiz')
        drp_abs_vals.addItem('No')
        drp_abs_vals.addItem('Convert X and Y to Absolute Values')
        drp_abs_vals.addItem('Convert X to Absolute Values')
        drp_abs_vals.addItem('Convert Y to Absolute Values')

        drp_norm_vals = gui.elements.grd_LabelDropdownPair(txt='Normalization',
                                                           css_ids=['', ''],
                                                           layout=self.sett_layout,
                                                           row=7, col=0,
                                                           orientation='horiz')
        drp_norm_vals.addItem('No')
        drp_norm_vals.addItem('Convert X and Y to Percentile Values')
        drp_norm_vals.addItem('Convert X to Percentile Values')
        drp_norm_vals.addItem('Convert Y to Percentile Values')
        drp_norm_vals.addItem('Normalize X and Y to Maximum Value')
        drp_norm_vals.addItem('Normalize X to Maximum Value')
        drp_norm_vals.addItem('Normalize Y to Maximum Value')

        self.sett_layout.addWidget(qw.QLabel(), 8, 0, 1, 1)  # spacer

        # ---------------------------------------------------------------------------
        # Display
        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt='Display', row=8)

        drp_display_z = gui.elements.grd_LabelDropdownPair(txt='Z Values',
                                                           css_ids=['', ''],
                                                           layout=self.sett_layout,
                                                           row=9, col=0,
                                                           orientation='horiz')
        drp_display_z.addItem('No')
        drp_display_z.addItem('As Colors')
        drp_display_z.addItem('As Point Size')
        drp_display_z.addItem('As Colors and Point Size')

        drp_display_bins = gui.elements.grd_LabelDropdownPair(txt='Show Bins',
                                                              css_ids=['', ''],
                                                              layout=self.sett_layout,
                                                              row=10, col=0,
                                                              orientation='horiz')
        drp_display_bins.addItem('No')
        drp_display_bins.addItem('Yes')

        drp_display_bin_model = gui.elements.grd_LabelDropdownPair(txt='Fit Bin Model',
                                                                   css_ids=['', ''],
                                                                   layout=self.sett_layout,
                                                                   row=11, col=0,
                                                                   orientation='horiz')
        drp_display_bin_model.addItem('None')
        drp_display_bin_model.addItem('Linear Regression')
        drp_display_bin_model.addItem('2nd Order Polynomial Regression')
        drp_display_bin_model.addItem('3rd Order Polynomial Regression')
        drp_display_bin_model.addItem('4th Order Polynomial Regression')

        drp_display_fit_per = gui.elements.grd_LabelDropdownPair(txt='Fit Per',
                                                                 css_ids=['', ''],
                                                                 layout=self.sett_layout,
                                                                 row=12, col=0,
                                                                 orientation='horiz')
        drp_display_fit_per.addItem('Dataset')
        drp_display_fit_per.addItem('Year')
        drp_display_fit_per.addItem('Month')

        self.sett_layout.addWidget(qw.QLabel(), 13, 0, 1, 1)  # spacer

        # Show in plot
        btn_show_in_plot = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                           txt='Show in Plot', css_id='',
                                                           row=14, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(15, 1)

        return drp_x, drp_y, drp_z, drp_abs_vals, drp_norm_vals, drp_display_z, btn_show_in_plot, \
               drp_display_bins, drp_display_bin_model, drp_display_fit_per


class Run(addContent):
    sub_outdir = "plot_scatter"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        self.sub_outdir = self.project_outdir / self.sub_outdir

    def prepare_scatter_df(self):
        scatter_df, x_col, y_col, z_col = self.make_scatter_df()
        scatter_df = self.convert_to_abs(method=self.abs_vals, df=scatter_df, x=x_col, y=y_col)
        scatter_df = self.convert_normalize(method=self.norm_vals, df=scatter_df, x=x_col, y=y_col)
        return scatter_df, x_col, y_col, z_col

    def make_scatter_df(self):
        x_ix = self.drp_x.currentIndex()
        y_ix = self.drp_y.currentIndex()
        z_ix = self.drp_z.currentIndex()
        x_col = self.col_dict_tuples[x_ix]
        y_col = self.col_dict_tuples[y_ix]
        z_col = self.col_dict_tuples[z_ix]

        # Drop duplicate column names from df
        # Necessary in case e.g. x is the same as y
        plot_list = [x_col, y_col, z_col]
        plot_list_no_duplicates = []
        for ix, x in enumerate(plot_list):
            if plot_list[ix] in plot_list_no_duplicates:
                pass
            else:
                plot_list_no_duplicates.append(plot_list[ix])
        scatter_df = self.tab_data_df[plot_list_no_duplicates].copy()
        # # Elegant solution to check for duplicates in column index, but slow
        # # https://stackoverflow.com/questions/54373153/removing-duplicate-columns-from-a-pandas-dataframe
        # plot_df = plot_df.T.drop_duplicates().T
        scatter_df = scatter_df.dropna()
        return scatter_df, x_col, y_col, z_col

    def convert_to_abs(self, method, df, x, y):
        # ABSOLUTE VALUES
        if method == 'No':
            pass
        elif method == 'Convert X and Y to Absolute Values':
            df[x] = df[x].abs()
            df[y] = df[y].abs()
        elif method == 'Convert X to Absolute Values':
            df[x] = df[x].abs()
        elif method == 'Convert Y to Absolute Values':
            df[y] = df[y].abs()
        return df

    def convert_normalize(self, method, df, x, y):
        # NORMALIZATION
        x_max = df[x].max()
        y_max = df[y].max()
        if method == 'No':
            pass
        elif method == 'Convert X and Y to Percentile Values':
            # plot_df.loc[:, x_colname] = plot_df.loc[:, x_colname].rank(pct=True)  # will be in decimal form
            df[x] = df[x].rank(pct=True)  # will be in decimal form
            df[y] = df[y].rank(pct=True)
        elif method == 'Convert X to Percentile Values':
            df[x] = df[x].rank(pct=True)
        elif method == 'Convert Y to Percentile Values':
            df[y] = df[y].rank(pct=True)
        elif method == 'Normalize X and Y to Maximum Value':
            df[x] = df[x].divide(x_max)
            df[y] = df[y].divide(y_max)
        elif method == 'Normalize X to Maximum Value':
            df[x] = df[x].divide(x_max)
        elif method == 'Normalize Y to Maximum Value':
            df[y] = df[y].divide(y_max)
        return df

    def calculate_bins(self, df, x_col, y_col):
        x_bin_avg_df = predicted_col = predicted_results = -9999
        if self.display_bins == 'Yes':
            # https://stackoverflow.com/questions/45273731/binning-column-with-python-pandas
            # scatter_df['x_bin'], retbins = pd.cut(scatter_df[x_col], bins=50, retbins=True)

            # Sometimes there are less unique x values than bins
            if df[x_col].unique().__len__() >= 20:
                q = 20
            else:
                q = 1
            df['x_bin'] = pd.qcut(df[x_col], q=q, labels=False)  # Series

            # x_bin_avg_df = scatter_df.groupby('x_bin').mean()

            x_bin_avg_df = df.groupby('x_bin').agg({
                x_col: ['mean', 'count'],
                y_col: ['mean', 'count']
            })
            x_bin_avg_df.dropna(inplace=True)

            if self.display_bin_model == 'Linear Regression':
                x_bin_avg_df, predicted_col, predicted_results = \
                    pkgs.dfun.fits.fit_to_bins_linreg(df=x_bin_avg_df,
                                                      x_col=x_col,
                                                      y_col=y_col,
                                                      bin_col='mean')

            elif 'Polynomial Regression' in self.display_bin_model:
                degree = 2
                if '2nd Order' in self.display_bin_model:
                    degree = 2
                elif '3rd Order' in self.display_bin_model:
                    degree = 3
                elif '4th Order' in self.display_bin_model:
                    degree = 4
                x_bin_avg_df, predicted_col, predicted_results = \
                    pkgs.dfun.fits.fit_to_bins_polyreg(df=x_bin_avg_df,
                                                       x_col=x_col,
                                                       y_col=y_col,
                                                       bin_col='mean',
                                                       degree=degree)

        return x_bin_avg_df, predicted_col, predicted_results

    def limit_z(self, z, limit_type):
        # Limit z-values so it can be used to display meaningful sizes and colors
        z_limited = z.copy()
        z_q80 = z_limited.quantile(0.80)
        z_q20 = z_limited.quantile(0.20)
        z_limited.loc[z_limited > z_q80] = z_q80
        z_limited.loc[z_limited < z_q20] = z_q20

        if limit_type == 'sizes':
            z_limited = z_limited.abs() / z_limited.abs().max()
            z_limited = z_limited.multiply(100).add(5)

        elif limit_type == 'colors':
            z_min = z_limited.min()
            z_min_abs = abs(z_min)
            z_limited = z_limited.add(z_min_abs).add(1)

        return z_limited

    def plot_scatter(self):

        ax = self.axes_dict['ax_main']
        ax.clear()

        self.abs_vals = self.drp_abs_vals.currentText()
        self.display_bin_model = self.drp_display_bin_model.currentText()
        self.display_bins = self.drp_display_bins.currentText()
        self.display_fit_per = self.drp_display_fit_per.currentText()
        self.display_z = self.drp_display_z.currentText()
        self.norm_vals = self.drp_norm_vals.currentText()

        df, x_col, y_col, z_col = self.prepare_scatter_df()

        scatter_alpha = 0.3

        try:

            # Data as Series
            x = df[x_col]
            y = df[y_col]
            z = df[z_col]

            if self.display_z == 'No':
                ax.scatter(x, y, alpha=scatter_alpha, edgecolors='none', marker='o', s=20,
                           c=theme.COLOR_SCATTER_DEFAULT)

            elif self.display_z == 'As Point Size':
                z_limited_size = self.limit_z(z=z, limit_type='sizes')
                ax.scatter(x, y, alpha=scatter_alpha, edgecolors='none',
                           marker='o', s=z_limited_size, c=theme.COLOR_SCATTER_DEFAULT)

            elif self.display_z == 'As Colors':
                z_limited_colors = self.limit_z(z=z, limit_type='colors')
                ax.scatter(x, y, alpha=scatter_alpha, edgecolors='none',
                           marker='o', c=z_limited_colors, cmap='Spectral_r',
                           s=20)

            elif self.display_z == 'As Colors and Point Size':
                z_limited_colors = self.limit_z(z=z, limit_type='colors')
                z_limited_size = self.limit_z(z=z, limit_type='sizes')
                ax.scatter(x, y, alpha=scatter_alpha, edgecolors='none',
                           marker='o', c=z_limited_colors, cmap='Spectral_r', s=z_limited_size)

            # ------------------------------------------------------------------------------
            # Add bins to scatter
            if self.display_bins == 'Yes':

                month_12color_list = theme.colors_12(shade=300)
                year_24color_list = theme.colors_24()

                if self.display_fit_per == 'Dataset':
                    x_bin_avg_df, predicted_col, predicted_results = \
                        self.calculate_bins(df=df, x_col=x_col, y_col=y_col)

                    self.show_bin_scatter(x_bin_avg_df=x_bin_avg_df, x_col=x_col, y_col=y_col,
                                          color=month_12color_list[0], month_str='')

                    self.show_bin_fits(x_bin_avg_df=x_bin_avg_df, x_col=x_col, predicted_col=predicted_col,
                                       predicted_results=predicted_results, color=month_12color_list[0], month_str='')

                elif self.display_fit_per == 'Month':

                    month_color = -1
                    grouped_df = df.groupby(df.index.month)
                    for grp_key, grp_month in grouped_df:
                        month_color += 1
                        month_str = grp_month.index[0].strftime('%b')  # Month as str, e.g. Jan, Feb, ...
                        x_bin_avg_df, predicted_col, predicted_results = \
                            self.calculate_bins(df=grp_month, x_col=x_col, y_col=y_col)

                        self.show_bin_scatter(x_bin_avg_df=x_bin_avg_df, x_col=x_col, y_col=y_col,
                                              color=month_12color_list[month_color], month_str=month_str)

                        self.show_bin_fits(x_bin_avg_df=x_bin_avg_df, x_col=x_col, predicted_col=predicted_col,
                                           predicted_results=predicted_results, color=month_12color_list[month_color],
                                           month_str=month_str)

                        self.fig.canvas.draw()  ## needed to update plot with new data
                        self.fig.canvas.flush_events()

                elif self.display_fit_per == 'Year':

                    year_color = -1
                    grouped_df = df.groupby(df.index.year)
                    for grp_key, grp_month in grouped_df:
                        year_color += 1
                        month_str = grp_month.index[0].strftime('%Y')  # Month as str, e.g. Jan, Feb, ...
                        x_bin_avg_df, predicted_col, predicted_results = \
                            self.calculate_bins(df=grp_month, x_col=x_col, y_col=y_col)

                        self.show_bin_scatter(x_bin_avg_df=x_bin_avg_df, x_col=x_col, y_col=y_col,
                                              color=year_24color_list[year_color], month_str=month_str)

                        self.show_bin_fits(x_bin_avg_df=x_bin_avg_df, x_col=x_col, predicted_col=predicted_col,
                                           predicted_results=predicted_results, color=year_24color_list[year_color],
                                           month_str=month_str)

                        self.fig.canvas.draw()  ## needed to update plot with new data
                        self.fig.canvas.flush_events()

            gui.plotfuncs.default_format(ax=ax, txt_xlabel=x_col[0], txt_ylabel=y_col[0], txt_ylabel_units=y_col[1],
                                         label_color=theme.COLOR_TXT_LEGEND, fontsize=theme.FONTSIZE_LABELS_AXIS)
            gui.plotfuncs.default_grid(ax=ax)

            gui.plotfuncs.default_legend(ax=ax)
            if (y.min() < 0) & (y.max() > 0):
                ax.axhline(y=0, color=theme.COLOR_LINE_ZERO, ls='-', lw=1, alpha=1, zorder=100)
            self.fig.canvas.draw()  ## needed to update plot with new data

        except:
            pass

    def show_bin_fits(self, x_bin_avg_df, x_col, predicted_col, predicted_results, color, month_str):
        ax = self.axes_dict['ax_main']
        if self.display_bin_model != 'None':
            txt_sign = '' if predicted_results['intercept'] < 0 else '+'
            if self.display_bin_model == 'Linear Regression':
                label_txt = "{} bin fit    y = {:.5f}x {}{:.5f}    r2 = {:.3f}".format(
                    month_str, predicted_results['slope'], txt_sign,
                    predicted_results['intercept'], predicted_results['r2']
                )
            elif 'Polynomial Regression' in self.display_bin_model:
                label_txt = '{} bin fit    coefficients: {}    intercept: {}    r2: {:.3f}'.format(
                    month_str, predicted_results['slope'], predicted_results['intercept'],
                    predicted_results['r2']
                )
            else:
                label_txt = 'None'

            # Predicted bins
            ax.plot(x_bin_avg_df[x_col]['mean'],
                    x_bin_avg_df[predicted_col],
                    color=color, alpha=1, lw=theme.WIDTH_LINE_DEFAULT * 2, ls='--',
                    label=label_txt)

    def show_bin_scatter(self, x_bin_avg_df, x_col, y_col, color, month_str):
        bin_min_counts = x_bin_avg_df[x_col]['count'].min()
        bin_max_counts = x_bin_avg_df[x_col]['count'].max()
        ax = self.axes_dict['ax_main']
        ax.scatter(x=x_bin_avg_df[x_col]['mean'],
                   y=x_bin_avg_df[y_col]['mean'],
                   marker='s', color=color, alpha=0.8, s=35, edgecolors='black',
                   label="{} equally-sized bins: {}-{} values per bin".format(
                       month_str, bin_min_counts, bin_max_counts))
