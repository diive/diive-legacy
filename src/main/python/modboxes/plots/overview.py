import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from pandas.plotting import register_matplotlib_converters

from . import heatmap

pd.plotting.register_matplotlib_converters()  # Needed for time plotting

from logger import log

register_matplotlib_converters()

import gui
from .styles import LightTheme as theme


class Run:

    def __init__(self, fig, focus_df, focus_col, data_df, limit_plot_xlim, limit_timerange_filter_dt):

        self.data_df = data_df
        self.freq = self.data_df.index.freq  # todo freq
        self.fig = fig
        self.focus_col = focus_col
        self.limit_plot_xlim = limit_plot_xlim
        self.limit_timerange_filter_dt = limit_timerange_filter_dt

        # Get plot data
        self.plot_df = self.make_plot_df(focus_df=focus_df)

        log(name=f'>>> Generating Overview Plots for {focus_col}',
            dict={'Rows': f'{self.plot_df[focus_col].shape[0]}',
                  'Total Values / Not NaN':
                      f'{self.plot_df[focus_col].size} / {self.plot_df[focus_col].dropna().size}'},
            highlight=True)

        self.x = self.plot_df.index
        self.y = self.plot_df[focus_col]
        self.y_cumsum = self.y.cumsum()

        # plots
        self.gs = self.make_axes()
        self.make_plots()

    def make_plot_df(self, focus_df):
        plot_df = focus_df.copy()
        plot_df['HOUR'] = plot_df.index.hour
        return plot_df

    def make_plots(self):
        # Make plots

        try:
            self.timeseries_plot(ax=self.ax_main)
            self.cumulative(ax=self.ax2_cumsum)
            self.histogram(ax=self.ax3_histogram)
            self.heatmap(ax=self.ax4_heatmap)
            self.mean_plot(ax=self.ax7_daily_avg, period='daily')
            self.hourly_avg(ax=self.ax8_rainbow_diel_cycle_avg)
            self.fig.canvas.draw()  # update plot w/ only canvas.draw but NOT with pause() works w/o background flashing
        except:
            gui.plotfuncs.non_numeric_error(ax=self.ax_main)

    def make_axes(self):
        # Setup grid for multiple axes
        gs = gridspec.GridSpec(3, 5)  # rows, cols
        gs.update(wspace=0.2, hspace=0.2, left=0.04, right=0.97, top=0.96, bottom=0.05)

        self.ax_main = self.fig.add_subplot(gs[0:2, 0:4])
        self.ax4_heatmap = self.fig.add_subplot(gs[0:3, 4:5])

        self.ax2_cumsum = self.fig.add_subplot(gs[2:3, 0:1])
        self.ax7_daily_avg = self.fig.add_subplot(gs[2:3, 1:2])
        self.ax8_rainbow_diel_cycle_avg = self.fig.add_subplot(gs[2:3, 2:3])
        self.ax3_histogram = self.fig.add_subplot(gs[2:3, 3:4])
        return gs

    def timeseries_plot(self, ax):
        """
        Make line plot with dates for full timeseries.
        """

        ax.plot_date(x=self.x, y=self.y, alpha=1, ls='-',
                     zorder=99, label=self.focus_col,
                     marker=theme.TYPE_MARKER_DEFAULT,
                     color=theme.COLOR_LINE_DEFAULT,
                     lw=theme.WIDTH_LINE_DEFAULT,
                     markeredgecolor=theme.COLOR_MARKER_DEFAULT_EDGE,
                     ms=theme.SIZE_MARKER_DEFAULT)

        ymin = self.plot_df[self.focus_col].min()
        ymax = self.plot_df[self.focus_col].max()

        if self.limit_plot_xlim:
            ax.set_xlim(self.limit_plot_xlim[0], self.limit_plot_xlim[1])
        ax.set_ylim(ymin, ymax)

        if (ymin < 0) & (ymax > 0):
            ax.axhline(y=0, color=theme.COLOR_LINE_ZERO, ls='-', lw=theme.WIDTH_LINE_ZERO, alpha=1, zorder=100)

        gui.plotfuncs.default_format(ax=ax, txt_xlabel='Date',
                                     txt_ylabel=self.focus_col[0],
                                     txt_ylabel_units=self.focus_col[1])

        gui.plotfuncs.default_grid(ax=ax)

        # Events
        try:
            self.show_events(ax=ax)
        except IndexError:
            pass

        # Seasons
        try:
            self.show_seasons(ax=ax)
        except:
            pass

        gui.plotfuncs.nice_date_ticks(ax=ax, minticks=3, maxticks=40, which='x')

    def mean_plot_settings(self, period, mean_df):
        # Check which period
        if period == 'monthly':
            period_index = mean_df.index.month
            xlabel = 'Month'
            title = 'Monthly Average'

        elif period == 'weekly':
            period_index = mean_df.index.week
            xlabel = 'Week'
            title = 'Weekly Average'

        elif period == 'daily':
            period_index = mean_df.index.date
            xlabel = 'Date'
            title = 'Daily Average'

        else:
            period_index = mean_df.index.date
            xlabel = 'Date'
            title = 'Daily Average'

        return period_index, xlabel, title

    def mean_plot(self, ax, period):
        """
        Mean plot (monthly, weekly, daily)
        """
        # log(name=self.mean_plot.__name__, dict={}, highlight=False)

        # New columns for agg df
        mean_col = (self.focus_col[0], self.focus_col[1], 'mean')
        mean_plus_sd_col = (self.focus_col[0], self.focus_col[1], 'mean+std')
        mean_minus_sd_col = (self.focus_col[0], self.focus_col[1], 'mean-std')
        period_index_col = ('period_index', '', '')

        mean_df = pd.DataFrame(index=self.plot_df.index,
                               columns=[self.focus_col],
                               data=self.plot_df[self.focus_col].values)

        period_index, xlabel, title = self.mean_plot_settings(period=period, mean_df=mean_df)

        # Aggregate by month
        mean_df_agg = \
            mean_df.groupby(period_index).aggregate({
                self.focus_col: ['mean', 'std']
            })

        # Average +/- standard deviation
        mean_df_agg[mean_plus_sd_col] = \
            mean_df_agg[self.focus_col]['mean'] + mean_df_agg[self.focus_col]['std']
        mean_df_agg[mean_minus_sd_col] = \
            mean_df_agg[self.focus_col]['mean'] - mean_df_agg[self.focus_col]['std']

        # Plot mean
        mean_df_agg[period_index_col] = mean_df_agg.index
        mean_df_agg.plot.line(ax=ax, x=period_index_col, y=mean_col, lw=theme.WIDTH_LINE_DEFAULT,
                              ls='-', color=theme.COLOR_LINE_DEFAULT, legend=False)

        # Plot std upper and lower
        ax.fill_between(mean_df_agg['period_index'],
                        mean_df_agg[mean_plus_sd_col],
                        mean_df_agg[mean_minus_sd_col],
                        color=theme.COLOR_LINE_DEFAULT, alpha=0.1)

        gui.plotfuncs.default_format(ax=ax, txt_xlabel=xlabel,
                                     txt_ylabel=None,
                                     txt_ylabel_units=None)

        overall_minVal = mean_df_agg[[mean_col, mean_plus_sd_col, mean_minus_sd_col]].min().min()
        overall_maxVal = mean_df_agg[[mean_col, mean_plus_sd_col, mean_minus_sd_col]].max().max()
        ax.set_ylim(overall_minVal, overall_maxVal)

        if (overall_minVal < 0) & (overall_maxVal > 0):
            ax.axhline(y=0, color=theme.COLOR_LINE_ZERO, ls='-', lw=theme.WIDTH_LINE_ZERO, alpha=1,
                       zorder=100)  # light green 500

        if self.limit_plot_xlim:
            ax.set_xlim(self.limit_plot_xlim[0], self.limit_plot_xlim[1])

        ax.text(0.05, 0.95, title,
                horizontalalignment='left', verticalalignment='top', transform=ax.transAxes,
                size=theme.FONTSIZE_HEADER_AXIS, color=theme.COLOR_LINE_DEFAULT, backgroundcolor='none')

        gui.plotfuncs.nice_date_ticks(ax=ax, minticks=3, maxticks=10, which='x')

    def hourly_avg(self, ax):
        """
        Hourly average plots per month (rainbow)
        """

        if self.limit_plot_xlim:
            plot_df = self.plot_df.copy()
            plot_df = plot_df[self.limit_timerange_filter_dt]
        else:
            plot_df = self.plot_df.copy()

        unique_months = plot_df.index.month.unique()
        unique_hours = plot_df.index.hour.unique()

        plot_hourly_avg_df = pd.DataFrame(index=unique_hours)

        for month in unique_months:
            _subset_month = plot_df.loc[(plot_df.index.month == month)]
            _subset_month = _subset_month.groupby(_subset_month.index.hour).mean()
            plot_hourly_avg_df['M{:02d}'.format(month)] = _subset_month[self.focus_col]

        plot_hourly_avg_df.sort_index(axis=0, inplace=True)
        plot_hourly_avg_df.sort_index(axis=1, inplace=True)
        plot_hourly_avg_df.plot(ax=ax, color=theme.colors_12(shade=300), linestyle='-', lw=theme.WIDTH_LINE_DEFAULT)

        gui.plotfuncs.default_legend(ax=ax, loc='upper right', bbox_to_anchor=(1, 1),
                                     labelspacing=0.2)

        ax.set_xticks([3, 6, 9, 12, 15, 18, 21])
        ax.set_xlim(-1, 30)

        gui.plotfuncs.default_format(ax=ax, txt_xlabel='Hour',
                                     txt_ylabel=None,
                                     txt_ylabel_units=None)

        overall_minVal = plot_hourly_avg_df.min().min()
        overall_maxVal = plot_hourly_avg_df.max().max()
        ax.set_ylim(overall_minVal, overall_maxVal)

        ax.text(0.05, 0.95, 'Hourly Average', horizontalalignment='left', verticalalignment='top',
                transform=ax.transAxes, size=theme.FONTSIZE_HEADER_AXIS, color=theme.FONTCOLOR_HEADER_AXIS,
                backgroundcolor='none')

    def show_seasons(self, ax):
        """ Shows seasons in plot. """
        # colorlist = colorwheel_36()

        # Loop through season columns
        for col in self.data_df.columns:
            if col[1] == '[season#]':

                # Detect where new season starts
                season_df = self.data_df[[col]].copy()
                season_df['diff'] = season_df[col].diff()  # Difference of element with previous
                filter_change_point = season_df['diff'] != 0  # If season did not change, diff is zero
                season_df = season_df[filter_change_point].dropna()  # First element diff yields NaN

                # Iterate through season change points detected with diff
                for id, val in enumerate(season_df[col]):
                    season_start = season_df[col].index[id]
                    season_label = int(season_df[col][id])

                    # Lines showing season start
                    ax.axvline(season_start, ls='--', label='season start')  # Row name is the datetime index

                    # Add text labels
                    ax.text(season_start, 0.98, f' Season {season_label}',
                            horizontalalignment='left', verticalalignment='top',
                            transform=ax.get_xaxis_transform(),
                            size=theme.FONTSIZE_HEADER_AXIS, color='black', backgroundcolor='none', zorder=100)

        else:
            pass

    def show_events(self, ax):
        """
        Shows variables with unit [event] directly as range in plot

        The start and end dates of the event are used to plot a line on ax.
        """
        colorlist = theme.colorwheel_36()
        lst_event_cols = []
        for c in self.data_df.columns:
            if c[1] == '[event]':
                lst_event_cols.append(c)

        num_events = len(lst_event_cols)

        # TODO check Events import if column is all zeros
        if len(lst_event_cols) > 0:
            for ix, col in enumerate(lst_event_cols):
                colname = col[0]
                filter = self.data_df[col] == 1  # 1=Yes
                event_yes = self.data_df[col][filter]

                # ax.plot_date([event_yes.index[0], event_yes.index[-1]], [0.95, 0.95],
                #              zorder=99, label="XXX", c='red', alpha=0.5,
                #              ls='-', lw=20, marker='', markeredgecolor='none', ms='10',
                #              transform=ax.get_xaxis_transform())

                x_start = event_yes.index[0]
                x_end = event_yes.index[-1]
                x_width = x_end - x_start
                x_range = [(x_start, x_width)]
                x_range_middle = x_width / 2  # Position of Event text
                x_range_middle = x_start + x_range_middle
                y_range = (0, 1)

                # ax.axvline(x_start, color=colorlist[ix], ls='--')
                ax.broken_barh(x_range, y_range, facecolor=colorlist[ix], hatch='', lw=0,
                               transform=ax.get_xaxis_transform(), alpha=0.1, zorder=0)
                # transform=ax.get_xaxis_transform())

                ax.text(x_start, 1.05, '{}'.format(colname),
                        horizontalalignment='left', verticalalignment='top',
                        transform=ax.get_xaxis_transform(),
                        size=theme.FONTSIZE_HEADER_AXIS, color=colorlist[ix], backgroundcolor='none', zorder=100)
        else:
            pass

    def cumulative(self, ax):
        """ Cumulative plot """
        ax.plot_date(x=self.x, y=self.y_cumsum, color=theme.COLOR_LINE_CUMULATIVE, alpha=0.9, ls='-',
                     marker='', markeredgecolor='none', ms=0, zorder=99, label=self.focus_col,
                     lw=theme.WIDTH_LINE_DEFAULT)

        cumsum_min = self.y_cumsum.min()
        cumsum_max = self.y_cumsum.max()
        ax.set_ylim(cumsum_min, cumsum_max)

        if self.limit_plot_xlim:
            ax.set_xlim(self.limit_plot_xlim[0], self.limit_plot_xlim[1])

        if (cumsum_min < 0) & (cumsum_max > 0):
            ax.axhline(y=0, color=theme.COLOR_LINE_ZERO, ls='-', lw=theme.WIDTH_LINE_ZERO, alpha=1,
                       zorder=100)  # light green 500

        ax.text(0.05, 0.95, "Cumulative",
                horizontalalignment='left', verticalalignment='top', transform=ax.transAxes,
                size=theme.FONTSIZE_HEADER_AXIS, color=theme.COLOR_LINE_CUMULATIVE, backgroundcolor='none')

        ax.text(0.95, 0.1, "{:.1f}".format(self.y_cumsum[-1]),
                horizontalalignment='right', verticalalignment='bottom', transform=ax.transAxes,
                size=theme.FONTSIZE_LABELS_AXIS, color=theme.COLOR_LINE_CUMULATIVE, backgroundcolor='none')

        gui.plotfuncs.default_format(ax=ax, txt_xlabel='Date',
                                     txt_ylabel=None,
                                     txt_ylabel_units=None)

        gui.plotfuncs.nice_date_ticks(ax=ax, minticks=3, maxticks=10, which='x')

    def histogram(self, ax):
        """ Histogram plot """
        if self.limit_plot_xlim:
            plot_df = self.plot_df.copy()
            plot_df = plot_df[self.limit_timerange_filter_dt]
        else:
            plot_df = self.plot_df.copy()

        y = plot_df[self.focus_col]

        h = sns.histplot(y.dropna(), ax=ax, kde=True, color=theme.COLOR_HISTOGRAM, bins=30, stat='count')
        # h = sns.distplot(y.dropna(), ax=ax, kde=False, color=COLOR_HISTOGRAM, bins=25)
        ax.text(0.05, 0.95, "Histogram",
                horizontalalignment='left', verticalalignment='top', transform=ax.transAxes,
                size=theme.FONTSIZE_HEADER_AXIS, color=theme.COLOR_HISTOGRAM, backgroundcolor='none')

        _median = y.median()
        ax.axvline(_median, label=f"median: {_median:.2f}")
        _mean = y.mean()
        ax.axvline(_mean, label=f"mean: {_mean:.2f}", ls="--")

        # ylabel = '{} {}'.format(self.focus_col[0], self.focus_col[1])
        ylabel = 'counts'
        gui.plotfuncs.default_format(ax=ax, txt_xlabel='Bin',
                                     txt_ylabel=ylabel,
                                     txt_ylabel_units=None)
        gui.plotfuncs.default_legend(ax=ax)

    def heatmap(self, ax):
        """
        Heatmap plot
        """

        if self.limit_plot_xlim:
            plot_df = self.plot_df.copy()
            plot_df = plot_df[self.limit_timerange_filter_dt]
        else:
            plot_df = self.plot_df.copy()

        selected_col_data = plot_df[self.focus_col].copy()
        x, y, z, plot_df = heatmap.Run._prepare(selected_col_data, display_type='Time & Date')

        if not selected_col_data.empty:
            # colormap
            if np.nanmin(z) < 0 and np.nanmax(z) > 0:
                cmap_type = 'diverging'
                cmap = plt.get_cmap('RdYlBu_r')  # set colormap, see https://matplotlib.org/users/colormaps.html
            else:
                cmap_type = 'sequential'
                cmap = plt.get_cmap('GnBu')

            # z_numvals_unique = z[~np.isnan(z)]
            # z_numvals_unique = len(np.unique(z_numvals_unique))
            z_numvals_unique = len(plot_df['z'].unique())
            if z_numvals_unique > 100:
                vmin = plot_df['z'].quantile(.01)
                vmax = plot_df['z'].quantile(.99)
            else:
                vmin = plot_df['z'].quantile(0)
                vmax = plot_df['z'].quantile(1)
            heatmap.Run.plot_heatmap(x=x, y=y, z=z, ax=ax, cmap=cmap, color_bad='Grey',
                                     info_txt="", vmin=vmin, vmax=vmax, cb_digits_after_comma=1)

            ax.set_facecolor('white')
            ax.set_xticks(['6:00', '12:00', '18:00'])
            ax.set_xticklabels([6, 12, 18])

            gui.plotfuncs.nice_date_ticks(ax=ax, minticks=3, maxticks=7, which='y')
            gui.plotfuncs.default_format(ax=ax, txt_xlabel='Time')

            plt.tight_layout()

    def get_ax(self):
        return self.ax_main, self.gs
