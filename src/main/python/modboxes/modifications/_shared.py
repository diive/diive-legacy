import pandas as pd

import gui.plotfuncs
import modboxes.plots._shared


def show_in_plot(self):
    # added to an existing plot, todo needs second y-axis

    plot_df = pd.DataFrame()
    plot_df[self.focus_col] = self.focus_df[self.focus_col]
    plot_df[self.inclass_flag_col] = self.focus_df[self.inclass_flag_col]

    # Marker points
    self.prev_line = modboxes.Plots._shared.add_to_existing_ax(self.fig, add_to_ax=self.axes_dict, x=plot_df.index,
                                                               y=plot_df[self.inclass_flag_col], linestyle='--', linewidth=5)


def show_converted_in_plot(self, resampled_df, marker_col):
    # plot_df = pd.DataFrame()
    # plot_df[self.measured_col] = self.focus_df[self.measured_col]
    # plot_df[self.rejectval_col] = self.focus_df[self.rejectval_col]

    # Marker points
    prev_line = gui.plotfuncs.add_to_existing_ax_with_values(self.fig,
                                                             x=resampled_df.index,
                                                             y=resampled_df[marker_col],
                                                             add_to_ax=self.axes_dict, ms=20, color=self.color,
                                                             marker='o',
                                                             linestyle='-', linewidth=3,
                                                             counts=resampled_df['counts'])

    return prev_line
