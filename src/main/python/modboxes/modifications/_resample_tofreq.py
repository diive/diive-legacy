import gui.elements
import gui.plotfuncs
from gui import elements
from modboxes.modifications import _shared


class AddControls():
    """
        Creates the gui control elements and their handles for usage.

    """

    def __init__(self, opt_stack, opt_frame, opt_layout, ref_stack):
        self.opt_stack = opt_stack
        self.opt_frame = opt_frame
        self.opt_layout = opt_layout
        self.ref_stack = ref_stack

        self.add_option()
        self.add_refinements()

    def add_option(self):
        self.opt_btn = elements.add_button_to_grid(grid_layout=self.opt_layout,
                                                   txt='Resample To Frequency', css_id='',
                                                   row=1, col=0, rowspan=1, colspan=1)

        self.opt_stack.addWidget(self.opt_frame)

    def add_refinements(self):
        # Frame & Layout
        ref_frame, ref_layout = elements.add_frame_grid()
        self.ref_stack.addWidget(ref_frame)

        gui.elements.add_header_to_grid_top(layout=ref_layout, txt='MF: Resample To')

        # Frequency
        self.ref_lne_freq_duration, self.ref_drp_freq \
            = elements.grd_LabelLineeditDropdownTriplet(txt='Frequency',
                                                        css_ids=['', 'cyan', 'cyan'],
                                                        layout=ref_layout,
                                                        orientation='horiz',
                                                        row=1, col=0)
        self.ref_drp_freq.addItem('Hourly')
        self.ref_drp_freq.addItem('Daily')
        self.ref_drp_freq.addItem('Weekly')
        self.ref_drp_freq.addItem('Monthly')
        self.ref_drp_freq.addItem('Yearly')

        # Method
        self.ref_drp_agg = elements.grd_LabelDropdownPair(txt='Aggregation',
                                                          css_ids=['', 'cyan'],
                                                          layout=ref_layout,
                                                          orientation='horiz',
                                                          row=2, col=0)
        self.ref_drp_agg.addItem('Mean')
        self.ref_drp_agg.addItem('Median')
        self.ref_drp_agg.addItem('SD')
        self.ref_drp_agg.addItem('Minimum')
        self.ref_drp_agg.addItem('Maximum')
        self.ref_drp_agg.addItem('Count')
        self.ref_drp_agg.addItem('Sum')
        self.ref_drp_agg.addItem('Variance')

        # Required Minimum Values
        self.ref_lne_min_vals = elements.add_label_linedit_pair_to_grid(txt='Minimum Values',
                                                                        css_ids=['', 'cyan'],
                                                                        layout=ref_layout,
                                                                        orientation='horiz',
                                                                        row=3, col=0)

        self.ref_btn_show_in_plot = elements.add_button_to_grid(grid_layout=ref_layout,
                                                                txt='Show in Plot', css_id='',
                                                                row=4, col=0, rowspan=1, colspan=2)

        ref_layout.setRowStretch(5, 1)


class Call():

    def __init__(self, fig, ax, focus_df, focus_col, duration, to_freq, agg_method, min_vals):
        # kudos: http://benalexkeen.com/resampling-time-series-data-with-pandas/

        self.fig = fig  # needed for redraw
        self.ax = ax  # adds to existing axis
        self.focus_df = focus_df.copy()
        self.focus_col = focus_col

        self.agg_method = agg_method
        self.duration = int(duration)
        self.min_vals = int(min_vals)

        self.freq_str = pkgs.dfun.times.generate_freq_str(to_freq=to_freq)

        # Add info about duration to freq_str, e.g. '3T' for mean values over 3 minutes
        self.freq_str = '{}{}'.format(self.duration, self.freq_str)

        self.marker_col = ('rs_freq_agg', 'marker')

        # Every time the slider multiplier is changed to a new value,
        # the marker that show the outlier values are drawn.
        # In case there is already a marker in the plot, it needs to be
        # removed first, then the new markers are drawn.
        # Since the main plot of the time series is line 0, the marker
        # and the limit lines are lines > 0. Therefore, here we try
        # to remove all lines > 0. If there is a marker and aux lines,
        # then all are removed. If there are none, in the current plot,
        # nothing is removed. Line 0 is the main plot and is never removed.
        # Since the index of lines changes after a removal, 3 times line 1
        # is removed.
        self.ax = gui.plotfuncs.remove_prev_lines(ax=self.ax)

    def resampling(self):

        # Count how many values fall into the selected averaging period
        focus_df_resampled = self.focus_df.copy()
        focus_df_resampled['counts'] = self.focus_df[self.focus_col].apply(self.freq_str).count()

        # Marker resampled values
        if self.agg_method == 'Mean':
            focus_df_resampled[self.marker_col] = self.focus_df[self.focus_col].apply(self.freq_str).mean()
        elif self.agg_method == 'Median':
            focus_df_resampled[self.marker_col] = self.focus_df[self.focus_col].apply(self.freq_str).median_col()
        elif self.agg_method == 'SD':
            focus_df_resampled[self.marker_col] = self.focus_df[self.focus_col].apply(self.freq_str).std_col()
        elif self.agg_method == 'Minimum':
            focus_df_resampled[self.marker_col] = self.focus_df[self.focus_col].apply(self.freq_str).min()
        elif self.agg_method == 'Maximum':
            focus_df_resampled[self.marker_col] = self.focus_df[self.focus_col].apply(self.freq_str).max()
        elif self.agg_method == 'Count':
            focus_df_resampled[self.marker_col] = self.focus_df[self.focus_col].apply(self.freq_str).count()
        elif self.agg_method == 'Sum':
            focus_df_resampled[self.marker_col] = self.focus_df[self.focus_col].apply(self.freq_str).sum()
        elif self.agg_method == 'Variance':
            focus_df_resampled[self.marker_col] = self.focus_df[self.focus_col].apply(self.freq_str).var_col()

        focus_df_resampled = focus_df_resampled.loc[focus_df_resampled['counts'] >= self.min_vals]

        self.color = '#e24d42'  # orange red
        _ = _shared.show_converted_in_plot(self=self,
                                           marker_col=self.marker_col,
                                           resampled_df=focus_df_resampled)
        # Resampling_shared.show_in_plot(self=self)
        return self.focus_df, self.marker_col
