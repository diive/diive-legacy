"""

XXX

"""
import matplotlib.gridspec as gridspec
import numpy as np
import pandas as pd
from PyQt5 import QtGui as qtg
from PyQt5 import QtWidgets as qtw

import gui.base
import pkgs.dfun.frames
import gui
import logger


# pd.set_option('display.width', 1000)
# pd.set_option('display.max_columns', 15)


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SPwT')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, qtg.QIcon(app_obj.ctx.tab_icon_analyses))

        # Add settings menu contents
        self.drp_var, self.drp_agg_type, self.lne_upper_lim, self.lne_lower_lim, \
        self.drp_grouping, self.lne_freq_duration, self.drp_freq, self.lne_min_vals, \
        self.drp_sort_results, self.lne_results_how_many, self.drp_aux_var1, self.drp_aux_var2, \
        self.btn_run = \
            self.add_settings_fields()

        # Add variables required in settings
        self.populate_settings_fields()

        # Add plots
        self.axes_dict = self.populate_plot_area()

    def populate_plot_area(self):
        return self.add_axes()

    def add_axes(self):
        gs = gridspec.GridSpec(2, 1)  # rows, cols
        gs.update(wspace=0.2, hspace=0.2, left=0.03, right=0.97, top=0.97, bottom=0.03)

        # plots
        ax_main = self.fig.add_subplot(gs[0, 0])
        ax_bars = self.fig.add_subplot(gs[1, 0])
        axes_dict = {'ax_main': ax_main, 'ax_bars': ax_bars}
        for key, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)
        return axes_dict

    def populate_settings_fields(self):
        self.drp_aux_var1.clear()
        self.drp_aux_var2.clear()
        self.drp_var.clear()

        # Default values for the text fields
        self.lne_min_vals.setText('0')
        self.lne_freq_duration.setText('1')
        self.lne_results_how_many.setText('20')
        self.lne_upper_lim.setText('9999')
        self.lne_lower_lim.setText('-9999')

        default_var_ix = 0

        # Add all variables to drop-down lists to allow full flexibility yay
        for ix, colname_tuple in enumerate(self.tab_data_df.columns):
            # print(colname_tuple)
            self.drp_aux_var1.addItem(self.col_list_pretty[ix])
            self.drp_aux_var2.addItem(self.col_list_pretty[ix])
            self.drp_var.addItem(self.col_list_pretty[ix])

        # Set dropdowns to found ix
        self.drp_aux_var1.setCurrentIndex(default_var_ix)
        self.drp_aux_var2.setCurrentIndex(default_var_ix)
        self.drp_var.setCurrentIndex(default_var_ix)

    def add_settings_fields(self):
        # Aggregation
        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt='Aggregation', row=0)
        drp_var = gui.elements.grd_LabelDropdownPair(txt='Main Variable',
                                                     css_ids=['', 'cyan'],
                                                     layout=self.sett_layout,
                                                     row=1, col=0,
                                                     orientation='horiz')
        drp_agg_type = gui.elements.grd_LabelDropdownPair(txt='Method',
                                                          css_ids=['', 'cyan'],
                                                          layout=self.sett_layout,
                                                          row=2, col=0,
                                                          orientation='horiz')
        drp_agg_type.addItems(['count', 'mean', 'median', 'min', 'max', 'sum', 'std'])
        lne_upper_lim = gui.elements.add_label_linedit_pair_to_grid(txt='Consider If Smaller Or Equal To',
                                                                    css_ids=['', 'cyan'],
                                                                    layout=self.sett_layout,
                                                                    orientation='horiz',
                                                                    row=3, col=0)
        lne_lower_lim = gui.elements.add_label_linedit_pair_to_grid(txt='Consider If Larger Or Equal To',
                                                                    css_ids=['', 'cyan'],
                                                                    layout=self.sett_layout,
                                                                    orientation='horiz',
                                                                    row=4, col=0)
        self.sett_layout.addWidget(qtw.QLabel(), 5, 0, 1, 1)  # spacer

        # Time Windows
        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt='Time Windows', row=6)
        drp_grouping = gui.elements.grd_LabelDropdownPair(txt='Overlap',
                                                          css_ids=['', 'cyan'],
                                                          layout=self.sett_layout,
                                                          row=7, col=0,
                                                          orientation='horiz')
        drp_grouping.addItems(['No (Group By)', 'Yes (rolling stats not implemented...yet)'])

        # Frequency
        lne_freq_duration, drp_freq \
            = gui.elements.grd_LabelLineeditDropdownTriplet(txt='Size',
                                                            css_ids=['', 'cyan', 'cyan'],
                                                            layout=self.sett_layout,
                                                            orientation='horiz',
                                                            row=8, col=0)
        drp_freq.addItems(['Hour(s)', 'Day(s)', 'Week(s)', 'Month(s)', 'Year(s)'])

        # Required Minimum Values
        lne_min_vals = gui.elements.add_label_linedit_pair_to_grid(txt='Minimum Values',
                                                                   css_ids=['', 'cyan'],
                                                                   layout=self.sett_layout,
                                                                   orientation='horiz',
                                                                   row=9, col=0)
        self.sett_layout.addWidget(qtw.QLabel(), 10, 0, 1, 1)  # spacer

        # Sort results
        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt='Sort Results', row=11)

        drp_sort_results = gui.elements.grd_LabelDropdownPair(txt='Sort Results',
                                                              css_ids=['', 'cyan'],
                                                              layout=self.sett_layout,
                                                              row=12, col=0,
                                                              orientation='horiz')
        drp_sort_results.addItems(['Descending', 'Ascending', 'Do Not Sort'])

        # Required Minimum Values
        lne_results_how_many = gui.elements.add_label_linedit_pair_to_grid(txt='Show How Many Results',
                                                                           css_ids=['', 'cyan'],
                                                                           layout=self.sett_layout,
                                                                           orientation='horiz',
                                                                           row=13, col=0)
        self.sett_layout.addWidget(qtw.QLabel(), 14, 0, 1, 1)  # spacer

        # Data
        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt='Data', row=15)

        # Variable selection
        drp_aux_var1 = gui.elements.grd_LabelDropdownPair(txt='Additional Variable 1',
                                                          css_ids=['', 'cyan'],
                                                          layout=self.sett_layout,
                                                          row=16, col=0,
                                                          orientation='horiz')
        drp_aux_var2 = gui.elements.grd_LabelDropdownPair(txt='Additional Variable 2',
                                                          css_ids=['', 'cyan'],
                                                          layout=self.sett_layout,
                                                          row=17, col=0,
                                                          orientation='horiz')
        self.sett_layout.addWidget(qtw.QLabel(), 18, 0, 1, 1)  # spacer

        # Run button
        btn_run = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                  txt='Run', css_id='btn_cat_ControlsRun',
                                                  row=19, col=0, rowspan=1, colspan=2)

        # Stretch
        self.sett_layout.setRowStretch(20, 2)  # empty row
        self.sett_layout.setColumnStretch(0, 0)  # empty row
        self.sett_layout.setColumnStretch(1, 0)  # empty row
        self.sett_layout.setColumnStretch(2, 1)  # empty row

        return drp_var, drp_agg_type, lne_upper_lim, lne_lower_lim, drp_grouping, lne_freq_duration, drp_freq, \
               lne_min_vals, drp_sort_results, lne_results_how_many, drp_aux_var1, drp_aux_var2, btn_run


class Run(addContent):
    sub_outdir = "analysis_aggregator"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Aggregator', dict={}, highlight=True)  # Log info
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.get_settings_from_fields()

    def get_settings_from_fields(self):
        """Get all settings from fields"""
        self.var_col = self.get_colname_tuples(self.drp_var.currentText())
        self.lower_lim = float(self.lne_lower_lim.text())
        self.upper_lim = float(self.lne_upper_lim.text())
        self.freq = self.drp_freq.currentText()
        self.freq_duration = int(self.lne_freq_duration.text())
        self.grouping = self.drp_grouping.currentText()
        self.min_vals = int(self.lne_min_vals.text())
        self.agg_type = self.drp_agg_type.currentText()
        self.results_how_many = int(self.lne_results_how_many.text())
        self.sort_results = self.drp_sort_results.currentText()

    def run(self):
        self.get_settings_from_fields()

        # Assemble and filter needed data
        self.aggr_df = self.make_aggr_df()
        self.aggr_df = self.filter_within_lim(df=self.aggr_df)

        # Add info about duration to freq_str, e.g. '3T' for mean values over 3 minutes
        self.freq_str = pkgs.dfun.times.generate_freq_str(to_freq=self.freq)
        self.freq_str = '{}{}'.format(self.freq_duration, self.freq_str)
        self.timestamp_resampled_col = ("TIMESTAMP", f"[in_{self.freq_str}_timeres]", '')

        # Aggregate
        agg_df = self.do_aggregation(df=self.aggr_df,
                                     agg=['median', 'mean', 'std', 'min', 'max', 'sum', 'count'],
                                     method=self.grouping)

        # Percentile info, overall mean
        agg_df = self.remove_counts_below_threshold(df=agg_df, use_col=self.var_col, threshold=self.min_vals)
        agg_df = self.insert_percentile_info(df=agg_df)
        overall_mean = agg_df[self.var_col][self.agg_type].mean()
        agg_df['over_under_overall_mean'] = agg_df[self.var_col][self.agg_type].subtract(overall_mean)

        # Prepare results for tableview
        _agg_df_flat_round = self.prepare_tableview(df=agg_df, sort_by=self.agg_type,
                                                    var_col=self.var_col, round_digits=5,
                                                    shortlist_only=True)

        # Show in tableview
        gui.tableview.show_df(df=_agg_df_flat_round,
                              how_many_rows=self.results_how_many,
                              tableview=self.table_view)

        # Plot
        self.plot_occurrences(df=agg_df, col=self.var_col, overall_mean_of_agg=overall_mean)

    def plot_occurrences(self, df, col, overall_mean_of_agg):

        ax = self.axes_dict['ax_main']
        ax.clear()
        ax.plot_date(df.index, y=df[col][self.agg_type], c='#FFB300', alpha=0.9,
                     markeredgecolor='none', marker='o', ls='-', lw=1,
                     label='{} of {}'.format(self.agg_type, col[0]))
        ax.axhline(y=0, color='#444444', ls='-', lw=1, alpha=0.8, zorder=97)
        ax.axhline(y=overall_mean_of_agg, color='#26C6DA', ls='--', lw=1, alpha=0.8, zorder=98, label='mean')

        gui.plotfuncs.default_legend(ax=ax)

        ax = self.axes_dict['ax_bars']
        ax.clear()
        bar_col = 'over_under_overall_mean'

        _df = df.copy()
        _filter = _df[bar_col] >= 0
        _df.loc[~_filter, bar_col] = np.nan
        # _df = _df[_filter]
        ax.bar(_df.index, _df['over_under_overall_mean'], label='over mean', width=1, fc='#9CCC65')

        _df = df.copy()
        _filter = _df[bar_col] < 0
        _df.loc[~_filter, bar_col] = np.nan
        # _df = _df[_filter]
        ax.bar(_df.index, _df['over_under_overall_mean'], label='under mean', width=1, fc='#ef5350')

        ax.axhline(y=0, color='#444444', ls='-', lw=1, alpha=0.8, zorder=97)
        gui.plotfuncs.default_legend(ax=ax)

        # Update Figure during calculations
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def insert_percentile_info(self, df):
        for col in df.columns:
            if 'median' in col:
                median_col_iloc = df.columns.get_loc(col)
                median_info_col_iloc = median_col_iloc + 1
                # median_of_medians = df[col].median()

                # Express median values as percentiles
                median_perc_data = pd.qcut(df[col], 100, labels=False, duplicates='drop')
                # median_info_data = pd.qcut(df[col], 5, labels=['very low', 'low', 'medium', 'high', 'very high'])

                median_perc_col = (col[0], col[1], 'median_perc')
                df.insert(median_info_col_iloc, median_perc_col, median_perc_data)
        return df

    def do_aggregation(self, df, agg, method):
        """

        :param df: pandas DataFrame
        :param var_col: name of the column that is analyzed as tuple
        :param group_by: column name as tuple or list, e.g. df.index.month
        :param agg: list, contains methods to convert data, e.g. 'mean', 'std', 'max'
        :param method: 'group_by' or 'rolling'
        :return: aggregated DataFrame
        """

        _df = pkgs.dfun.frames.remove_duplicate_cols(df=df)

        # testing: find e.g. hottest week
        if method == 'No (Group By)':
            # todo refine Grouper
            _df = _df.groupby(pd.Grouper(level=df.index.name, freq=self.freq_str))

        elif method == 'Rolling':
            # todo
            # _df = _df.rolling(self.freq_str, min_periods=None, center=False)
            pass

        else:
            raise

        agg_df = _df.agg(agg)

        # .agg in combination with .rolling does *not* work with datetime
        # .agg in combination with .groupby works
        # self._eef_df.index.name: ['min', 'max'],

        # agg_df = _df.agg({
        #     var_col: agg,
        #     add_var1_col: agg
        # })

        return agg_df

    def remove_counts_below_threshold(self, df, use_col, threshold):
        # Use only rows where count of main_var passes the min vals threshold
        _filter_by_col = (use_col[0], use_col[1], 'count')
        _filter = df[_filter_by_col] >= threshold
        df = df[_filter]
        return df

    def sort_df(self, sort_by_col, df, sort_by):
        _sort_by_col = (sort_by_col[0], sort_by_col[1], sort_by)
        if self.sort_results != 'Do Not Sort':
            ascending = True if self.sort_results == 'Ascending' else False
            df.sort_values(by=_sort_by_col, ascending=ascending, inplace=True)
        return df

    def insert_timestamp_col(self, df):
        # Insert resampled timestamp as col, needed for table view
        df.insert(0, self.timestamp_resampled_col, df.index)
        return df

    def flatten_col_names(self, df):
        # Flatten column MultiIndex, needed for table display
        df.columns = ['_'.join(col).strip() for col in df.columns.values]
        return df

    def prepare_tableview(self, df, sort_by, var_col, round_digits, shortlist_only):
        # df.columns = df.columns.droplevel([0, 1])  # column ix simplified for table view

        df = df.copy()
        df = self.sort_df(sort_by_col=var_col, df=df, sort_by=sort_by)
        df = self.insert_timestamp_col(df=df)

        # todo timestamp as table index?
        # Create shortlist of vars to be shown in the table, basically a subset of the df

        shortlist = [
            self.timestamp_resampled_col,
            (self.var_col[0], self.var_col[1], self.agg_type),
            (self.var_col[0], self.var_col[1], 'count')
        ]

        if shortlist_only:
            df = df[shortlist]
        df = self.flatten_col_names(df=df)

        df = pkgs.dfun.frames.remove_duplicate_cols(df=df)

        df = df.round(round_digits)
        df.reset_index(inplace=True, drop=True)
        return df

    def get_colname_tuples(self, colname_pretty):
        # selected_cols_pretty = [self.main_var_data_col, self.add_var1_data_col, self.count_var_data_col]
        # selected_cols_tuples = []
        # for col in selected_cols_pretty:
        col_pretty_ix = self.col_list_pretty.index(colname_pretty)  # get ix in pretty list
        colname_tuple = self.col_dict_tuples[col_pretty_ix]  # use ix to get col name as tuple
        return colname_tuple

    def make_aggr_df(self):
        aggr_df = pd.DataFrame(data=self.tab_data_df[[self.var_col]])
        # Count and Add cols are renamed for visibility but also b/c they can be the same col as the main var.
        # Duplicate column names in the df causes problems.
        # aggr_df[self.aux_var1_data_col_renamed] = self.data_df[self.aux_var1_data_col]
        # aggr_df[self.aux_var2_data_col_renamed] = self.data_df[self.aux_var2_data_col]
        return aggr_df

    def filter_within_lim(self, df):
        filter_within_thres = (df[self.var_col] >= self.lower_lim) & \
                              (df[self.var_col] <= self.upper_lim)
        df.loc[~filter_within_thres, self.var_col] = np.nan
        return df
