"""

FEATURE SELECTION

    * Boruta
        - Code: https://github.com/scikit-learn-contrib/boruta_py
        - Description: https://danielhomola.com/feature%20selection/phd/borutapy-an-all-relevant-feature-selection-method/
        - Original method paper: https://www.jstatsoft.org/article/view/v036i11
        kudos:
            - https://blog.exploratory.io/finding-variable-importance-with-random-forest-boruta-28badd116197
            - https://towardsdatascience.com/simple-example-using-boruta-feature-selection-in-python-8b96925d5d7a

    * Recursive feature elimination and cross-validation
        Feature ranking with recursive feature elimination and
        cross-validated selection of the best number of features:
        - https://scikit-learn.org/stable/modules/generated/sklearn.feature_selection.RFECV.html
        - https://scikit-learn.org/stable/modules/cross_validation.html#cross-validation-of-time-series-data
        kudos:
            - https://github.com/betterdatascience/YouTube/blob/master/0001_RFECV.ipynb
            - https://www.youtube.com/watch?v=jXSw6em5whI

"""
import fnmatch

import matplotlib.gridspec as gridspec
import numpy as np
import pandas as pd
from PyQt5 import QtGui as qtg
from PyQt5 import QtWidgets as qtw
from PyQt5.QtGui import QDoubleValidator, QIntValidator
from boruta import BorutaPy
from matplotlib.ticker import ScalarFormatter
from sklearn.ensemble import RandomForestRegressor
from sklearn.feature_selection import RFECV
from sklearn.model_selection import TimeSeriesSplit

import gui
import gui.add_to_layout
import gui.base
import gui.make
import logger
import modboxes.plots.styles.LightTheme as theme
from help.tooltips import an_feature_selection as tooltips
from utils.vargroups import *


# pd.set_option('display.width', 1000)
# pd.set_option('display.max_columns', 15)
# pd.set_option('display.max_rows', 999)


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVonVP')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, qtg.QIcon(app_obj.ctx.tab_icon_analyses))

        # Add settings menu contents
        self.drp_target, self.btn_add_vars_auto, self.btn_find_features, \
        self.drp_estimator, self.lne_step, self.lne_min_features_to_select, self.drp_cv, \
        self.drp_scoring, self.lne_verbose, self.lne_n_jobs, self.drp_importance_getter, \
        self.drp_method, self.lne_method_n_estimators, self.lne_perc_threshold, \
        self.lne_alpha, self.drp_two_step, self.lne_max_iter = \
            self.add_settings_fields()

        # # Add variables required in settings
        # self.populate_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # Add special
        self.lbl_statsbox_numvals_target, self.lbl_statsbox_available = self.add_statsboxes()

        # # Add plots
        # self.axes_dict = self.populate_plot_area()

    def add_statsboxes(self):
        frm_statsboxes_gf = qtw.QFrame()
        grd_statsboxes_gf = qtw.QGridLayout()  # create layout for frame
        frm_statsboxes_gf.setLayout(grd_statsboxes_gf)  # assign layout to frame
        # lyt_vert_figs_tt_gf.setContentsMargins(0, 0, 0, 0)

        lbl_statsbox_numvals_target = gui.elements.add_label_pair_to_grid_layout(
            txt='Target Number Of Values (Without NaNs)',
            css_ids=['lbl_statsbox_txt', 'lbl_statsbox_val'],
            layout=grd_statsboxes_gf,
            row=0, col=0, orientation='vert')
        lbl_statsbox_numvals_target.setText('-')

        lbl_statsbox_available = gui.elements.add_label_pair_to_grid_layout(
            txt='Available Rows',
            css_ids=['lbl_statsbox_txt', 'lbl_statsbox_val'],
            layout=grd_statsboxes_gf,
            row=0, col=1, orientation='vert')
        lbl_statsbox_available.setText('-')

        self.lyt_plot_area.addWidget(frm_statsboxes_gf, stretch=1)
        return lbl_statsbox_numvals_target, lbl_statsbox_available

    def populate_plot_area(self):
        pass

    def populate_settings_fields(self):
        pass

    def add_axes(self):
        pass

    def add_settings_fields(self):
        gui.elements.add_header_subheader_to_grid_top(layout=self.sett_layout,
                                                      txt=["Feature Selection",
                                                           "Analyses"])
        gui.add_to_layout.add_spacer_item_to_grid(self.sett_layout, 1, 0)

        # Target
        gui.elements.add_header_in_grid_row(
            row=2, layout=self.sett_layout, txt='Target')
        drp_target = gui.elements.grd_LabelDropdownPair(
            txt='Target', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=3, col=0, orientation='horiz')
        gui.add_to_layout.add_spacer_item_to_grid(self.sett_layout, 4, 0)

        # # Day / night todo
        # gui.gui_elements.add_header_in_grid_row(
        #     row=3, layout=self.sett_layout, txt='Day / Night')
        # drp_separate_day_night = gui_elements.grd_LabelDropdownPair(
        #     txt='Separate For Day / Night', css_ids=['', 'cyan'], layout=self.sett_layout,
        #     row=4, col=0, orientation='horiz')
        # drp_separate_day_night.addItems(['Yes', 'No'])
        # drp_define_nighttime_based_on = gui_elements.grd_LabelDropdownPair(
        #     txt='Define Nighttime Based On', css_ids=['', 'cyan'], layout=self.sett_layout,
        #     row=5, col=0, orientation='horiz')
        # drp_set_nighttime_if = gui_elements.grd_LabelDropdownPair(
        #     txt='Set To Nighttime If', css_ids=['', 'cyan'], layout=self.sett_layout,
        #     row=6, col=0, orientation='horiz')
        # drp_set_nighttime_if.addItems(['Smaller Than Threshold', 'Larger Than Threshold'])
        # lne_nighttime_threshold = gui_elements.add_label_linedit_pair_to_grid(
        #     txt='Threshold', css_ids=['', 'cyan'], layout=self.sett_layout,
        #     row=7, col=0, orientation='horiz')
        # lne_nighttime_threshold.setText('20')
        # gui_elements.add_spacer_item_to_grid(self.sett_layout, 8, 0)

        # MODEL SETTINGS
        # =======================================
        onlyInt = QIntValidator()
        onlyFloat = QDoubleValidator()

        gui.elements.add_header_in_grid_row(
            row=7, layout=self.sett_layout, txt='Settings')
        args = dict(css_ids=['', 'cyan'], layout=self.sett_layout, col=0, orientation='horiz')

        drp_method = gui.elements.grd_LabelDropdownPair(
            txt='Method', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=8, col=0, orientation='horiz')
        gui.add_to_layout.add_spacer_item_to_grid(self.sett_layout, 6, 0)
        drp_method.addItems(['Boruta',
                             'Recursive Feature Elimination With Cross-Validation'])

        drp_estimator = gui.elements.add_label_dropdown_info_triplet_to_grid(
            row=9, txt='Estimator', txt_info_hover=tooltips._help_drp_estimator, **args)
        drp_estimator.addItems(['Random Forest Regressor (10)',
                                'Random Forest Regressor (100)'])

        lne_method_n_estimators = gui.elements.add_label_linedit_info_triplet_to_grid(
            row=10, txt='Method Estimators', txt_info_hover=tooltips._help_lne_method_estimators, **args)
        lne_method_n_estimators.setText('-1')
        lne_method_n_estimators.setValidator(onlyInt)

        lne_step = gui.elements.add_label_linedit_info_triplet_to_grid(
            row=11, txt='Step', txt_info_hover=tooltips._help_lne_step, **args)
        lne_step.setText('1')
        lne_step.setValidator(onlyFloat)

        lne_min_features_to_select = gui.elements.add_label_linedit_info_triplet_to_grid(
            row=12, txt='Min Features To Select', txt_info_hover=tooltips._help_lne_min_features_to_select, **args)
        lne_min_features_to_select.setText('1')
        lne_min_features_to_select.setValidator(onlyInt)

        drp_cv = gui.elements.add_label_dropdown_info_triplet_to_grid(
            row=13, txt='Cross-validation Splitting Strategy', txt_info_hover=tooltips._help_drp_cv, **args)
        drp_cv.addItems(['Time Series Split'])

        drp_scoring = gui.elements.add_label_dropdown_info_triplet_to_grid(
            row=14, txt='Scoring', txt_info_hover=tooltips._help_drp_scoring, **args)
        drp_scoring.addItems(['None'])

        lne_verbose = gui.elements.add_label_linedit_info_triplet_to_grid(
            row=15, txt='Verbose', txt_info_hover=tooltips._help_lne_verbose, **args)
        lne_verbose.setText('0')
        lne_verbose.setValidator(onlyInt)

        lne_n_jobs = gui.elements.add_label_linedit_info_triplet_to_grid(
            row=16, txt='Number Of Jobs', txt_info_hover=tooltips._help_lne_n_jobs, **args)
        lne_n_jobs.setText('-9999')  # -9999 translates to None
        lne_n_jobs.setValidator(onlyInt)

        drp_importance_getter = gui.elements.add_label_dropdown_info_triplet_to_grid(
            row=17, txt='Feature Importance', txt_info_hover=tooltips._help_drp_importance_getter, **args)
        drp_importance_getter.addItems(['Auto'])

        lne_perc_threshold = gui.elements.add_label_linedit_info_triplet_to_grid(
            row=18, txt='Percentile Threshold', txt_info_hover=tooltips._help_lne_perc_threshold, **args)
        lne_perc_threshold.setText('100')
        lne_perc_threshold.setValidator(onlyInt)

        lne_alpha = gui.elements.add_label_linedit_info_triplet_to_grid(
            row=19, txt='Alpha', txt_info_hover=tooltips._help_lne_alpha, **args)
        lne_alpha.setText('0.05')
        lne_alpha.setValidator(onlyFloat)

        lne_max_iter = gui.elements.add_label_linedit_info_triplet_to_grid(
            row=20, txt='Max Number Of Iterations', txt_info_hover=tooltips._help_lne_max_iter, **args)
        lne_max_iter.setText('100')
        lne_max_iter.setValidator(onlyInt)

        drp_two_step = gui.elements.add_label_dropdown_info_triplet_to_grid(
            row=21, txt='Two Step', txt_info_hover=tooltips._help_drp_two_step, **args)
        drp_two_step.addItems(['True', 'False'])

        gui.add_to_layout.add_spacer_item_to_grid(self.sett_layout, 22, 0)

        # Buttons
        btn_add_vars_auto = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='Add All Variables To Selected', css_id='btn_cat_ControlsRun',
            row=23, col=0, rowspan=1, colspan=2)
        btn_find_features = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='Find Features', css_id='btn_cat_ControlsRun',
            row=24, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(25, 2)  # empty row

        return drp_target, btn_add_vars_auto, btn_find_features, \
               drp_estimator, lne_step, lne_min_features_to_select, drp_cv, drp_scoring, lne_verbose, \
               lne_n_jobs, drp_importance_getter, drp_method, lne_method_n_estimators, lne_perc_threshold, \
               lne_alpha, drp_two_step, lne_max_iter


class Run(addContent):
    """
    Start random forest
    """
    sub_outdir = "analyses_feature_selection"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Analyses: Feature Selection', dict={}, highlight=True)  # Log info
        self.grp_model_dict = {}
        self.class_df = pd.DataFrame()
        self.selected_vars_lookup_dict = {}
        self.vars_selected_df = pd.DataFrame()
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.update_refinements()
        self.check_method_selection()

    def check_method_selection(self):
        """Activates or deactivates GUI fields depending on selected method"""
        self.get_settings_from_fields()

        if self.method == 'Boruta':
            self.drp_estimator.setEnabled(True)
            self.lne_verbose.setEnabled(True)
            self.lne_method_n_estimators.setEnabled(True)
            self.lne_perc_threshold.setEnabled(True)
            self.lne_alpha.setEnabled(True)
            self.drp_two_step.setEnabled(True)
            self.lne_max_iter.setEnabled(True)

            self.lne_n_jobs.setDisabled(True)  # todo for estimator?
            self.lne_step.setDisabled(True)
            self.lne_min_features_to_select.setDisabled(True)
            self.drp_cv.setDisabled(True)
            self.drp_scoring.setDisabled(True)
            self.drp_importance_getter.setDisabled(True)

        if self.method == 'Recursive Feature Elimination With Cross-Validation':
            self.drp_estimator.setEnabled(True)
            self.lne_step.setEnabled(True)
            self.lne_min_features_to_select.setEnabled(True)
            self.drp_cv.setEnabled(True)
            self.drp_scoring.setEnabled(True)
            self.lne_verbose.setEnabled(True)
            self.lne_n_jobs.setEnabled(True)
            self.drp_importance_getter.setEnabled(True)

            self.lne_method_n_estimators.setDisabled(True)
            self.lne_perc_threshold.setDisabled(True)
            self.lne_alpha.setDisabled(True)
            self.drp_two_step.setDisabled(True)
            self.lne_max_iter.setDisabled(True)

    def update_refinements(self):
        self.drp_target.clear()
        # self.drp_define_nighttime_based_on.clear()

        default_target_ix = default_nighttime_ix = 0
        default_target_ix_found = default_nighttime_ix_found = False

        # Add all variables to drop-down lists to allow full flexibility yay
        for ix, colname_tuple in enumerate(self.tab_data_df.columns):
            self.drp_target.addItem(self.col_list_pretty[ix])
            # self.drp_define_nighttime_based_on.addItem(self.col_list_pretty[ix])

            # Check if column appears in the global vars
            if any(fnmatch.fnmatch(colname_tuple[0], vid) for vid in NIGHTTIME_DETECTION):
                default_nighttime_ix = ix if not default_nighttime_ix_found else default_nighttime_ix
                default_nighttime_ix_found = True

        # Set dropdowns to found ix
        self.drp_target.setCurrentIndex(default_target_ix)
        # self.drp_define_nighttime_based_on.setCurrentIndex(default_nighttime_ix)

        # # Default values for the text fields
        # self.lne_swin_sim.setText('50')  # W m-2

    def add_var_to_selected(self):
        """Add feature from available to selected"""
        selected_var = self.lst_varlist_available.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = self.col_dict_tuples[selected_var_ix]
        selected_pretty = self.col_list_pretty[selected_var_ix]  # Needs new pretty list (screen names)

        # Add to lookup dict and df
        self.selected_vars_lookup_dict[selected_col] = selected_pretty
        self.vars_selected_df[selected_col] = self.tab_data_df[selected_col]
        self.update_varlist_selected(list=self.lst_varlist_selected,
                                     df=self.vars_selected_df,
                                     lookup_dict=self.selected_vars_lookup_dict)

        self.get_settings_from_fields()

    def add_all_vars_to_selected(self):
        for ix in range(self.lst_varlist_available.count()):
            var_pretty = self.lst_varlist_available.item(ix).text()
            var_col = self.col_dict_tuples[ix]
            self.selected_vars_lookup_dict[var_col] = var_pretty
            self.vars_selected_df[var_col] = self.tab_data_df[var_col].copy()

        self.update_varlist_selected(list=self.lst_varlist_selected,
                                     df=self.vars_selected_df,
                                     lookup_dict=self.selected_vars_lookup_dict)
        # self.get_settings_from_fields()

    def remove_var_from_selected(self):
        """Remove feature from available to selected"""
        selected_var = self.lst_varlist_selected.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = list(self.selected_vars_lookup_dict.keys())[selected_var_ix]  # Get key by index
        # selected_pretty = self.selected_features_lookup_dict[selected_col]

        # Remove from lookup dict and df
        del self.selected_vars_lookup_dict[selected_col]
        self.vars_selected_df.drop(selected_col, axis=1, inplace=True)
        self.update_varlist_selected(list=self.lst_varlist_selected,
                                     df=self.vars_selected_df,
                                     lookup_dict=self.selected_vars_lookup_dict)

        self.check_if_selected_empty()
        self.get_settings_from_fields()

    def stats_target_features(self):
        target_gap_locations = self.tab_data_df[self.target_col].isnull()  # True if gap in measured
        target_num_gaps = target_gap_locations.sum()
        target_available_vals = self.tab_data_df[self.target_col][~target_gap_locations].count()
        features_available_vals_with_target = len(self.vars_selected_df[~target_gap_locations].dropna())
        return target_num_gaps, target_available_vals, features_available_vals_with_target

    def fill_statsboxes(self):
        self.lbl_statsbox_numvals_target.setText(
            f"{self.target_col[0]} Number Of Values (Without NaNs) : "
            f"{self.target_available_vals}")

        perc = (self.features_available_vals_with_target / self.target_available_vals) * 100
        self.lbl_statsbox_available.setText(
            f"All selected features are available for {self.features_available_vals_with_target} "
            f"of {self.target_available_vals} target values ({perc:.1f}%)")

    def update_varlist_selected(self, list, df, lookup_dict):
        list.clear()
        for col in df.columns:
            item = qtw.QListWidgetItem(lookup_dict[col])
            list.addItem(item)

    def check_if_selected_empty(self):
        _list = self.lst_varlist_selected
        if self.vars_selected_df.empty:
            _list.addItem('-empty-')

    def collect_data(self):
        """Collect training and test data: measured data and selected features data in one df"""
        tt_df = self.vars_selected_df.copy()  # Add selected features
        tt_df[self.target_col] = self.tab_data_df[self.target_col]  # Add measured data, target
        tt_df = tt_df.dropna()
        return tt_df

    def get_settings_from_fields(self):
        """Get settings from fields and translate to usable formats"""

        self.target_col = self.get_col_from_drp_pretty(drp=self.drp_target)
        # self.nighttime_col = self.get_col_from_drp_pretty(drp=self.drp_define_nighttime_based_on)
        # self.separate_day_night = self.drp_separate_day_night.currentText()
        # self.set_nighttime_if = self.drp_set_nighttime_if.currentText()
        # self.nighttime_threshold = int(self.lne_nighttime_threshold.text())

        self.target_col = self.get_col_from_drp_pretty(drp=self.drp_target)  # Update target from dropdown selection

        # Model settings
        # see: https://scikit-learn.org/stable/modules/generated/sklearn.feature_selection.RFECV.html

        self.method = self.drp_method.currentText()

        _estimator = self.drp_estimator.currentText()
        if _estimator == 'Random Forest Regressor (10)':
            self.estimator = RandomForestRegressor(n_estimators=10)
        elif _estimator == 'Random Forest Regressor (100)':
            self.estimator = RandomForestRegressor(n_estimators=100)
        else:
            self.estimator = RandomForestRegressor(n_estimators=10)

        # Method number of estimators, e.g. how often to run Boruta analysis
        if int(self.lne_method_n_estimators.text()) == -1:
            self.method_n_estimators = 'auto'
        else:
            self.method_n_estimators = int(self.lne_method_n_estimators.text())

        _step = float(self.lne_step.text())
        if _step >= 1:
            self.step = int(self.lne_step.text())
        elif (0 < _step < 1):
            self.step = float(self.lne_step.text())
        else:
            self.step = 1  # Set to default

        self.min_features_to_select = int(self.lne_min_features_to_select.text())

        _cv = self.drp_cv.currentText()
        if _cv == 'Time Series Split':
            self.cv = TimeSeriesSplit(n_splits=3)
        else:
            self.cv = TimeSeriesSplit(n_splits=3)  # Set to default

        _scoring = self.drp_scoring.currentText()
        if _scoring == 'None':
            self.scoring = None
        else:
            self.scoring = None  # Set to default

        self.verbose = int(self.lne_verbose.text())
        self.n_jobs = None if self.lne_n_jobs.text() == '-9999' else int(self.lne_n_jobs.text())

        _importance_getter = self.drp_importance_getter.currentText()
        if _importance_getter == 'Auto':
            self.importance_getter = 'auto'
        else:
            self.importance_getter = 'auto'  # Set to default

        _perc_threshold = int(self.lne_perc_threshold.text())
        if _perc_threshold > 100:
            self.perc_threshold = 100
        elif _perc_threshold < 1:
            self.perc_threshold = 1
        else:
            self.perc_threshold = _perc_threshold

        _alpha = float(self.lne_alpha.text())
        if _alpha < 0:
            self.alpha = 0.01
        else:
            self.alpha = _alpha

        self.max_iter = 1 if int(self.lne_max_iter.text()) < 1 else int(self.lne_max_iter.text())

        _two_step = self.drp_two_step.currentText()
        if _two_step == 'True':
            self.two_step = True
        elif _two_step == 'False':
            self.two_step = False
        else:
            self.two_step = True  # Set to default

        self.target_num_gaps, self.target_available_vals, self.features_available_vals_with_target = \
            self.stats_target_features()
        self.fill_statsboxes()

    def find_features(self):
        """Find features"""
        self.get_settings_from_fields()
        if self.method == 'Recursive Feature Elimination With Cross-Validation':
            self.use_rfecv()
        elif self.method == 'Boruta':
            self.use_boruta()

    def prepare_data(self):
        """Prepare data"""
        features_df = self.vars_selected_df.copy()  # Get all features
        features_df[self.target_col] = self.tab_data_df[self.target_col]  # Add target data to features for NaN removal
        features_df = features_df.dropna()  # Remove NaNs
        target = features_df[[self.target_col]]  # Get target data
        target = target.to_numpy().ravel()  # Needs to be 1d array
        features_df = features_df.drop(self.target_col, axis=1)  # Remove target data from features
        X = features_df  # Can be used directly as df, column names are tuple MultiIndex
        return X, target

    def use_boruta(self):
        """Run Boruta feature selection method
        https://pypi.org/project/Boruta/#description
        """
        X_df, target = self.prepare_data()

        # Boruta needs numpy arrays, so we will use arrays also for the estimator
        X = X_df.to_numpy()

        # Estimator fit (e.g. RandomForestRegressor), needed for Boruta
        self.estimator.fit(X, target)

        # Define Boruta feature selection method
        feat_selector = BorutaPy(self.estimator,
                                 n_estimators=self.method_n_estimators,
                                 perc=self.perc_threshold,
                                 alpha=self.alpha,
                                 two_step=self.two_step,
                                 max_iter=self.max_iter,
                                 verbose=self.verbose)

        # Find all relevant features
        feat_selector.fit(X, target)

        # Results showing rank for each var
        var_ranks_df = pd.DataFrame(index=X_df.columns,
                                    data={'rank': feat_selector.ranking_,
                                          'keep': feat_selector.support_})
        var_ranks_df.sort_values('rank', ascending=False, inplace=True)

        # Plot
        axes_dict = self.add_boruta_axes()
        self.plot_boruta_feature_ranks(df=var_ranks_df, ax=axes_dict['ax_boruta_feature_ranks'])

        # feat_selector.support_  # Check selected features
        # feat_selector.ranking_  # Check ranking of features
        # # Call transform() on X to filter it down to selected features
        # X_filtered = feat_selector.transform(X)
        # # zip my names, ranks, and decisions in a single iterable
        # feature_ranks = list(zip(X_df.columns,
        #                          feat_selector.ranking_,
        #                          feat_selector.support_))
        # # iterate through and print out the results
        # for feat in feature_ranks:
        #     print(f"Feature: {feat[0]} Rank: {feat[1]},  Keep: {feat[2]}")

    def add_rfecv_axes(self):
        """Add axes for plotting RFECV results"""
        self.fig.clear()  # Reset figure
        gs = gridspec.GridSpec(2, 1)  # rows, cols
        gs.update(wspace=0.2, hspace=0.4, left=0.07, right=0.97, top=0.97, bottom=0.03)
        axes_dict = {'ax_num_selected_features': self.fig.add_subplot(gs[0, 0]),
                     'ax_feature_importances': self.fig.add_subplot(gs[1, 0])}
        for key, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)
        return axes_dict

    def add_boruta_axes(self):
        """Add axes for plotting Boruta results"""
        self.fig.clear()  # Reset figure
        gs = gridspec.GridSpec(1, 1)  # rows, cols
        gs.update(wspace=0.2, hspace=0.4, left=0.07, right=0.97, top=0.95, bottom=0.05)
        axes_dict = {'ax_boruta_feature_ranks': self.fig.add_subplot(gs[0, 0])}
        for key, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)
        return axes_dict

    def plot_boruta_feature_ranks(self, df, ax):
        """ Plot Boruta feature ranks """

        # Convert tuples to nice string b/c bar plot does not accept tuples for y
        df['feature'] = [f"{x[0]}_{x[1]}" for x in df.index]

        # Assign bar colors
        color_rank_1 = '#9CCC65'  # light green 400
        color_rank_2 = '#FFF59D'  # yellow 200
        color_rank_3 = '#CFD8DC'  # blue grey 100
        df['color'] = np.nan

        df.loc[df['rank'] == 1, ['color']] = color_rank_1
        df.loc[df['rank'] == 2, ['color']] = color_rank_2
        df.loc[df['rank'] > 2, ['color']] = color_rank_3

        ax.barh(y=df['feature'], width=df['rank'], color=df['color'])

        ax.set_title("Feature Ranks",
                     fontsize=theme.FONTSIZE_HEADER_AXIS_LARGE, color=theme.FONTCOLOR_HEADER_AXIS,
                     loc='left')
        ax.set_xlabel('Rank [1=confirmed, 2=tentative, 3=rejected]', fontsize=theme.FONTSIZE_LABELS_AXIS_7, labelpad=4)
        ax.set_ylabel('Feature', fontsize=theme.FONTSIZE_LABELS_AXIS_7, labelpad=4)
        ax.set_yticklabels(df['feature'], horizontalalignment='left')
        ax.tick_params(axis="y", direction="in", pad=-22)

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def use_rfecv(self):
        """Run Recursive Feature Elimination With Cross-Validation
        https://scikit-learn.org/stable/modules/generated/sklearn.feature_selection.RFECV.html
        """
        X, target = self.prepare_data()

        # Find features
        rfecv = RFECV(estimator=self.estimator,
                      step=self.step,
                      min_features_to_select=self.min_features_to_select,
                      cv=self.cv,
                      scoring=self.scoring,
                      verbose=self.verbose,
                      n_jobs=self.n_jobs)
        rfecv.fit(X, target)

        # Feature importances
        # print(np.where(rfecv.support_ == False)[0])
        X.drop(X.columns[np.where(rfecv.support_ == False)[0]], axis=1, inplace=True)

        feature_importances_df = pd.DataFrame()
        feature_importances_df['feature'] = X.columns
        feature_importances_df['importance'] = rfecv.estimator_.feature_importances_
        feature_importances_df = feature_importances_df.sort_values(by='importance', ascending=True)

        # Plot
        axes_dict = self.add_rfecv_axes()
        self.plot_rfecv_num_features_selected(results=rfecv, ax=axes_dict['ax_num_selected_features'])
        self.plot_rfecv_feature_importances(df=feature_importances_df, ax=axes_dict['ax_feature_importances'])

    def get_col_from_drp_pretty(self, drp):
        """ Set target to selected variable. """
        target_pretty = drp.currentText()
        target_pretty_ix = self.col_list_pretty.index(target_pretty)
        target_col = self.col_dict_tuples[target_pretty_ix]
        return target_col

    def plot_rfecv_feature_importances(self, df, ax):
        """ Plot RFECV feature importances """

        ax.clear()

        # Convert tuples to nice string b/c bar plot does not accept tuples for y
        df['feature'] = [f"{x[0]}_{x[1]}" for x in df['feature']]

        ax.barh(y=df['feature'], width=df['importance'], color='#81D4FA')

        ax.set_title("Feature Importances",
                     fontsize=theme.FONTSIZE_HEADER_AXIS_LARGE, color=theme.FONTCOLOR_HEADER_AXIS,
                     loc='left')
        ax.set_xlabel('Importance', fontsize=theme.FONTSIZE_LABELS_AXIS_7, labelpad=4)
        ax.set_ylabel('Feature', fontsize=theme.FONTSIZE_LABELS_AXIS_7, labelpad=4)
        ax.set_yticklabels(df['feature'], horizontalalignment='left')
        ax.tick_params(axis="y", direction="in", pad=-22)

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def plot_rfecv_num_features_selected(self, results, ax):
        """ Plot RFECV number of selected features and explained variance """
        ax.clear()

        x = range(1, len(results.grid_scores_) + 1)
        y = results.grid_scores_

        ax.plot(x, y,
                color='#4CAF50', alpha=0.5, ls='-',
                marker='o', markeredgecolor='none', ms=6, zorder=98, label='measured')

        ax.set_title("Recursive Feature Elimination with Cross-Validation",
                     fontsize=theme.FONTSIZE_HEADER_AXIS_LARGE, color=theme.FONTCOLOR_HEADER_AXIS,
                     loc='left')
        ax.set_xlabel('Number of features selected', fontsize=theme.FONTSIZE_LABELS_AXIS_7, labelpad=4)
        ax.set_ylabel('Score ...?', fontsize=theme.FONTSIZE_LABELS_AXIS_7, labelpad=4)
        y_formatter = ScalarFormatter(useOffset=False)  # To avoid scientific notation w/ offset
        ax.yaxis.set_major_formatter(y_formatter)

        ax.text(0.98, 0.95, f"Optimal number of features: {results.n_features_}",
                horizontalalignment='right', verticalalignment='top', transform=ax.transAxes,
                size=theme.FONTSIZE_INFO_LARGE_8, color=theme.COLOR_TXT_INFO_LARGE,
                backgroundcolor=theme.COLOR_BG_INFO_LARGE)

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()
