"""

Find extreme events based on percentiles

"""
import fnmatch

import matplotlib.gridspec as gridspec
# matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.font_manager import FontProperties

import gui.elements
import gui.plotfuncs
from utils.vargroups import *


# pd.set_option('display.width', 1000)
# pd.set_option('display.max_columns', 15)
from PyQt5 import QtWidgets as qw
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

from gui import elements
from gui import plotfuncs


class AddControlsToMain():

    def __init__(self, opt_stack, opt_frame, opt_layout, ref_stack, ctx):
        self.opt_stack = opt_stack
        self.opt_frame = opt_frame
        self.opt_layout = opt_layout
        self.ref_stack = ref_stack
        self.ctx = ctx
        self.add_option()
        self.add_refinements()

    def add_option(self):
        # One option button_qual_A that shows the refinements when pressed
        self.opt_btn = elements.add_button_to_grid(grid_layout=self.opt_layout,
                                                   txt='Net GHG Balance', css_id='',
                                                   row=7, col=0, rowspan=1, colspan=1)

        self.opt_stack.addWidget(self.opt_frame)

    def add_refinements(self):
        # The plot is created in a new tab, i.e. only the button_qual_A that later links to the tab
        # creation is needed here.
        ref_frame, ref_layout = elements.add_frame_grid()
        self.ref_stack.addWidget(ref_frame)

        gui.elements.add_header_to_grid_top(layout=ref_layout, txt='AN: Net GHG Balance')

        self.ref_btn_showInNewTab = \
            elements.add_iconbutton_to_grid(grid_layout=ref_layout,
                                            txt='Show in New Tab', css_id='',
                                            row=1, col=0, rowspan=1, colspan=2,
                                            icon=self.ctx.icon_open_in_new_tab)

        ref_layout.setRowStretch(2, 1)


class MakeNewTab:
    """
        Connection between the main gui tab and the newly added tab.
        Creates the new tab for the plots and then adds the plots control elements
        (buttons, dropdowns) to the new tab.
    """

    def __init__(self, TabWidget, data_df, col_list_pretty, new_tab_id):

        self.TabWidget = TabWidget
        self.colname_pretty_string = col_list_pretty
        self.data_df = data_df

        linebreak = '\n'
        self.title = f"Net GHG{linebreak}Balance {new_tab_id}"  # used in tab name and header

        # Create tab menu (refinements)
        self.drp_co2_flux_var, self.drp_n2o_flux_var, self.drp_ch4_flux_var, self.btn_start, \
        self.ref_frame = \
            self.create_ref_tab_menu()

        # Create and add new tab
        self.canvas, self.figure, self.axes_dict = self.create_tab()

        # Update the gui dropdown menus of the new tab
        self.update_refinements()

    def get_handles(self):
        return self.drp_co2_flux_var, self.drp_n2o_flux_var, self.drp_ch4_flux_var, self.btn_start, \
               self.figure, self.axes_dict

    def create_tab(self):
        tabContainerVertLayout, tab_ix = \
            self.TabWidget.add_new_tab(title=self.title)
        self.TabWidget.setCurrentIndex(tab_ix)  ## select newly created tab

        # HEADER (top): Create and add
        elements.add_label_to_layout(txt=self.title, css_id='lbl_Header2', layout=tabContainerVertLayout)

        # MENU (left): Stack for refinement menu
        left_tabMenu_ref_stack = qw.QStackedWidget()
        left_tabMenu_ref_stack.setProperty('labelClass', '')
        left_tabMenu_ref_stack.setContentsMargins(0, 0, 0, 0)
        left_tabMenu_ref_stack.addWidget(self.ref_frame)

        # PLOT & NAVIGATION CONTROLS (right frame)
        # in horizontal layout

        # Figure (right frame)
        tabFigure = plt.Figure(facecolor='white')
        # tabFigure = plt.Figure(facecolor='#29282d')
        tabCanvas = FigureCanvas(tabFigure)

        # Make axes
        axes_dict = self.make_axes_dict(tabFigure=tabFigure)

        # Navigation (right frame)
        tabToolbar = NavigationToolbar(tabCanvas, parent=self.TabWidget)

        # Frame for figure & navigation (right frame)
        right_frm_fig_nav = qw.QFrame()
        lytVert_fig_nav = qw.QVBoxLayout()  # create layout for frame
        right_frm_fig_nav.setLayout(lytVert_fig_nav)  # assign layout to frame
        lytVert_fig_nav.setContentsMargins(0, 0, 0, 0)
        lytVert_fig_nav.addWidget(tabCanvas)  # add widget to layout
        lytVert_fig_nav.addWidget(tabToolbar)

        # ASSEMBLE MENU AND PLOT
        # Splitter for menu on the left and plot on the right
        tabSplitter = qw.QSplitter()
        tabSplitter.addWidget(left_tabMenu_ref_stack)  ## add ref menu first (left)
        tabSplitter.addWidget(right_frm_fig_nav)  ## add plot & navigation second (right)
        tabSplitter.setStretchFactor(0, 1)
        tabSplitter.setStretchFactor(1, 2)  ## stretch right more than left
        tabContainerVertLayout.addWidget(tabSplitter, stretch=1)  ## add Splitter to tab layout

        return tabCanvas, tabFigure, axes_dict

    def make_axes_dict(self, tabFigure):

        gs = gridspec.GridSpec(2, 2)  # rows, cols
        gs.update(wspace=0.2, hspace=0.2, left=0.03, right=0.97, top=0.97, bottom=0.03)

        # plots
        ax_main = tabFigure.add_subplot(gs[0, 0:2])
        ax_cake = tabFigure.add_subplot(gs[1, 0])
        # ax_TA_vs_USTAR_subclass_avg_season_2 = tabFigure.add_subplot(
        #     gs[0, 3], sharex=ax_TA_vs_USTAR_subclass_avg_season_1, sharey=ax_TA_vs_USTAR_subclass_avg_season_1)

        axes_dict = {'ax_main': ax_main,
                     'ax_cake': ax_cake}

        for key, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)

        return axes_dict

    def create_ref_tab_menu(self):
        ref_frame, ref_layout = elements.add_frame_grid()

        # ----------------------------------------------------------------------------
        # GHG Variables
        # ----------------------------------------------------------------------------
        gui.elements.add_header_in_grid_row(layout=ref_layout, txt='Select GHG Fluxes', row=0)

        drp_co2_flux_var = elements.grd_LabelDropdownPair(txt='CO2 Flux',
                                                          css_ids=['', 'cyan'],
                                                          layout=ref_layout,
                                                          row=1, col=0,
                                                          orientation='horiz')

        drp_n2o_flux_var = elements.grd_LabelDropdownPair(txt='N2O Flux',
                                                          css_ids=['', 'cyan'],
                                                          layout=ref_layout,
                                                          row=2, col=0,
                                                          orientation='horiz')

        drp_ch4_flux_var = elements.grd_LabelDropdownPair(txt='CH4 Flux',
                                                          css_ids=['', 'cyan'],
                                                          layout=ref_layout,
                                                          row=3, col=0,
                                                          orientation='horiz')

        ref_layout.addWidget(qw.QLabel(), 4, 0, 1, 1)  # spacer

        # ----------------------------------------------------------------------------
        # RUN
        # ----------------------------------------------------------------------------
        btn_start = elements.add_button_to_grid(grid_layout=ref_layout,
                                                txt='Run', css_id='btn_cat_ControlsRun',
                                                row=5, col=0, rowspan=1, colspan=2)

        # ----------------------------------------------------------------------------
        # STRETCH
        # ----------------------------------------------------------------------------
        ref_layout.setRowStretch(6, 1)  # empty row
        # ref_layout.setColumnStretch(2, 1)  # empty row

        return drp_co2_flux_var, drp_n2o_flux_var, drp_ch4_flux_var, btn_start, ref_frame

    def update_refinements(self):
        self.drp_co2_flux_var.clear()
        self.drp_n2o_flux_var.clear()
        self.drp_ch4_flux_var.clear()

        # Add all variables to drop-down lists to allow full flexibility yay
        for ix, colname_tuple in enumerate(self.data_df.columns):
            if any(fnmatch.fnmatch(colname_tuple[0], flux_id) for flux_id in FLUXES_GENERAL_CO2):
                self.drp_co2_flux_var.addItem(self.colname_pretty_string[ix])
            elif any(fnmatch.fnmatch(colname_tuple[0], flux_id) for flux_id in FLUXES_GENERAL_N2O):
                self.drp_n2o_flux_var.addItem(self.colname_pretty_string[ix])
            elif any(fnmatch.fnmatch(colname_tuple[0], flux_id) for flux_id in FLUXES_GENERAL_CH4):
                self.drp_ch4_flux_var.addItem(self.colname_pretty_string[ix])

        # # Set dropdowns to found ix
        # default_var_ix = 0
        # self.drp_main_var.setCurrentIndex(default_var_ix)
        # self.drp_add_var1.setCurrentIndex(default_var_ix)
        # self.drp_count_var.setCurrentIndex(default_var_ix)


class Run:
    mol_weight_co2 = 44.01  # g mol-1
    mol_weight_n2o = 44.013
    mol_weight_ch4 = 16.04

    gwp100_n2o = 298
    gwp100_ch4 = 34

    gwp100_col = ('GWP', '[g co2-eq m-2 30min-1]')
    gwp100_cum_col = ('GWP_cum', '[g co2-eq m-2]')

    def __init__(self, co2_flux_var, n2o_flux_var, button_run, col_dict_tuples, data_df, fig,
                 col_list_pretty, axes_dict, ch4_flux_var):
        self.axes_dict = axes_dict
        self.col_dict_tuples = col_dict_tuples
        self.col_list_pretty = col_list_pretty
        self.data_df = data_df
        self.fig = fig
        self.co2_flux_var = self.get_colname_tuples(colname_pretty=co2_flux_var)
        self.n2o_flux_var = self.get_colname_tuples(colname_pretty=n2o_flux_var)
        self.ch4_flux_var = self.get_colname_tuples(colname_pretty=ch4_flux_var)
        self.button_run = button_run
        # self.add_var1_data_col_renamed = (self.n2o_flux_var[0] + '_ADD', self.n2o_flux_var[1])

        # Assemble needed data
        self.netghg_df = self.make_netghg_df(dropna=True)

        gwp_df = self.convert_to_co2_eq()
        # self.pie_df = self.calc_offset(gwp_df=gwp_df)

        self.plot_timeseries_cum(gwp_df=gwp_df)
        # self.plot_offset_pie()

    def calc_offset(self, gwp_df):

        co2_cum = gwp_df[self.co2_flux_var][-1]
        n2o_cum = gwp_df[self.n2o_flux_var][-1]
        ch4_cum = gwp_df[self.ch4_flux_var][-1]

        if (co2_cum < 0) & (n2o_cum > 0) & (ch4_cum > 0):
            # todo i think calc not yet correct
            n2o_offset_perc = abs((n2o_cum / co2_cum) * 100)
            ch4_offset_perc = abs((ch4_cum / co2_cum) * 100)
            co2_after_offsets_perc = 100 - n2o_offset_perc - ch4_offset_perc

        pie_df = pd.DataFrame({'offsets': [co2_after_offsets_perc, n2o_offset_perc, ch4_offset_perc]},
                              index=['CO2 uptake', 'N2O offset', 'CH4 offset'])

        return pie_df

    def plot_offset_pie(self):
        ax_cake = self.axes_dict['ax_cake']
        ax_cake.clear()
        plot = self.pie_df.plot.pie(y='offsets', ax=ax_cake)
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def convert_to_co2_eq(self):

        # for half-hourly data

        # Convert to mol m-2 30min-1

        # To mol m-2 s-1
        gain_umol_to_mol = 1 / 1000000
        gwp_df = self.netghg_df.multiply(gain_umol_to_mol)  # convert from umol to mol

        # To mol m-2 30min-1
        gain_persec_to_per30min = 1 * 60 * 30
        gwp_df = gwp_df.multiply(gain_persec_to_per30min)  # convert from s-1 to 30min-1

        # To g m-2 30min-1
        gwp_df[self.co2_flux_var] = gwp_df[self.co2_flux_var].multiply(self.mol_weight_co2)
        gwp_df[self.n2o_flux_var] = gwp_df[self.n2o_flux_var].multiply(self.mol_weight_n2o)
        gwp_df[self.ch4_flux_var] = gwp_df[self.ch4_flux_var].multiply(self.mol_weight_ch4)

        # To g CO2-eq m-2 30min-1
        gwp_df[self.n2o_flux_var] = gwp_df[self.n2o_flux_var].multiply(self.gwp100_n2o)
        gwp_df[self.ch4_flux_var] = gwp_df[self.ch4_flux_var].multiply(self.gwp100_ch4)

        # Overall *cumulative* Net GHG Balance
        gwp_df = gwp_df.cumsum()
        gwp_df[self.gwp100_col] = gwp_df.iloc[:, :].sum(axis=1)
        # gwp_df[self.gwp100_cum_col] = gwp_df[self.gwp100_col].cumsum()

        return gwp_df

        # _netghg_df.head(5)

    def plot_timeseries_cum(self, gwp_df):
        ax_main = self.axes_dict['ax_main']

        ax_main.clear()

        # ax.plot_date(df.index, y=df[self.gwp100_cum_col], c='#FFB300', alpha=0.9,
        #              markeredgecolor='none', marker='o', ls='-', lw=1,
        #              label='{}'.format(self.gwp100_cum_col))

        gwp_df.plot(alpha=0.8, ax=ax_main,
                    markeredgecolor='none', marker='o', ls='-', lw=1)

        gui.plotfuncs.default_legend(ax=ax_main)

        # ax.text(0.05, 1.05, "Counts Of {}".format(col[0]),
        #         horizontalalignment='left', verticalalignment='top', transform=ax.transAxes,
        #         size='x-large', color='#999c9f', backgroundcolor='none')

        # ax.set_title('YYY', color='#999c9f', size='x-large')

        # Update Figure during calculations
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def get_colname_tuples(self, colname_pretty):
        # selected_cols_pretty = [self.main_var_data_col, self.add_var1_data_col, self.count_var_data_col]
        # selected_cols_tuples = []
        # for col in selected_cols_pretty:
        col_pretty_ix = self.col_list_pretty.index(colname_pretty)  # get ix in pretty list
        colname_tuple = self.col_dict_tuples[col_pretty_ix]  # use ix to get col name as tuple
        return colname_tuple

    def make_netghg_df(self, dropna):
        netghg_df = pd.DataFrame(data=self.data_df[[self.co2_flux_var, self.n2o_flux_var, self.ch4_flux_var]])
        if dropna:
            netghg_df.dropna(inplace=True)
        return netghg_df
