# matplotlib.use('Qt5Agg')
import datetime as dt
import fnmatch

import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import numpy as np
import pandas as pd
from PyQt5 import QtCore as qtc, QtWidgets as qw
from PyQt5 import QtGui
from PyQt5.QtGui import QDoubleValidator
from pandas.plotting import register_matplotlib_converters

import gui.add_to_layout
import gui.base
import gui.make
from pkgs.dfun.frames import export_to_main
from utils.vargroups import *

pd.plotting.register_matplotlib_converters()  # Needed for time plotting

import gui.elements
import gui.plotfuncs
import logger
from modboxes.plots.styles.LightTheme import *


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVLP')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_outlier_detection))

        # Add settings menu contents
        self.dte_start, self.dte_end, self.btn_calc, self.btn_add_as_new_var, \
        self.btn_add_timerange, self.btn_add_timerange, self.drp_define_nighttime_based_on, \
        self.drp_set_nighttime_if, self.lne_nighttime_threshold, \
        self.drp_apply_to_daynight, self.lne_trim_perc, self.drp_expand_selection = \
            self.add_settings_fields()

        # Populate settings fields
        self.populate_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # # Add plots
        # self.axes_dict = self.populate_plot_area()

    def populate_plot_area(self):
        return self.add_axes()

    def add_axes(self):
        pass

    def populate_settings_fields(self):
        self.drp_define_nighttime_based_on.clear()

        default_nighttime_ix = 0
        default_nighttime_ix_found = False

        # Add all variables to drop-down lists to allow full flexibility yay
        for ix, colname_tuple in enumerate(self.tab_data_df.columns):
            self.drp_define_nighttime_based_on.addItem(self.col_list_pretty[ix])

            # Check if column appears in the global vars
            if any(fnmatch.fnmatch(colname_tuple[0], vid) for vid in NIGHTTIME_DETECTION):
                default_nighttime_ix = ix if not default_nighttime_ix_found else default_nighttime_ix
                default_nighttime_ix_found = True

        # Set dropdowns to found ix
        self.drp_define_nighttime_based_on.setCurrentIndex(default_nighttime_ix)

    def add_settings_fields(self):
        onlyDouble = QDoubleValidator()  # Allow only doubles as input (floats, integers, no text)

        # Settings
        gui.elements.add_header_subheader_to_grid_top(layout=self.sett_layout,
                                                      row=0, col=0, rowspan=1, colspan=2,
                                                      txt=["Trim",
                                                           "Outlier Detection"])
        gui.add_to_layout.add_spacer_item_to_grid(
            row=1, col=0, layout=self.sett_layout)

        # Trim amount (percentiles)
        gui.elements.add_header_info_pair_in_grid_row(
            row=2, col=0, layout=self.sett_layout,
            txt='Trim', txt_info_hover='Data are trimmed by x%. For example, 1% removes data'
                                       'above the 99th percentile AND data below the 1st percentile.')
        lne_trim_perc = gui.elements.add_label_linedit_pair_to_grid(
            txt='Trim Data By (%)', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=3, col=0, orientation='horiz')
        lne_trim_perc.setText('1')
        lne_trim_perc.setValidator(onlyDouble)
        gui.add_to_layout.add_spacer_item_to_grid(
            row=4, col=0, layout=self.sett_layout)

        # Time range
        gui.elements.add_header_info_pair_in_grid_row(
            row=5, col=0, layout=self.sett_layout,
            txt='Time Range', txt_info_hover='Select time range to trim')
        dte_start = gui.elements.grd_LabelDateEditPair(
            txt='Time Range Start (YYYY-MM-DD)', css_ids=['', ''], layout=self.sett_layout,
            row=6, col=0)
        dte_end = gui.elements.grd_LabelDateEditPair(
            txt='Time Range End (YYYY-MM-DD)', css_ids=['', ''], layout=self.sett_layout,
            row=7, col=0)
        btn_add_timerange = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='+ Add Time Range', css_id='btn_teal',
            row=8, col=1, rowspan=1, colspan=1)
        gui.add_to_layout.add_spacer_item_to_grid(
            row=9, col=0, layout=self.sett_layout)

        # Selection
        gui.elements.add_header_info_pair_in_grid_row(
            row=10, col=0, layout=self.sett_layout,
            txt='Selection', txt_info_hover='Expand selection to include more data,'
                                            ' e.g. select same months over multiple years')
        drp_expand_selection = gui.elements.grd_LabelDropdownPair(
            txt='Expand Selection', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=11, col=0, orientation='horiz')
        drp_expand_selection.addItems(['Only Selected Time Ranges',
                                       'Same Months In All Years'])
        gui.add_to_layout.add_spacer_item_to_grid(
            row=12, col=0, layout=self.sett_layout)

        # Day / night
        gui.elements.add_header_in_grid_row(
            row=13, layout=self.sett_layout, txt='Day / Night')
        drp_apply_to_daynight = gui.elements.grd_LabelDropdownPair(
            txt='Apply To', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=14, col=0, orientation='horiz')
        drp_apply_to_daynight.addItems(['Daytime And Nighttime Data',
                                        'Only Daytime Data',
                                        'Only Nighttime Data'])
        drp_define_nighttime_based_on = gui.elements.grd_LabelDropdownPair(
            txt='Define Nighttime Based On', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=15, col=0, orientation='horiz')
        drp_set_nighttime_if = gui.elements.grd_LabelDropdownPair(
            txt='Set To Nighttime If', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=16, col=0, orientation='horiz')
        drp_set_nighttime_if.addItems(['Smaller Than Threshold', 'Larger Than Threshold'])
        lne_nighttime_threshold = gui.elements.add_label_linedit_pair_to_grid(
            txt='Threshold', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=17, col=0, orientation='horiz')
        lne_nighttime_threshold.setText('20')
        lne_nighttime_threshold.setValidator(onlyDouble)
        gui.add_to_layout.add_spacer_item_to_grid(self.sett_layout, 18, 0)

        drp_apply_to_daynight.setDisabled(True)
        drp_define_nighttime_based_on.setDisabled(True)
        drp_set_nighttime_if.setDisabled(True)
        lne_nighttime_threshold.setDisabled(True)

        # Buttons
        btn_calc = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='Preview', css_id='',
            row=19, col=0, rowspan=1, colspan=2)
        btn_add_as_new_var = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='+ Add As New Variable',
            css_id='btn_add_as_new_var',
            row=20, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(21, 1)

        return dte_start, dte_end, btn_calc, btn_add_as_new_var, btn_add_timerange, \
               btn_add_timerange, drp_define_nighttime_based_on, drp_set_nighttime_if, \
               lne_nighttime_threshold, drp_apply_to_daynight, lne_trim_perc, drp_expand_selection


class Run(addContent):
    color_list = colorwheel_36()  # get some colors
    sub_outdir = "outlier_detection_trim"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Outlier Detection: Trim', dict={}, highlight=True)  # Log info
        self.vars_available_df = self.tab_data_df.copy()
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.trim_df = pd.DataFrame()
        self.timeranges_df = pd.DataFrame(columns=['start', 'end', 'timedelta'])
        self.axes_dict = None
        self.target_col = None
        self.target_isset = False
        self.manage_fields()
        self.axes_dict = self.make_axes_dict()

    def add_to_rangelist(self):
        """Add time range to list"""
        self.get_settings_from_fields()
        self.add_timerange_to_selected()
        self.update_rangelist()

    def add_timerange_to_selected(self):
        """Collect selected time ranges in df"""
        self.timeranges_df.loc[len(self.timeranges_df)] = [self.start, self.end, self.end - self.start]

    def update_rangelist(self):
        """Show contents of timerange_df in list"""
        self.lst_rangelist_selected.clear()
        for idx, row in self.timeranges_df.iterrows():
            listitem = f"{str(row['start'])} to {str(row['end'])}"
            item = qw.QListWidgetItem(listitem)
            self.lst_rangelist_selected.addItem(item)
        self.manage_fields()

    def remove_from_rangelist(self):
        idx = self.lst_rangelist_selected.currentRow()
        self.timeranges_df.drop(idx, inplace=True)
        self.timeranges_df.reset_index(inplace=True, drop=True)
        self.update_rangelist()

    def set_colnames(self):
        self.target_preview_col = (f"_target_preview", self.target_col[1])
        self.inranges_vals_col = (f"_inranges_vals", self.target_col[1])
        self.outlier_vals_col = (f"_outlier_vals", self.target_col[1])  # Only outlier vals

    def manage_fields_timerange(self):
        if self.timeranges_df.empty and not self.target_isset:
            self.btn_calc.setDisabled(True)
            self.btn_add_as_new_var.setDisabled(True)
        elif not self.timeranges_df.empty and self.target_isset:
            self.btn_calc.setEnabled(True)
            self.btn_add_as_new_var.setEnabled(True)

    def manage_fields(self):
        """Activate and deactivate GUI fields"""
        self.get_settings_from_fields()
        self.manage_fields_timerange()

    def keep_data_within_timeranges(self, df: pd.DataFrame, timeranges: pd.DataFrame):
        """
        Keep data in series that is within the time ranges given in timeranges

            Data outside the given time ranges is set to NaN.

        Parameters
        ----------
        df: pd.Series
            Time series of data
        timeranges: pd.DataFrame
            Provides start and end of time ranges

        Returns
        -------
        pd.DataFrame

        """
        for idx, row in timeranges.iterrows():
            # Data rows that are within time range
            _filter = (df.index.date >= row['start']) \
                      & (df.index.date <= row['end'])
            # Get these rows from target
            df.loc[_filter, [self.inranges_vals_col]] = df[self.target_col]

        return df

    def expand_data_selection(self, timeranges: pd.DataFrame, series: pd.Series):
        _unique_years = series.index.year.unique()  # ALL years in series
        expanded_timeranges = pd.DataFrame(columns=['start', 'end', 'timedelta'])
        for idx, row in timeranges.iterrows():
            for _year in _unique_years:
                add_start = dt.datetime(_year, row['start'].month, row['start'].day)
                add_end = add_start + pd.Timedelta(row['timedelta'])
                timedelta = add_end - add_start
                # add_end = dt.datetime(_year, row['end'].month, row['end'].day)
                expanded_timeranges.loc[len(expanded_timeranges)] = [add_start.date(), add_end.date(), timedelta]
        expanded_timeranges = expanded_timeranges.drop_duplicates(subset=['start', 'end'], keep="first")
        return expanded_timeranges

    def calc(self):
        self.get_settings_from_fields()

        self.trim_df = pd.DataFrame()
        self.trim_df[self.target_col] = self.tab_data_df[self.target_col].copy()
        self.trim_df[self.inranges_vals_col] = np.nan
        self.trim_df[self.outlier_vals_col] = np.nan
        self.trim_df[self.target_preview_col] = self.trim_df[self.target_col].copy()

        # Expand time ranges
        if self.expand_selection == 'Only Selected Time Ranges':
            pass
        elif self.expand_selection == 'Same Months In All Years':
            self.timeranges_df = self.expand_data_selection(timeranges=self.timeranges_df.copy(),
                                                            series=self.trim_df[self.target_col].copy())
            self.update_rangelist()

        # Set data outside time ranges to missing
        self.trim_df = \
            self.keep_data_within_timeranges(df=self.trim_df.copy(),
                                             timeranges=self.timeranges_df)

        # Calc limits from vals in ranges
        self.trim_df[self.outlier_vals_col] = \
            self.trim_data(trim_series=self.trim_df[self.inranges_vals_col].copy(),
                           trim_perc=self.trim_perc)

        # Remove outlier vals from target
        _filter = self.trim_df[self.outlier_vals_col].isnull()
        self.trim_df.loc[~_filter, [self.target_preview_col]] = np.nan  # Inverse filter

        self.make_fig()
        self.btn_add_as_new_var.setEnabled(True)

    def trim_data(self, trim_series: pd.Series, trim_perc: float):
        # Calc limits from vals in ranges
        _lowerlim = trim_series.quantile(trim_perc)
        _upperlim = trim_series.quantile(1 - trim_perc)
        _filter = (trim_series <= _lowerlim) | (trim_series > _upperlim)
        trimmed_series = trim_series.loc[_filter]
        return trimmed_series

    def prepare_export(self):
        pass

    def export_single_column(self, main_df):
        """
        Set to missing in target variable and insert as new column

        Parameters
        ----------
        main_df

        Returns
        -------
        main_df:    The original input df with one column added. The column added
                    is the target var but with selected time periods set to missing.

        """
        export_df = self.trim_df[[self.target_preview_col]].copy()
        _new_col = (self.target_col[0] + '+odTR', self.target_col[1])
        export_df.rename(columns={self.target_preview_col: _new_col}, inplace=True)

        main_df = export_to_main(main_df=main_df,
                                 export_df=export_df,
                                 tab_data_df=self.tab_data_df)  # Return results to main
        self.btn_add_as_new_var.setDisabled(True)
        return main_df

    def get_selected(self, main_df):
        """Return modified class data back to main data"""
        self.get_settings_from_fields()
        main_df = self.export_single_column(main_df=main_df)
        return main_df

    def update_dynamic_fields(self):
        """ Set line edit default entry and add dropdown options. """
        min_data_day_str = self.trim_df.index[0].strftime("%Y-%m-%d")  # str
        max_data_day_str = self.trim_df.index[-1].strftime("%Y-%m-%d")
        min_data_day_qt = qtc.QDate.fromString(min_data_day_str, 'yyyy-MM-dd')  # Qt format
        max_data_day_qt = qtc.QDate.fromString(max_data_day_str, 'yyyy-MM-dd')
        self.dte_start.setDate(min_data_day_qt)
        self.dte_end.setDate(max_data_day_qt)

    def make_axes_dict(self):
        # Setup grid for multiple axes
        gs = gridspec.GridSpec(2, 1)  # rows, cols
        gs.update(wspace=0.3, hspace=0.3, left=0.03, right=0.97, top=0.97, bottom=0.03)
        ax_main = self.fig.add_subplot(gs[0, 0])
        ax_preview = self.fig.add_subplot(gs[1, 0], sharex=ax_main)
        axes_dict = {'ax_main': ax_main, 'ax_preview': ax_preview}
        return axes_dict

    def update_target(self):
        """Set data for target"""
        self.trim_df = pd.DataFrame()  # Reset
        self.target_col = self.get_selected_var(varlist=self.lst_varlist_available)
        self.set_colnames()
        self.trim_df[self.target_col] = self.tab_data_df[self.target_col].copy()
        self.trim_df[self.target_preview_col] = np.nan
        self.update_dynamic_fields()
        self.make_fig()
        self.target_isset = True
        self.manage_fields()

    def make_fig(self):
        for ax in self.fig.axes:
            ax.lines = []

        for col in self.trim_df:
            args = dict(series=self.trim_df[col], fig=self.fig)
            if self.trim_df[col].empty:
                continue
            if col == self.target_col:
                ax = self.axes_dict['ax_main']
                self.plot_ts(**args, ax=ax, label=f"{self.target_col[0]}")
            elif col == self.inranges_vals_col:
                ax = self.axes_dict['ax_main']
                self.plot_ts(**args, ax=ax, color='orange', label="values in selected range(s)")
            elif col == self.outlier_vals_col:
                ax = self.axes_dict['ax_main']
                self.plot_ts(**args, ax=ax, color='red', label="outliers: values outside range")

            if col == self.target_preview_col:
                ax = self.axes_dict['ax_preview']
                self.plot_ts(**args, ax=ax, color='#607D8B', label="preview")

        for ax_name, ax in self.axes_dict.items():
            ax.set_xlim(self.trim_df.index[0], self.trim_df.index[-1])
            gui.plotfuncs.default_legend(ax=ax, labelspacing=1)
            locator = mdates.AutoDateLocator(minticks=3, maxticks=30)
            formatter = mdates.ConciseDateFormatter(locator)
            ax.xaxis.set_major_locator(locator)
            ax.xaxis.set_major_formatter(formatter)
            self.fig.canvas.draw()
            self.fig.canvas.flush_events()

        self.axes_dict['ax_main'].text(0.02, 0.95, f"FOCUS: {self.target_col[0]}",
                                       size=FONTSIZE_HEADER_AXIS, color=FONTCOLOR_HEADER_AXIS,
                                       backgroundcolor='#FFE082', transform=self.axes_dict['ax_main'].transAxes,
                                       alpha=1,
                                       horizontalalignment='left', verticalalignment='top', zorder=99)

    def plot_ts(self, series, ax, fig, color='#90A4AE', label=None):
        """Plot time series of target var"""
        ax.plot_date(x=series.index, y=series, color=color,
                     alpha=1, ls='-', marker='o', markeredgecolor='none', ms=4, zorder=98,
                     label=label)

    def get_selected_var(self, varlist):
        """Get column name from dropdown selection"""
        selected_var = varlist.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = self.col_dict_tuples[selected_var_ix]
        # selected_pretty = self.col_list_pretty[selected_var_ix]  # Needs new pretty list (screen names)
        return selected_col

    def get_settings_from_fields(self):
        """Get selected settings from fields"""
        self.start = self.dte_start.date().toPyDate()
        self.end = self.dte_end.date().toPyDate()
        self.trim_perc = float(self.lne_trim_perc.text()) / 100
        self.define_nighttime_based_on_col = self.get_col_from_drp_pretty(drp=self.drp_define_nighttime_based_on)
        self.set_nighttime_if = self.drp_set_nighttime_if.currentText()
        self.expand_selection = self.drp_expand_selection.currentText()
        self.nighttime_threshold = float(self.lne_nighttime_threshold.text())
        self.apply_to_daynight = self.drp_apply_to_daynight.currentText()

    def get_col_from_drp_pretty(self, drp):
        """ Set target to selected variable. """
        target_pretty = drp.currentText()
        target_pretty_ix = self.col_list_pretty.index(target_pretty)
        target_col = self.col_dict_tuples[target_pretty_ix]
        return target_col


_help_drp_incomplete_data_rows = \
    "XXX"
