import numpy as np

import modboxes.plots._shared
from modboxes.analyses import gapfinder


# pd.set_option('display.width', 1000)
# pd.set_option('display.max_columns', 15)
# pd.set_option('display.max_rows', 999)


def show_in_plot(self, additional_line):
    # time focus_series are added to an existing plot, no new axis

    if additional_line:
        # Auxiliary lines
        marker = ''
        modboxes.Plots._shared.add_to_existing_ax(self.fig, add_to_ax=self.axes_dict, x=self.focus_df.index,
                                                  y=self.focus_df[self.running_simple], linestyle='--', linewidth=1)

    # Auxiliary lines
    modboxes.Plots._shared.add_to_existing_ax(self.fig, add_to_ax=self.axes_dict, x=self.focus_df.index,
                                              y=self.focus_df[self.gap_values_col],
                                              linestyle='', linewidth=3)


def linear_interpolation(df, measured_col, limit, filled_col, interpolated_values_col,
                         initialize_filled_col=False, fqual_col=False, fqual_id=False):
    # Fill measured values into column that will be filled
    if initialize_filled_col:
        df[filled_col] = np.nan
        df[filled_col].fillna(df[measured_col], inplace=True)

    # Analyze gaps in filled_col
    gapfinder_agg_df = gapfinder.GapFinder(df=df, col=filled_col, limit=limit).get_results()

    # Interpolation is done using measured data
    # First, ALL gaps are temporarily filled with interpolated data.
    _temp_gapfilled = \
        df[measured_col].interpolate(method='linear', limit=None,
                                     limit_area='inside', limit_direction='both')

    # Second, the info from gapfinder_agg_df is used to keep the desired gap-filled values
    # Iterate over each row in gapfinder_agg_df and check the range of the found gap
    # (a gap can span many records, depending on limit settings).
    # The range info is then used to fill the respective interpolated values from
    # _temp_gapfilled into focus_df.
    df[interpolated_values_col] = np.nan
    for ix, row in gapfinder_agg_df.iterrows():
        gap_start = row['min']
        gap_end = row['max']
        mask = (df.index >= gap_start) & (df.index <= gap_end)
        df.loc[mask, interpolated_values_col] = _temp_gapfilled[mask]
        if fqual_id:
            df.loc[mask, fqual_col] = fqual_id

    # Create complete time series, with measured and newly gap-filled values
    df[filled_col].fillna(df[interpolated_values_col], inplace=True)

    return df

# # https://stackoverflow.com/questions/43077166/interpolate-only-if-single-nan
# s[s.isnull()] = (s.shift(-1) + s.shift(1)) / 2
