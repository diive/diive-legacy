import gui.elements
import gui.plotfuncs
import modboxes.plots._shared
from gui import elements
from modboxes.gapfilling import _shared


class AddControls():
    """ Creates the gui control elements and their handles for usage. """

    def __init__(self, opt_stack, opt_frame, opt_layout, ref_stack):
        self.opt_stack = opt_stack
        self.opt_frame = opt_frame
        self.opt_layout = opt_layout
        self.ref_stack = ref_stack

        self.add_option()
        self.add_refinements()

    def add_option(self):
        # Option Button: [3] Running Median
        self.opt_btn = elements.add_button_to_grid(grid_layout=self.opt_layout,
                                                   txt='Linear Interpolation (Simple)', css_id='',
                                                   row=2, col=0, rowspan=1, colspan=1)
        self.opt_stack.addWidget(self.opt_frame)

    def add_refinements(self):
        ref_frame, ref_layout = elements.add_frame_grid()
        self.ref_stack.addWidget(ref_frame)

        gui.elements.add_header_to_grid_top(layout=ref_layout, txt='GF: Linear Interpolation (Simple)')

        self.ref_lne_limit = \
            elements.add_label_linedit_pair_to_grid(txt='Limit (values)', css_ids=['', 'cyan'], layout=ref_layout,
                                                    row=1, col=0, orientation='horiz')

        self.ref_btn_showInPlot = elements.add_button_to_grid(grid_layout=ref_layout,
                                                              txt='Show in Plot', css_id='',
                                                              row=2, col=0, rowspan=1, colspan=2)

        ref_layout.setRowStretch(3, 1)


class Call:
    def __init__(self, fig, ax, focus_df, focus_col, limit):
        self.fig = fig  # needed for redraw
        self.ax = ax  # adds to existing axis
        self.measured_col = focus_col
        self.limit = limit  # number of allowed consecutive gaps

        # Prepare focus_df
        self.focus_df = focus_df.copy()
        self.make_required_cols()

        self.ax = gui.plotfuncs.remove_prev_lines(ax=self.ax)

        self.run()

    def run(self):
        self.focus_df = _shared.linear_interpolation(df=self.focus_df, measured_col=self.measured_col,
                                                     limit=self.limit, filled_col=self.filled_col,
                                                     interpolated_values_col=self.interpolated_values_col,
                                                     initialize_filled_col=True)
        self.show_in_plot()

    def make_required_cols(self):
        # Define column names
        self.interpolated_values_col = (self.measured_col[0] + '_interpolated_values_col', self.measured_col[1])
        self.filled_col = (self.measured_col[0] + '+gfLIS', self.measured_col[1])
        self.gaplen_below_limit_col = 'gaplen_below_lim'

    def show_in_plot(self):
        # Auxiliary lines
        modboxes.Plots._shared.add_to_existing_ax(self.fig, add_to_ax=self.ax, x=self.focus_df.index,
                                                  y=self.focus_df[self.interpolated_values_col],
                                                  linestyle='', linewidth=3)

    def get_results(self):
        return self.focus_df, self.filled_col, self.limit
