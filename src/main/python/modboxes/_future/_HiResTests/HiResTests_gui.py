from gui import elements
from modboxes.Extensions._HiResTests import StationarityM98


def connect(self, opt_tab_idx):
    # (ModBox) Buttons
    self.btn_hrt_cat_StationarityM98_showStack.clicked.connect((lambda: self.view_stk_option(tab_idx=opt_tab_idx)))

    # Option: Stationarity Mahrt 1998
    self.btn_hrt_opt_StationarityM98.clicked.connect(
        lambda: self.view_stk_refinement(tab_idx=self.hrt_ref_StationarityM98_stackTab))

    self.btn_hrt_ref_StationarityM98_showInNewTab.clicked.connect(lambda: self.make_tab(make_tab='STATIONARITY_M98'))

    return self


class AddControls:
    """
        Creates gui for outlier detection
    """

    def __init__(self, grd_Categories, stk_Options, stk_Refinements, ctx):
        super(AddControls, self).__init__()
        self.ctx = ctx

        # (1) Categories
        self.grd_Categories = grd_Categories
        self.category = 'HiRes Tests'
        self.add_category()

        # (2) Options & Refinements
        opt_frame, opt_layout = elements.add_frame_grid()
        self.add_options(layout=opt_layout)

        # Linear Interpolation
        self.hrt_ref_StationarityM98_stackTab = 17
        self.btn_hrt_opt_StationarityM98, \
        self.btn_hrt_ref_StationarityM98_showInNewTab = \
            StationarityM98.AddControls(opt_stack=stk_Options, opt_frame=opt_frame, opt_layout=opt_layout,
                                        ref_stack=stk_Refinements,
                                        ctx=self.ctx).get_handles()

    def get_handles(self):
        return self.btn_hrt_cat_StationarityM98_showStack, \
               self.hrt_ref_StationarityM98_stackTab, \
               self.btn_hrt_opt_StationarityM98, \
               self.btn_hrt_ref_StationarityM98_showInNewTab

    def add_category(self):
        # Category button_qual_A in categories grid (for stack selection)
        self.btn_hrt_cat_StationarityM98_showStack = elements.add_iconbutton_to_grid(
            grid_layout=self.grd_Categories,
            txt=self.category,
            css_id='',
            row=4, col=0, rowspan=1, colspan=1,
            icon=self.ctx.icon_cat_hires_tests)
        return self

    def add_options(self, layout):
        _header = elements.grd_Label(lyt=layout,
                                     txt=self.category,
                                     css_id='lbl_Header2',
                                     row=0, col=0, rowspan=1, colspan=1)

        layout.setRowStretch(2, 1)

        return self
