lne_TA_numClasses_txt_info_hover = \
    "[...] the data set is split into six temperature classes of\n" \
    "equal sample size (according to quantiles) [...]\n" \
    "-Source: Papale et al. (2006), p574"

lne_uStar_numClasses_txt_info_hover = \
    "[...] for each temperature class, the set is split into 20 equally sized ustar-classes.\n" \
    "-Source: Papale et al. (2006), p574"

lne_flux_plateau_thres_perc_txt_info_hover = \
    "The threshold is defined as the ustar-class where the average night-time flux\n" \
    "reaches more than 99% of the average flux at the higher ustar-classes.\n" \
    "-Source: Papale et al. (2006), p574"

lne_ta_ustar_corr_thres_txt_info_hover = \
    "The threshold is only accepted if for the temperature class, temperature and ustar\n" \
    "are not or only weakly correlated (|r|<0.4).\n" \
    "-Source: Papale et al. (2006), p574"

lne_set_to_ustar_data_percentile_txt_info_hover = \
    "In cases where no ustar-threshold could be found in any of the 3-month periods with\n" \
    "this approach, it is set to the 90% percentile of the data (i.e. a minimum 10% of\n" \
    "the data are retained).\n" \
    "-Source: Papale et al. (2006), p574"

lne_allowed_min_ustar_threshold_txt_info_hover = \
    "A minimum threshold is set to 0.1 ms−1 for forest canopies and 0.01 ms−1 for short\n" \
    "vegetation sites that commonly have lower ustar values [...].\n" \
    "-Source: Papale et al. (2006), p574"

drp_flux_plateau_method_txt_info_hover = \
    "[...] the mean NEE value in each of the 20 USTAR classes is compared to the mean NEE\n"\
    "measured in the 10 higher USTAR classes. The threshold selected is the USTAR class in\n"\
    "which the average nighttime NEE reaches more than 99% of the average NEE at the higher\n"\
    "USTAR classes [OPTION: 10]. An improvement of the MP method was implemented here for\n"\
    "robustness over noisy data, by adding a second step to the original MP implementation:\n"\
    "when a threshold is selected, it was tested to ensure it was also valid for the following\n"\
    "USTAR class [OPTION: 10+10].\n"\
    "-Source: Pastorello et al. (2020), in review"

stability_parameter_monin_obukhov = \
    "Monin-Obukhov stability parameter, describes atmospheric stratification.\n" \
    "z ... measuring height\n" \
    "d ... zero plane displacement height\n" \
    "L ... Obukhov length, relates dynamic, thermal and buoyant processes\n" \
    "Source:\n" \
    "    Aubinet, M., Vesala, T., & Papale, D. (Eds.). (2012). Eddy Covariance:\n" \
    "        A Practical Guide to Measurement and Data Analysis. Springer Netherlands.\n" \
    "        https://doi.org/10.1007/978-94-007-2351-1"

ssitc = \
    "0-1-2 flagging system used in the CarboEurope IP project.\n" \
    "Based on the 1-9 flagging system described in Foken (2005).\n" \
    "Scheme after the Spoleto agreement 2004:\n" \
    "   0 ... high-quality fluxes\n" \
    "   1 ... moderate-quality fluxes, no restrictions for use in long term\n" \
    "         observation programs\n" \
    "   2 ... low-quality fluxes, don't use, gap-filling necessary\n" \
    "Source:\n" \
    "   Foken, T., et al. (2005). Post-Field Data Quality Control. In X. Lee,\n" \
    "       W. Massman, & B. Law (Eds.), Handbook of Micrometeorology (Vol. 29, pp. 181–208).\n" \
    "       Kluwer Academic Publishers. https://doi.org/10.1007/1-4020-2265-4_9\n" \
    "    Foken, T., and Mauder, M. (2011). Documentation and Instruction\n" \
    "       Manual of the Eddy-Covariance Software Package TK3. \n" \
    "       Arbeitsergebnisse Nr. 46, Univ. Bayreuth\n" \
    "       https://epub.uni-bayreuth.de/2130/\n" \
    "       http://www.bayceer.uni-bayreuth.de/mm/de/software/software/software_dl.php?id_obj=96792"

