_help_lne_num_estimators = \
    "The number of trees in the forest. (sklearn)"
_help_drp_criterion = \
    "The function to measure the quality of a split.\n" \
    "Supported criteria are “mse” for the mean squared error,\n" \
    "which is equal to variance reduction as feature selection\n" \
    "criterion, and “mae” for the mean absolute error. (sklearn)"
_help_lne_max_depth = \
    "The maximum depth of the tree. If None, then nodes are\n" \
    "expanded until all leaves are pure or until all leaves\n" \
    "contain less than min_samples_split samples. (sklearn)"
_help_lne_min_samples_split = \
    "The minimum number of samples required to split an internal node.\n" \
    "min_samples_split must be an integer greater than 1 or a float in (0.0, 1.0] (sklearn)"
_help_lne_min_samples_leaf = \
    "The minimum number of samples required to be at a leaf node.\n" \
    "min_samples_leaf must be at least 1 or in (0, 0.5] (sklearn)"
_help_lne_min_weight_fraction_leaf = \
    "The minimum weighted fraction of the sum total of weights\n" \
    "(of all the input samples) required to be at a leaf node.\n" \
    "Samples have equal weight when sample_weight is not provided. (sklearn)"
_help_drp_max_features = \
    "The number of features to consider when looking for the best split. (sklearn)\n" \
    "(Option to use int or float not implemented here.)"
_help_lne_max_leaf_nodes = \
    "Grow trees with max_leaf_nodes in best-first fashion. Best nodes\n" \
    "are defined as relative reduction in impurity. If None then unlimited\n" \
    "number of leaf nodes. (sklearn)"
_help_lne_min_impurity_decrease = \
    "A node will be split if this split induces a decrease of the impurity\n" \
    "greater than or equal to this value. (sklearn)"
_help_drp_oob_score = \
    "Whether to use out-of-bag samples to estimate the R^2 on unseen data. (sklearn)"
_help_lne_n_jobs = \
    "The number of jobs to run in parallel. fit, predict, decision_path and\n" \
    "apply are all parallelized over the trees. None means 1 unless in a\n" \
    "joblib.parallel_backend context. -1 means using all processors. See\n" \
    "Glossary for more details. (sklearn)"
_help_lne_random_state = \
    "Controls both the randomness of the bootstrapping of the samples used when\n" \
    "building trees (if bootstrap=True) and the sampling of the features to consider\n" \
    "when looking for the best split at each node (if max_features < n_features).\n" \
    "See Glossary for details. (sklearn)"
_help_lne_verbose = \
    "Controls the verbosity when fitting and predicting. (sklearn)"
_help_drp_warm_start = \
    "When set to True, reuse the solution of the previous call to fit and add more\n" \
    "estimators to the ensemble, otherwise, just fit a whole new forest. See the\n" \
    "Glossary. (sklearn)"
_help_lne_ccp_alpha = \
    "Complexity parameter used for Minimal Cost-Complexity Pruning. The subtree with\n" \
    "the largest cost complexity that is smaller than ccp_alpha will be chosen. By default,\n" \
    "no pruning is performed. See Minimal Cost-Complexity Pruning for details. (sklearn)\n" \
    "Must be a positive float. If a negative float is given, it's positive counterpart\n" \
    "is used instead. (DIIVE)"
_help_lne_max_samples = \
    "If bootstrap is True, the number of samples to draw from X to train each base estimator. (sklearn)"