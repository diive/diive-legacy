_help_drp_estimator = \
    "A supervised learning estimator with a fit method that provides\n" \
    "information about feature importance either through a coef_ attribute\n" \
    "or through a feature_importances_ attribute. (sklearn)\n\n" \
    "A supervised learning estimator, with a 'fit' method that returns the\n" \
    "feature_importances_ attribute. Important features must correspond to\n" \
    "high absolute values in the feature_importances_. (boruta_py)\n\n" \
    "The number in brackets gives the number of estimators (DIIVE)."

_help_lne_step = \
    "If greater than or equal to 1, then step corresponds to the (integer)\n" \
    "number of features to remove at each iteration. If within (0.0, 1.0),\n" \
    "then step corresponds to the percentage (rounded down) of features to\n" \
    "remove at each iteration. Note that the last iteration may remove fewer\n" \
    "than step features in order to reach min_features_to_select. (sklearn)\n\n" \
    "Must be a positive number and is set to default 1 if a negative number\n" \
    "is given. (DIIVE)"

_help_lne_min_features_to_select = \
    "The minimum number of features to be selected. This number of features\n" \
    "will always be scored, even if the difference between the original\n" \
    "feature count and min_features_to_select isn’t divisible by step. (sklearn)"

_help_drp_cv = \
    "Determines the cross-validation splitting strategy. (sklearn)\n\n" \
    "Not all options from sklearn are implemented here. (DIIVE)"

_help_drp_scoring = \
    "A string (see model evaluation documentation) [...] (sklearn)\n\n" \
    "Not all options from sklearn are implemented here. (DIIVE)"

_help_lne_verbose = \
    "Controls verbosity of output. (sklearn)"

_help_lne_n_jobs = \
    "Number of cores to run in parallel while fitting across folds.\n" \
    "'None' means 1 unless in a joblib.parallel_backend context. '-1' means\n" \
    "using all processors. See Glossary for more details. (sklearn)\n\n" \
    "'-9999' translates to 'None'. (DIIVE)"

_help_drp_importance_getter = \
    "If ‘auto’, uses the feature importance either through a coef_ or\n" \
    "feature_importances_ attributes of estimator. (sklearn)\n\n" \
    "Not all options from sklearn are implemented here. (DIIVE)"

_help_lne_method_estimators = \
    "If int sets the number of estimators in the chosen ensemble method.\n" \
    "If 'auto' this is determined automatically based on the size of the\n" \
    "dataset. The other parameters of the used estimators need to be set\n" \
    "with initialisation. (boruta_py)\n\n" \
    "'-1' translates to 'auto'. (DIIVE)"

_help_lne_perc_threshold = \
    "Default: 100\n\n" \
    "Instead of the max we use the percentile defined by the user, to pick\n" \
    "our threshold for comparison between shadow and real features. The max\n" \
    "tend to be too stringent. This provides a finer control over this. The\n" \
    "lower perc is the more false positives will be picked as relevant but\n" \
    "also the less relevant features will be left out. The usual trade-off.\n" \
    "The default '100' is essentially the vanilla Boruta corresponding to\n" \
    "the max. (boruta_py)\n\n" \
    "Must be a number between 1 and 100. If a number < 1 is provided as input,\n" \
    "the minimum '1' will be used instead. If a number > 100 is provided,\n" \
    "the maximum '100' will be used. (DIIVE)"

_help_lne_alpha = \
    "Default: 0.05\n\n" \
    "Level at which the corrected p-values will get rejected in both\n" \
    "correction steps. (boruta_py)\n\n" \
    "Is set to '0.01' if a number < 0 is provided as input. (DIIVE)"

_help_drp_two_step = \
    "Default: True\n\n" \
    "If you want to use the original implementation of Boruta with\n" \
    "Bonferroni correction only set this to False. (boruta_py)"

_help_lne_max_iter = \
    "Default: 100\n\n" \
    "The number of maximum iterations to perform. (boruta_py)\n\n" \
    "Is set to '1' if input number < 1. (DIIVE)"
