# -*- coding: utf-8 -*-
import sys

# matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
from PyQt5 import QtCore as qtc
from PyQt5 import QtGui
from PyQt5 import QtWidgets as qw
from PyQt5.QtGui import QIcon
import gui
import gui.tabs
from gui.timerange_controls import TimeRangeControls

qw.QApplication.setAttribute(qtc.Qt.AA_EnableHighDpiScaling, True)  # enable highdpi scaling
qw.QApplication.setAttribute(qtc.Qt.AA_UseHighDpiPixmaps, True)  # use highdpi icons

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

# from gui.top_menu import TopMenu
from logger import QTextEditLogger



class Ui_MainWindow(object):
    """
        Prepares the raw gui, i.e. the canvas that is filled with content later.
    """

    def setupUi(self, MainWindow, ctx, version_info):
        self.ctx = ctx  # Application context, resources
        self.version_info = version_info

        # LAYOUT HIERARCHY
        #   > MainWindow (level 0)
        #       >> CentralWidget (QWidget) (level 1)
        #           >>> Container (QHBoxLayout) (level 2)
        #               >>>> Tabs  (QTabWidget) (level 3)
        #                   >>>>> Splitter (QSplitter) (level 4)
        #                       >>>>>> VariableList  (QFrame, QVBoxLayout) (level 5)
        #                       >>>>>> FocusArea     (QFrame, QVBoxLayout) (level 5)

        # ==================================================================
        # INIT COMPONENTS //////////////////////////////////////////////////
        # ==================================================================

        # Init MainWindow (level 0)
        # info = self.ctx.build_settings  # deprecated
        MainWindow.setWindowTitle(f"{self.ctx.build_settings['app_name']} "
                                  f"{self.ctx.build_settings['version']}")
        MainWindow.setWindowIcon(QIcon(self.ctx.icon_diive))
        # MainWindow.setWindowIcon(QIcon('gui/Icons/python_icon.png'))
        MainWindow.resize(999, 999)
        MainWindow.showMaximized()
        MainWindow.setContentsMargins(0, 0, 0, 0)

        # TOP MENU
        # --------
        self.TopMenu = gui.top_menu.TopMenu(main_win=MainWindow, ctx=self.ctx)

        # MAKE VARIABLE LIST AND FOCUS AREA
        # ---------------------------------
        self.frm_varlist, self.lne_filter_varlist, self.lst_varlist = self.assemble_variable_list()

        self.frm_focus_area, self.lbl_focus_varname, \
        self.plt_figure, self.plt_canvas, self.plt_toolbar \
            = self.assemble_focus_area()

        # Splitter
        splitter = gui.make.qsplitter()
        splitter.addWidget(self.frm_varlist)  # Left side
        splitter.addWidget(self.frm_focus_area)  # Right side
        splitter.setStretchFactor(0, 1)
        splitter.setStretchFactor(1, 3)

        # TAB
        # ---
        # Tab contains Splitter
        self.TabWidget = gui.tabs.QCustomTabWidget()
        self.TabWidget.addTab(splitter, 'VARIABLES')
        self.TabWidget.setTabIcon(0, QtGui.QIcon(self.ctx.tab_icon_variables))

        # CONTAINER
        # Container contains tab
        container_layout = gui.make.qlayout_horiz()
        container_layout.addWidget(self.TabWidget)

        # CENTRAL WIDGET
        # CentralWidget contains Container
        central_widget = gui.make.qwidget(parent=MainWindow, css_id='MainWindow')
        central_widget.setLayout(container_layout)

        # MainWindow contains CentralWidget
        MainWindow.setCentralWidget(central_widget)

    def assemble_variable_list(self):
        """Create variable list"""
        frame = gui.make.qframe(css_id='frm_VariableList', min_width=100)
        layout = gui.make.qlayout_vert()
        frame.setLayout(layout)

        lineedit = gui.make.qlineedit(css_id='search_box')
        layout.addWidget(lineedit)

        list = gui.make.qlist()
        layout.addWidget(list)
        return frame, lineedit, list

    def assemble_focus_area(self):
        """Create focus area that shows selected var"""

        # Container frame & layout
        frame = gui.make.qframe(css_id='frm_FocusArea', min_width=200)
        layout = gui.make.qlayout_vert(spacing=5)
        frame.setLayout(layout)

        # Assemble header frame
        header_frame = gui.make.qframe(css_id='frm_statsbox', min_width=100)
        header_layout = gui.make.qlayout_horiz()
        header_frame.setLayout(header_layout)

        header_varname = gui.make.qlabel(txt='*Please Select Variable*', css_id='lbl_Header1', max_width=1500)
        header_layout.addWidget(header_varname, stretch=1)

        # Add time range controls to header frame
        self.obj_time_range_controls = TimeRangeControls(layout=header_layout)

        # Overview plots area
        plots_frame = gui.make.qframe(css_id='frm_PlotTools')
        plots_layout = gui.make.qlayout_vert()
        plots_frame.setLayout(plots_layout)

        plots_figure = plt.Figure(facecolor='white')
        plots_canvas = FigureCanvas(plots_figure)
        plots_layout.addWidget(plots_canvas)

        plots_toolbar = NavigationToolbar(plots_canvas, parent=frame)
        plots_toolbar.setProperty('labelClass', 'frm_FocusTools')
        plots_layout.addWidget(plots_toolbar)

        # Horizontal sub-layout for statsboxes and logbox
        lower_layout_horiz = gui.make.qlayout_horiz()

        # -Statsboxes
        statsboxes_frame = gui.make.qframe(css_id='frm_statsbox')
        statsboxes_grid = gui.make.qlayout_grid()
        statsboxes_frame.setLayout(statsboxes_grid)

        # todo refactor
        self.add_statsboxes(layout=statsboxes_grid)

        # -Logbox
        logbox_frame = gui.make.qframe(css_id='frm_LogBox')
        logbox_layout = gui.make.qlayout_vert()
        logbox_frame.setLayout(logbox_layout)
        logbox_header = gui.make.qlabel(txt='LogBox (currently incomplete ...)', css_id='lbl_Header2')
        logbox_layout.addWidget(logbox_header)

        # todo refactor
        self.txt_OnScreenOut = QTextEditLogger(self, lyt=logbox_layout)

        # Assemble horizontal sub-layout
        lower_layout_horiz.addWidget(statsboxes_frame, stretch=10)
        lower_layout_horiz.addWidget(logbox_frame, stretch=0)

        # Assemble focus area
        layout.addWidget(header_frame, stretch=0)
        layout.addWidget(plots_frame, stretch=2)
        layout.addLayout(lower_layout_horiz, stretch=0)

        return frame, header_varname, plots_figure, plots_canvas, plots_toolbar

    def add_statsboxes(self, layout):
        # Adds labels for text and values to the grid lyt

        orientation = 'horiz'

        # # Row 0: Header
        # header1 = qw.QLabel('Stats')
        # header1.setProperty('labelClass', 'lbl_Header2')
        # layout.addWidget(header1, 0, 0, 1, 2)

        # Two different CSS classes, one for the text parts,
        # one for the values of the statsboxes
        css_id_txt = 'lbl_statsbox_txt'
        css_id_val = 'lbl_statsbox_val'
        css_ids = [css_id_txt, css_id_val]

        args = dict(col=0, css_ids=css_ids, layout=layout, orientation=orientation)
        self.lbl_startdate_val = \
            gui.elements.add_label_pair_to_grid_layout(txt='Start Date', row=0, **args)
        self.lbl_enddate_val = \
            gui.elements.add_label_pair_to_grid_layout(txt='End Date', row=1, **args)
        self.lbl_period_val = \
            gui.elements.add_label_pair_to_grid_layout(txt='Period', row=2, **args)

        args = dict(col=2, css_ids=css_ids, layout=layout, orientation=orientation)
        self.lbl_type_val = \
            gui.elements.add_label_pair_to_grid_layout(txt='Type', row=0, **args)
        self.lbl_nov_val = \
            gui.elements.add_label_pair_to_grid_layout(txt='Number Of Values', row=1, **args)
        self.lbl_missingvalues_val = \
            gui.elements.add_label_pair_to_grid_layout(txt='Missing Values', row=2, **args)
        self.lbl_missingvaluesperc_val = \
            gui.elements.add_label_pair_to_grid_layout(txt='Missing Values %', row=3, **args)

        args = dict(col=4, css_ids=css_ids, layout=layout, orientation=orientation)
        self.lbl_median_val = \
            gui.elements.add_label_pair_to_grid_layout(txt='Median', row=0, **args)
        self.lbl_mean_val = \
            gui.elements.add_label_pair_to_grid_layout(txt='Mean', row=1, **args)
        self.lbl_sd_val = \
            gui.elements.add_label_pair_to_grid_layout(txt='SD', row=2, **args)
        self.lbl_sdmean_val = \
            gui.elements.add_label_pair_to_grid_layout(txt='SD / Mean', row=3, **args)
        self.lbl_max_val = \
            gui.elements.add_label_pair_to_grid_layout(txt='Maximum', row=4, **args)
        self.lbl_p95_val = \
            gui.elements.add_label_pair_to_grid_layout(txt='Perc 95', row=5, **args)
        self.lbl_p75_val = \
            gui.elements.add_label_pair_to_grid_layout(txt='Perc 75', row=6, **args)
        self.lbl_p25_val = \
            gui.elements.add_label_pair_to_grid_layout(txt='Perc 25', row=7, **args)
        self.lbl_p05_val = \
            gui.elements.add_label_pair_to_grid_layout(txt='Perc 05', row=8, **args)
        self.lbl_min_val = \
            gui.elements.add_label_pair_to_grid_layout(txt='Minimum', row=9, **args)
        self.lbl_cumsum_val = \
            gui.elements.add_label_pair_to_grid_layout(txt='Cumulative Sum', row=10, **args)
        self.lbl_cumsum_val.setToolTip('cumulative sum')

        args = dict(col=6, css_ids=css_ids, layout=layout, orientation=orientation)
        self.lbl_run_id = gui.elements.add_label_pair_to_grid_layout(
            txt='Run ID', row=0, **args)
        self.lbl_dataset_freq_val = gui.elements.add_label_pair_to_grid_layout(
            txt='Frequency', row=1, **args)
        self.lbl_dataset_num_cols_val = gui.elements.add_label_pair_to_grid_layout(
            txt='Columns', row=2, **args)
        self.lbl_dataset_seasons_available_val = gui.elements.add_label_pair_to_grid_layout(
            txt='Seasons', row=3, **args)
        self.lbl_dataset_events_available_val = gui.elements.add_label_pair_to_grid_layout(
            txt='Events', row=4, **args)
        self.lbl_dataset_current_outdir = gui.elements.add_label_pair_to_grid_layout(
            txt='Output Dir', row=5, col=6, css_ids=['lbl_statsbox_txt', 'lbl_statsbox_val_trunc'],
            layout=layout, orientation=orientation)
        self.lbl_dataset_current_filetpye = gui.elements.add_label_pair_to_grid_layout(
            txt='File Type', row=6, col=6, css_ids=['lbl_statsbox_txt', 'lbl_statsbox_val_trunc'],
            layout=layout, orientation=orientation)
        # layout.setRowStretch(27, 3)

    def VBoxFrame(self, css_id, maxw):
        col = qw.QFrame()
        col.setProperty('labelClass', css_id)
        col.setContentsMargins(0, 0, 0, 0)
        col.setMaximumWidth(maxw)

        lyt = qw.QVBoxLayout()
        lyt.setContentsMargins(0, 0, 0, 0)
        lyt.setSpacing(0)

        return col, lyt


# todo https://stackoverflow.com/questions/28599068/changing-the-edge-color-of-zoom-rect-in-matplotlib
# drawRectangle in backend_qt5.py


def main():
    appp = qw.QApplication(sys.argv)
    appp.exec_()


if __name__ == '__main__':
    main()
