from PyQt5 import QtCore as qtc
from PyQt5 import QtWidgets as qtw

import gui
import pkgs.dfun as dfun
from help.tooltips import filetype_dialog as tooltips
from pkgs.filereader.filereader import ConfigFileReader

class SelectFileTypeSettings(gui.base.SelectFileTypeSettingsFile):
    """ Settings window is created in gui_building_blocks. """

    def __init__(self, dir_filetypes):
        super().__init__()

        # Initialize dict that stores settings_dict
        self.settings_dict = {}

        # Dialog window
        self.layout, self.listwidget = self.gui()

        # Search files and show them in list
        self.found_files_dict = self.search_files(dir_filetypes=dir_filetypes)

        # Make buttons in gui
        self.add_files_to_list()

    def get_filepath(self):
        selected_file = self.listwidget.selectedIndexes()
        selected_file_name = selected_file[0].data(qtc.Qt.UserRole)
        selected_file_path = self.found_files_dict[selected_file_name]
        return selected_file_path

    def add_files_to_list(self):
        file_idx = -1
        for filename, filepath in sorted(self.found_files_dict.items()):
            file_idx += 1
            temp_dict = ConfigFileReader(configfilepath=filepath).read()
            name_in_list = temp_dict['GENERAL']['NAME']  # todo

            info = '<h2>{}</h2>'.format(temp_dict['GENERAL']['NAME'])
            del temp_dict['GENERAL']['NAME']  # Remove from dict, we already used it

            info += '<p>{}</p>'.format(temp_dict['GENERAL']['DESCRIPTION'])
            del temp_dict['GENERAL']['DESCRIPTION']  # Remove from dict, we already used it

            info += '<p>{}</p>'.format(temp_dict['TIMESTAMP']['DESCRIPTION'])
            del temp_dict['TIMESTAMP']['DESCRIPTION']  # Remove from dict, we already used it

            # info += '<br/>'.join(['<b>%s</b>: %s' % (key, value) for (key, value) in temp_dict.items()])

            # Format filetype info as html
            for key in temp_dict.keys():
                info += f'<h3>{key}</h3>'
                for option, val in temp_dict[key].items():
                    info +=    f'<p><b>{option}</b>: {val}</p>'

            # Item for list
            self.listwidget = gui.lists.add_custom_item(text=filename,
                                                        filename=filename,
                                                        freq=temp_dict['DATA']['FREQUENCY'],
                                                        tags=temp_dict['GENERAL']['TAGS'],
                                                        listwidget=self.listwidget,
                                                        tooltip=info)

    def search_files(self, dir_filetypes):
        # dir_filetypes = Path(dir_filetypes) / 'inout' / 'FileTypes'
        found_files_dict = dfun.files.search_files(src_dir=dir_filetypes, ext='*.yml')
        return found_files_dict

    def gui(self):
        """
        Construct gui for filetypes window
        """

        left_layout, right_layout = self.setupUi(self, win_title='File Import Settings')

        gui.elements.add_label_to_layout(txt='Load File Type Settings', css_id='lbl_Header2', layout=left_layout)
        gui.elements.add_infobox_to_layout(txt=tooltips.filetype_dialog, css_id='infobox', layout=right_layout)

        # List of files
        filelist = gui.elements.add_list_to_layout(layout=left_layout, add_empty_string=False)
        filelist.setWordWrap(True)
        filelist.setAlternatingRowColors(True)

        # Button box: OK and Cancel
        self.button_box = qtw.QDialogButtonBox()
        self.button_box.setEnabled(True)
        self.button_box.setStandardButtons(qtw.QDialogButtonBox.Cancel | qtw.QDialogButtonBox.Ok)
        self.button_box.setObjectName("button_box")
        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)

        # Assemble
        left_layout.addWidget(filelist)
        left_layout.addWidget(self.button_box)

        return left_layout, filelist
