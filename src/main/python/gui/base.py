"""
BASE CLASSES FOR GUI
====================


"""

from PyQt5 import QtWidgets as qtw, QtWidgets as qw
from PyQt5.QtGui import QIcon
from PyQt5.QtWebEngineWidgets import QWebEngineView
from matplotlib import pyplot as plt
from matplotlib.backends.backend_qt5 import NavigationToolbar2QT as NavigationToolbar
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas

import gui
import gui.make


class CustomQDialogWindow(qtw.QDialog):
    """Base class for dialog window"""

    def setupUi(self, SimpleSettingsWindow, win_title, ctx, icon):
        SimpleSettingsWindow.setAccessibleName('')
        # SimpleSettingsWindow.setWindowFlags(qtc.Qt.FramelessWindowHint)
        SimpleSettingsWindow.setWindowTitle(win_title)
        SimpleSettingsWindow.setWindowIcon(icon)
        # SelectFileTypeSettingsFile.resize(1200, 800)
        SimpleSettingsWindow.setContentsMargins(0, 0, 0, 0)

        # Layout
        container_layout = gui.make.qlayout_horiz()
        self.setLayout(container_layout)

        # Frame
        container_frame = gui.make.qframe(css_id='SimpleSettingsWindowFrame')
        container_layout.addWidget(container_frame)

        # Grid
        grid = gui.make.qlayout_grid()
        container_frame.setLayout(grid)

        return grid


class buildTab:
    """
    Build empty tab

    Templates:
        4 columns: SPoVPoVPoV
            #1 settings (S)
            #2 plot area (P) on top of (o) selected variable (V)
            #3 plot area (P) on top of (o) selected variable (V)
            #4 plot area (P) on top of (o) selected variable (V)

        4 columns: SVVP
            #1 settings (S)
            #2 available variables (V)
            #3 selected variables (V)
            #4 plot area (P)

        4 columns: SVLP
            #1 settings (S)
            #2 available variables (V)
            #3 list of selected e.g. time ranges (L)
            #4 plot area (P)

        3 columns: SPoVPoV
            #1 settings (S)
            #2 plot area (P) on top of (o) selected variable (V)
            #3 plot area (P) on top of (o) selected variable (V)

        3 columns: SVonVP
            #1 settings (S)
            #2 available variables (V) on top of selected variables (V)
            #3 plot area (P)

        4 columns: SVaVaLP
            #1 settings (S)
            #2 list of available variables (V)
            #3 list of available variables (V)
            #4 list of selected time ranges (L)
            #5 plot area (P)

        4 columns: SVaVaP
            #1 settings (S)
            #2 list of available variables (V)
            #3 list of available variables (V)
            #5 plot area (P)

        3 columns: SVonVF
            #1 settings (S)
            #2 available variables (V) on top of selected variables (V)
            #3 frame that can be filled with widgets, e.g. webview (F)

        4 columns: SVonVPT
            #1 settings (S)
            #2 available variables (V) on top of selected variables (V)
            #3 plot area (P)
            #4 text area (T)

        3 columns: SVonVT
            #1 settings (S)
            #2 available variables (V) on top of selected variables (V)
            #3 text area (T)

        3 columns: SVP
            #1 settings (S)
            #2 available variables (V)
            #3 plot area

        3 columns: SVPwT
            #1 settings (S)
            #2 available variables (V)
            #3 plot area with table view (PwT)

        3 columns: SVV
            #1 settings (S)
            #2 available variables (V)
            #3 selected variables (V)

        2 columns: ST
            #1 settings (S)
            #2 table view (T)

        2 columns: SPwT
            #1 settings (S)
            #2 plot area with table view (PwT)

        2 columns: SP
            #1 settings (S)
            #2 plot area (P)
    """

    def __init__(self, app_obj, title, tab_id, tab_template):
        self.TabWidget = app_obj.TabWidget
        self.tab_template = tab_template
        self.col_list_pretty = app_obj.col_list_pretty
        self.col_dict_tuples = app_obj.col_dict_tuples
        self.tab_data_df = app_obj.data_df.copy()  # Data used in tab
        self.project_outdir = app_obj.project_outdir
        self.ctx = app_obj.ctx

        # Tab
        lyt_tab_vert, self.tab_ix = self.TabWidget.add_new_tab(title=f"{title} {tab_id}")
        self.TabWidget.setCurrentIndex(self.tab_ix)  # Select newly created tab

        # Templates
        splitter = None
        if tab_template == 'SVP':
            splitter = self.template_SVP()
        if tab_template == 'SVPwT':
            splitter = self.template_SVPwT()
        if tab_template == 'SVLP':
            splitter = self.template_SVLP()
        if tab_template == 'SVV':
            splitter = self.template_SVV()
        if tab_template == 'SVVP':
            splitter = self.template_SVVP()
        if tab_template == 'SVonVP':
            splitter = self.template_SVonVP()
        if tab_template == 'SVaVaLP':
            splitter = self.template_SVaVaLP()
        if tab_template == 'SVaVaP':
            splitter = self.template_SVaVaP()
        if tab_template == 'SVonVF':
            splitter = self.template_SVonVF()
        if tab_template == 'SVonVPT':
            splitter = self.template_SVonVPT()
        if tab_template == 'SVonVT':
            splitter = self.template_SVonVT()
        if tab_template == 'SPwT':
            splitter = self.template_SPwT()
        if tab_template == 'SP':
            splitter = self.template_SP()
        if tab_template == 'ST':
            splitter = self.template_ST()
        if tab_template == 'SPoVPoVPoV':
            splitter = self.template_SPoVPoVPoV()

        lyt_tab_vert.addWidget(splitter, stretch=1)  ## add Splitter to tab layout

    def template_SPoVPoVPoV(self):

        # Column 1
        frm_plot_on_varlist_1 = qw.QFrame()
        self.lyt_plot_on_varlist_1 = qw.QVBoxLayout()  # Make this also as self, to add buttons later
        frm_plot_on_varlist_1.setLayout(self.lyt_plot_on_varlist_1)
        self.frm_plot_area_1, self.fig_1, self.lyt_plot_area_1 = self.make_plot_area()
        self.frm_varlist_selected_1, self.lst_varlist_selected_1 = self.make_varlist(txt_header='Select Variable')
        self.lyt_plot_on_varlist_1.addWidget(self.frm_plot_area_1, stretch=3)
        self.lyt_plot_on_varlist_1.addWidget(self.frm_varlist_selected_1, stretch=1)

        # Column 2
        frm_plot_on_varlist_2 = qw.QFrame()
        self.lyt_plot_on_varlist_2 = qw.QVBoxLayout()  # Make this also as self, to add buttons later
        frm_plot_on_varlist_2.setLayout(self.lyt_plot_on_varlist_2)
        self.frm_plot_area_2, self.fig_2, self.lyt_plot_area_2 = self.make_plot_area()
        self.frm_varlist_selected_2, self.lst_varlist_selected_2 = self.make_varlist(txt_header='Select Variable')
        self.lyt_plot_on_varlist_2.addWidget(self.frm_plot_area_2, stretch=3)
        self.lyt_plot_on_varlist_2.addWidget(self.frm_varlist_selected_2, stretch=1)

        # Column 3
        frm_plot_on_varlist_3 = qw.QFrame()
        self.lyt_plot_on_varlist_3 = qw.QVBoxLayout()  # Make this also as self, to add buttons later
        frm_plot_on_varlist_3.setLayout(self.lyt_plot_on_varlist_3)
        self.frm_plot_area_3, self.fig_3, self.lyt_plot_area_3 = self.make_plot_area()
        self.frm_varlist_selected_3, self.lst_varlist_selected_3 = self.make_varlist(txt_header='Select Option')
        self.lyt_plot_on_varlist_3.addWidget(self.frm_plot_area_3, stretch=3)
        self.lyt_plot_on_varlist_3.addWidget(self.frm_varlist_selected_3, stretch=1)
        self.btn_varlist_3 = gui.elements.add_simplebutton_to_layout(layout=self.lyt_plot_on_varlist_3,
                                                                     style='teal', txt='Button')

        # Settings
        self.sett_menu, self.sett_layout = self.make_settings_menu()

        splitter = qw.QSplitter()
        splitter.addWidget(self.sett_menu)  # Settings
        splitter.addWidget(frm_plot_on_varlist_1)  # Plot and varlist
        splitter.addWidget(frm_plot_on_varlist_2)
        splitter.addWidget(frm_plot_on_varlist_3)
        splitter.setStretchFactor(0, 1)
        splitter.setStretchFactor(1, 1)
        splitter.setStretchFactor(2, 1)
        splitter.setStretchFactor(3, 1)
        return splitter

    def template_SVP(self):
        self.sett_menu, self.sett_layout = self.make_settings_menu()
        self.frm_varlist_available, self.lst_varlist_available = self.make_varlist(txt_header='Available Variables')
        self.frm_plot_area, self.fig, self.lyt_plot_area = self.make_plot_area()
        splitter = qw.QSplitter()
        splitter.addWidget(self.sett_menu)  # Settings menu (left)
        splitter.addWidget(self.frm_varlist_available)  # List of vars
        splitter.addWidget(self.frm_plot_area)  # List of vars
        splitter.setStretchFactor(0, 2)
        splitter.setStretchFactor(1, 1)
        splitter.setStretchFactor(2, 3)
        return splitter

    def template_SVPwT(self):
        self.sett_menu, self.sett_layout = self.make_settings_menu()
        self.frm_varlist_available, self.lst_varlist_available = self.make_varlist(txt_header='Available Variables')
        self.table_view = self.make_tableview()
        self.frm_plot_area, self.fig, self.lyt_plot_area = self.make_plot_area()

        # Collect tableview and plot area, show in same frame next to each other
        subframe, sublayout_horiz = self.make_horiz_frame()
        sublayout_horiz.addWidget(self.table_view, stretch=1)
        sublayout_horiz.addWidget(self.frm_plot_area, stretch=3)

        splitter = qw.QSplitter()
        splitter.addWidget(self.sett_menu)  # Settings menu (left)
        splitter.addWidget(self.frm_varlist_available)  # List of vars
        splitter.addWidget(subframe)  # Tableview + plot area
        splitter.setStretchFactor(0, 2)
        splitter.setStretchFactor(1, 1)
        splitter.setStretchFactor(2, 3)
        return splitter

    def template_SPwT(self):
        self.sett_menu, self.sett_layout = self.make_settings_menu()
        self.table_view = self.make_tableview()
        self.frm_plot_area, self.fig, self.lyt_plot_area = self.make_plot_area()

        # Collect tableview and plot area, show in same frame next to each other
        subframe, sublayout_horiz = self.make_horiz_frame()
        sublayout_horiz.addWidget(self.table_view, stretch=1)
        sublayout_horiz.addWidget(self.frm_plot_area, stretch=2)

        splitter = qw.QSplitter()
        splitter.addWidget(self.sett_menu)  # Settings menu (left)
        splitter.addWidget(subframe)  # tableview + plot area
        splitter.setStretchFactor(0, 1)
        splitter.setStretchFactor(1, 2)
        return splitter

    def template_SVLP(self):
        self.sett_menu, self.sett_layout = self.make_settings_menu()
        self.frm_varlist_available, self.lst_varlist_available = self.make_varlist(txt_header='Available Variables')
        self.frm_rangelist_selected, self.lst_rangelist_selected = self.make_varlist(txt_header='Selected Time Ranges')
        self.frm_plot_area, self.fig, self.lyt_plot_area = self.make_plot_area()
        splitter = qw.QSplitter()
        splitter.addWidget(self.sett_menu)  # Settings menu (left)
        splitter.addWidget(self.frm_varlist_available)  # List of vars
        splitter.addWidget(self.frm_rangelist_selected)  # List of vars
        splitter.addWidget(self.frm_plot_area)  # List of vars
        splitter.setStretchFactor(0, 2)
        splitter.setStretchFactor(1, 1)
        splitter.setStretchFactor(2, 1)
        splitter.setStretchFactor(3, 3)
        return splitter

    def template_SVonVP(self):
        # Settings
        self.sett_menu, self.sett_layout = self.make_settings_menu()

        # Var lists
        frm_varlists = qw.QFrame()
        self.lyt_varlists = qw.QVBoxLayout()  # Make this also as self, to add buttons later
        frm_varlists.setLayout(self.lyt_varlists)
        self.frm_varlist_available, self.lst_varlist_available = self.make_varlist(txt_header='Available Variables')
        self.frm_varlist_selected, self.lst_varlist_selected = self.make_varlist(txt_header='Selected Variables')
        self.lyt_varlists.addWidget(self.frm_varlist_available, stretch=2)
        self.lyt_varlists.addWidget(self.frm_varlist_selected, stretch=1)

        # Plot area
        self.frm_plot_area, self.fig, self.lyt_plot_area = self.make_plot_area()

        # Splitter
        splitter = qw.QSplitter()
        splitter.addWidget(self.sett_menu)  # Settings menu (left)
        splitter.addWidget(frm_varlists)  # Lists of vars
        splitter.addWidget(self.frm_plot_area)  # List of vars
        splitter.setStretchFactor(0, 2)
        splitter.setStretchFactor(1, 2)
        splitter.setStretchFactor(2, 4)  # Stretch right more than left
        return splitter

    def template_SVaVaLP(self):
        """2x available vars list + list of time ranges"""
        self.sett_menu, self.sett_layout = self.make_settings_menu()

        self.frm_varlist_target, self.lst_varlist_target = self.make_varlist(txt_header='Target')
        self.frm_varlist_reference, self.lst_varlist_reference = self.make_varlist(txt_header='Reference')
        self.frm_rangelist_selected, self.lst_rangelist_selected = self.make_varlist(txt_header='Fit Time Ranges')

        frm_varlists = qw.QFrame()
        self.lyt_varlists = qw.QHBoxLayout()  # Make this also as self, to add buttons later
        frm_varlists.setLayout(self.lyt_varlists)
        self.lyt_varlists.addWidget(self.frm_varlist_target, stretch=1)
        self.lyt_varlists.addWidget(self.frm_varlist_reference, stretch=1)
        self.lyt_varlists.addWidget(self.frm_rangelist_selected, stretch=1)

        self.frm_plot_area, self.fig, self.lyt_plot_area = self.make_plot_area()

        splitter = qw.QSplitter()
        splitter.addWidget(self.sett_menu)  # Settings menu (left)
        splitter.addWidget(frm_varlists)  # Lists of vars
        splitter.addWidget(self.frm_plot_area)  # List of vars
        splitter.setStretchFactor(0, 2)
        splitter.setStretchFactor(1, 1)
        splitter.setStretchFactor(2, 4)  # Stretch right more than left
        return splitter

    def template_SVaVaP(self):
        """2x available vars list"""
        self.sett_menu, self.sett_layout = self.make_settings_menu()

        self.frm_varlist_target, self.lst_varlist_target = self.make_varlist(txt_header='Target')
        self.frm_varlist_reference, self.lst_varlist_reference = self.make_varlist(txt_header='Reference')

        frm_varlists = qw.QFrame()
        self.lyt_varlists = qw.QHBoxLayout()  # Make this also as self, to add buttons later
        frm_varlists.setLayout(self.lyt_varlists)
        self.lyt_varlists.addWidget(self.frm_varlist_target, stretch=1)
        self.lyt_varlists.addWidget(self.frm_varlist_reference, stretch=1)

        self.frm_plot_area, self.fig, self.lyt_plot_area = self.make_plot_area()

        splitter = qw.QSplitter()
        splitter.addWidget(self.sett_menu)  # Settings menu (left)
        splitter.addWidget(frm_varlists)  # Lists of vars
        splitter.addWidget(self.frm_plot_area)  # List of vars
        splitter.setStretchFactor(0, 4)
        splitter.setStretchFactor(1, 1)
        splitter.setStretchFactor(2, 4)  # Stretch right more than left
        return splitter

    def template_SVonVF(self):
        """F = Frame only w/o widgets, but with layout"""
        self.sett_menu, self.sett_layout = self.make_settings_menu()
        self.frm_varlist_available, self.lst_varlist_available = self.make_varlist(txt_header='Available Variables')
        frm_varlists = qw.QFrame()
        self.lyt_varlists = qw.QVBoxLayout()  # Make this also as self, to add buttons later
        frm_varlists.setLayout(self.lyt_varlists)
        self.frm_varlist_selected, self.lst_varlist_selected = self.make_varlist(txt_header='Selected Variables')
        self.frm_plot_area, self.lyt_plot_area = self.make_frame_only()
        self.lyt_varlists.addWidget(self.frm_varlist_available, stretch=2)
        self.lyt_varlists.addWidget(self.frm_varlist_selected, stretch=1)
        splitter = qw.QSplitter()
        splitter.addWidget(self.sett_menu)  # Settings menu (left)
        splitter.addWidget(frm_varlists)  # Lists of vars
        splitter.addWidget(self.frm_plot_area)  # List of vars
        splitter.setStretchFactor(0, 2)
        splitter.setStretchFactor(1, 2)
        splitter.setStretchFactor(2, 4)  # Stretch right more than left
        return splitter

    def template_SVonVT(self):
        self.sett_menu, self.sett_layout = self.make_settings_menu()

        # Make varlists in sub-layout
        self.frm_varlist_available, self.lst_varlist_available = self.make_varlist(txt_header='Available Variables')
        frm_varlists = qw.QFrame()
        self.lyt_varlists = qw.QVBoxLayout()  # Make this also as self, to add buttons later
        frm_varlists.setLayout(self.lyt_varlists)
        self.frm_varlist_selected, self.lst_varlist_selected = self.make_varlist(txt_header='Selected Variables')
        self.frm_plot_area, self.fig, self.lyt_plot_area = self.make_plot_area()
        self.lyt_varlists.addWidget(self.frm_varlist_available, stretch=2)
        self.lyt_varlists.addWidget(self.frm_varlist_selected, stretch=1)

        frm_textbox = qw.QFrame()
        lyt_textbox = qw.QVBoxLayout()
        frm_textbox.setLayout(lyt_textbox)
        self.txe_textbox = qw.QTextEdit()
        lyt_textbox.addWidget(self.txe_textbox)

        # Add to Splitter
        splitter = qw.QSplitter()
        splitter.addWidget(self.sett_menu)  # Settings menu (left)
        splitter.addWidget(frm_varlists)  # Lists of vars
        # splitter.addWidget(self.frm_plot_area)
        splitter.addWidget(frm_textbox)  # Text area
        splitter.setStretchFactor(0, 0)
        splitter.setStretchFactor(1, 0)
        splitter.setStretchFactor(2, 1)
        return splitter

    def template_SVonVPT(self):
        self.sett_menu, self.sett_layout = self.make_settings_menu()

        # Make varlists in sub-layout
        self.frm_varlist_available, self.lst_varlist_available = self.make_varlist(txt_header='Available Variables')
        frm_varlists = qw.QFrame()
        self.lyt_varlists = qw.QVBoxLayout()  # Make this also as self, to add buttons later
        frm_varlists.setLayout(self.lyt_varlists)
        self.frm_varlist_selected, self.lst_varlist_selected = self.make_varlist(txt_header='Selected Variables')
        self.frm_plot_area, self.fig, self.lyt_plot_area = self.make_plot_area()
        self.lyt_varlists.addWidget(self.frm_varlist_available, stretch=2)
        self.lyt_varlists.addWidget(self.frm_varlist_selected, stretch=1)

        frm_textbox = qw.QFrame()
        lyt_textbox = qw.QVBoxLayout()
        frm_textbox.setLayout(lyt_textbox)
        self.txe_textbox = qw.QTextEdit()
        lyt_textbox.addWidget(self.txe_textbox)
        self.btn_save_to_txt_file = \
            gui.elements.add_button_to_layout(layout=lyt_textbox, txt="Save To Text File", css_id='btn_amber')

        # Add to Splitter
        splitter = qw.QSplitter()
        splitter.addWidget(self.sett_menu)  # Settings menu (left)
        splitter.addWidget(frm_varlists)  # Lists of vars
        splitter.addWidget(self.frm_plot_area)  # List of vars
        splitter.addWidget(frm_textbox)  # Text area
        splitter.setStretchFactor(0, 2)
        splitter.setStretchFactor(1, 2)
        splitter.setStretchFactor(2, 4)  # Stretch more than others
        splitter.setStretchFactor(3, 2)
        return splitter

    def template_SVV(self):
        self.sett_menu, self.sett_layout = self.make_settings_menu()
        self.frm_varlist_available, self.lst_varlist_available = self.make_varlist(txt_header='Available Variables')
        self.frm_varlist_selected, self.lst_varlist_selected = self.make_varlist(txt_header='Selected Variables')
        splitter = qw.QSplitter()
        splitter.addWidget(self.sett_menu)  # Settings menu (left)
        splitter.addWidget(self.frm_varlist_available)  # List of vars
        splitter.addWidget(self.frm_varlist_selected)  # List of vars
        splitter.addWidget(qw.QWidget())
        splitter.setStretchFactor(0, 1)
        splitter.setStretchFactor(1, 3)
        splitter.setStretchFactor(2, 3)
        splitter.setStretchFactor(3, 6)
        return splitter

    def template_SVVP(self):
        self.sett_menu, self.sett_layout = self.make_settings_menu()
        self.frm_varlist_available, self.lst_varlist_available = self.make_varlist(txt_header='Available Variables')
        self.frm_varlist_selected, self.lst_varlist_selected = self.make_varlist(txt_header='Selected Variables')
        self.frm_plot_area, self.fig, self.lyt_plot_area = self.make_plot_area()
        splitter = qw.QSplitter()
        splitter.addWidget(self.sett_menu)  # Settings menu (left)
        splitter.addWidget(self.frm_varlist_available)  # List of vars
        splitter.addWidget(self.frm_varlist_selected)  # List of vars
        splitter.addWidget(self.frm_plot_area)  # List of vars
        splitter.setStretchFactor(0, 2)
        splitter.setStretchFactor(1, 1)
        splitter.setStretchFactor(2, 1)
        splitter.setStretchFactor(3, 4)  # Stretch right more than left
        return splitter

    def template_ST(self):
        self.sett_menu, self.sett_layout = self.make_settings_menu()
        self.frm_tableview, self.table_view = self.make_tableview_in_frame()
        splitter = qw.QSplitter()
        splitter.addWidget(self.sett_menu)  # Settings menu (left)
        splitter.addWidget(self.frm_tableview)  # Frame with table view
        splitter.setStretchFactor(0, 1)
        splitter.setStretchFactor(1, 2)
        return splitter

    def template_SP(self):
        self.sett_menu, self.sett_layout = self.make_settings_menu()
        self.frm_plot_area, self.fig, self.lyt_plot_area = self.make_plot_area()
        splitter = qw.QSplitter()
        splitter.addWidget(self.sett_menu)  # Settings menu (left)
        splitter.addWidget(self.frm_plot_area)  # List of vars
        splitter.setStretchFactor(0, 1)
        splitter.setStretchFactor(1, 2)
        return splitter

    def make_tableview(self):
        """Make table view widget"""
        table_view = qw.QTableView()
        table_view.setSizeAdjustPolicy(qw.QAbstractScrollArea.AdjustToContents)
        # tabTable.setRowCount(4)
        # tabTable.setColumnCount(2)
        # self.lyt_plot_area.addWidget(table_view, stretch=1)
        return table_view

    @staticmethod
    def make_tableview_in_frame():
        """Add table view in frame"""
        frm_tableview = qw.QFrame()
        lyt_tableview = qw.QVBoxLayout()  # create layout for frame
        frm_tableview.setLayout(lyt_tableview)  # assign layout to frame
        lyt_tableview.setContentsMargins(0, 0, 0, 0)
        table_view = qw.QTableView()
        table_view.setSizeAdjustPolicy(qw.QAbstractScrollArea.AdjustToContents)
        lyt_tableview.addWidget(table_view, stretch=1)
        return frm_tableview, table_view

    def make_horiz_frame(self):
        frame = qw.QFrame()
        layout = qw.QHBoxLayout()  # Create layout for frame
        frame.setLayout(layout)  # Assign layout to frame
        layout.setContentsMargins(0, 0, 0, 0)
        return frame, layout

    def make_plot_area(self):
        """Frame and layout for plot(s)"""
        frm_plot_area = qw.QFrame()
        lyt_plot_area = qw.QVBoxLayout()  # create layout for frame
        frm_plot_area.setLayout(lyt_plot_area)  # assign layout to frame
        lyt_plot_area.setContentsMargins(0, 0, 0, 0)

        # Figure
        fig = plt.Figure(facecolor='white')
        canvas = FigureCanvas(fig)
        # axes_dict = self.make_axes_dict(tabFigure=fig)  # Make axes
        toolbar = NavigationToolbar(canvas, parent=self.TabWidget)  # Navigation (right)

        # Add elements to layout
        lyt_plot_area.addWidget(canvas, stretch=4)  # add widget to layout
        lyt_plot_area.addWidget(toolbar, stretch=0)

        return frm_plot_area, fig, lyt_plot_area

    def make_frame_only(self):
        """Frame e.g. for webview plot(s)"""
        frm_plot_area = qw.QFrame()
        lyt_plot_area = qw.QVBoxLayout()  # create layout for frame
        frm_plot_area.setLayout(lyt_plot_area)  # assign layout to frame
        lyt_plot_area.setContentsMargins(0, 0, 0, 0)
        return frm_plot_area, lyt_plot_area

    def make_settings_menu(self):
        """Settings menu (left)"""
        sett_frame, sett_layout = gui.elements.add_frame_grid()
        sett_menu = qw.QStackedWidget()
        sett_menu.setProperty('labelClass', '')
        sett_menu.setContentsMargins(0, 0, 0, 0)
        sett_menu.addWidget(sett_frame)
        return sett_menu, sett_layout

    def make_varlist(self, txt_header):
        """ Make the frame containing the variable list. """
        css_id = 'frm_VariableList'
        maxw = 600

        # Frame
        frm_varlist = qw.QFrame()
        frm_varlist.setProperty('labelClass', css_id)
        frm_varlist.setContentsMargins(0, 0, 0, 0)
        frm_varlist.setMaximumWidth(maxw)

        # List
        lyt_varlist = qw.QVBoxLayout()
        lyt_varlist.setContentsMargins(0, 0, 0, 0)
        lyt_varlist.setSpacing(0)

        gui.elements.add_label_to_layout(txt=txt_header, layout=lyt_varlist, css_id='lbl_Header2')
        lst_Features = gui.elements.add_list_to_layout(layout=lyt_varlist)
        lst_Features.setWordWrap(True)
        lst_Features.setAlternatingRowColors(True)
        frm_varlist.setLayout(lyt_varlist)

        return frm_varlist, lst_Features

    @staticmethod
    def find_free_tab_id(TabWidget, tab_ids):
        """
        Check if a tab with a name that contains all elements in tab_ids already exists
        and then append ID number to tab_name.
        """

        num_tabs = TabWidget.count()  ## number of tabs

        # Check if the plot tab already exists in the tabbar
        tabs_existing = []  # collects names of already exising tabs
        for tab_ix in range(num_tabs):
            if all(tab_id in TabWidget.tabText(tab_ix) for tab_id in tab_ids):
                tabs_existing.append(TabWidget.tabText(tab_ix))

        # Check which ids (int) are already in use
        ids_existing = []
        for tab_existing in tabs_existing:
            splits = tab_existing.split(' ')
            ids_existing.append(int(splits[-1]))

        # Check if list is empty, i.e. not ids exist yet
        if not ids_existing:
            new_tab_id = 1
        else:
            new_tab_id = max(ids_existing) + 1

        # # Alternatively: search for the next available (not used) id
        # start_id = 1
        # while start_id in ids_existing:
        #     start_id += 1
        # new_tab_id = start_id

        # num_tabs = len(tabs_existing)
        # new_tab_id = num_tabs + 1
        return new_tab_id

    @staticmethod
    def add_webview(to_layout):
        webview = QWebEngineView()
        webview.setContentsMargins(0, 0, 0, 0)
        # webview.setMaximumHeight(500)
        # webview_sankey.setMaximumHeight(maxh=10)
        to_layout.addWidget(webview, stretch=0)
        # self.lyt_plot_area.setStretch(10,990)
        return webview

    @staticmethod
    def populate_variable_list(obj):
        obj.lst_varlist_available.clear()
        list_cols = []
        # Add all variables to var list
        for ix, colname_tuple in enumerate(obj.tab_data_df.columns):
            item = qw.QListWidgetItem(obj.col_list_pretty[ix])
            obj.lst_varlist_available.addItem(item)  # add column name to list
            list_cols.append(colname_tuple)

    @staticmethod
    def populate_multiple_variable_lists(col_list_pretty, df, lists):
        """Add available variables to variable lists"""
        for list in lists:
            list.clear()
        # list_cols = []
        # Add all variables to var list
        for ix, colname_tuple in enumerate(df.columns):
            for list in lists:
                item = qw.QListWidgetItem(col_list_pretty[ix])
                list.addItem(item)  # add column name to list
                # list_cols.append(colname_tuple)

    @staticmethod
    def update_btn_status(obj, target=True, marker=True, export=True):
        if target:
            if obj.target_loaded:
                obj.btn_calc.setEnabled(True)
            else:
                obj.btn_calc.setDisabled(True)

        if marker:
            if obj.marker_isset:
                obj.btn_keep_marked.setEnabled(True)
                obj.btn_remove_marked.setEnabled(True)
            else:
                obj.btn_keep_marked.setDisabled(True)
                obj.btn_remove_marked.setDisabled(True)

        if export:
            if obj.ready_to_export:
                obj.btn_add_as_new_var.setEnabled(True)
            else:
                obj.btn_add_as_new_var.setDisabled(True)


class SelectFileTypeSettingsFile(qtw.QDialog):
    def setupUi(self, SelectFileTypeSettingsFile, win_title):
        SelectFileTypeSettingsFile.setAccessibleName('SettingsWindow')

        SelectFileTypeSettingsFile.setWindowTitle(win_title)
        SelectFileTypeSettingsFile.setWindowIcon(QIcon('gui/Icons/python_icon.png'))
        SelectFileTypeSettingsFile.resize(800, 600)
        SelectFileTypeSettingsFile.setContentsMargins(0, 0, 0, 0)

        # Container layout
        container_layout = qtw.QHBoxLayout()
        container_layout.setContentsMargins(0, 0, 0, 0)
        container_layout.setSpacing(0)
        self.setLayout(container_layout)

        # Left layout
        layout_left = qtw.QVBoxLayout()
        layout_left.setContentsMargins(0, 0, 0, 0)
        layout_left.setSpacing(0)

        # Right layout
        layout_right = qtw.QVBoxLayout()
        layout_right.setContentsMargins(0, 0, 0, 0)
        layout_right.setSpacing(0)

        # Assemble
        container_layout.addLayout(layout_left, stretch=1)
        container_layout.addLayout(layout_right, stretch=1)

        return layout_left, layout_right