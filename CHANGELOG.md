# Changelog

![DIIVE](images/logo_diive1_256px.png)



## v0.22.0 | 21 Sep 2021

### Start Of MVC Implementation

#### **GENERAL**

This update starts to implement the ``MVC`` (model-view-controller) software
design pattern throughout ``DIIVE``. The goal is to separate the GUI from all
underlying functionality. The benefit of this approach will be that functions
and methods implemented in ``DIIVE`` can be used without the GUI. The GUI will
be optional, not mandatory. 

Although this is quite a substantial undertaking it is necessary to facilitate
future extensions of the code base. Separating everything into little packages
will make working on the code much easier in the future.

The full implementation of the ``MVC`` design will take some time, but the goal
is to retain all functionality so that each future update still works as expected.
Current functions will be updated silently, old functions (i.e. functions that
have not been updated yet) remain active.

This update implements the ``MVC`` pattern for two packages. What that means is 
that e.g. the ``abslim`` package in ``pkgs.outlierdetection`` comprises one file
for the outlier calculation (``abslim.py``, the "model"), one file that creates
the GUI (the tab) with all required controls (``dvgui.py``, the "view", where
``dv`` stands for ``DIIVE``), and finally one file to connect the "model" with
the "view" (``dvcontroller.py``, the "controller").

In this update, I also had to remove some unfinished methods for now, but they
will be re-implemented soon, along with code improvements.

#### **PACKAGES**
- Added new subfolder ``pkgs`` (packages) that will contain all packages that are
  used in ``DIIVE``. In the future, it will be possible to access all functionality
  without using the GUI.
- New package ``dfun``: Data functions to work with data
- New package ``filereader``: Methods to read data files
- New package ``ml``: Will collect machine learning packages
- New package in ``ml``: ``randomforest_ts``, random forest regressor for time series. Is 
  not yet implemented in the GUI but functional as package without GUI.
- New package ``outlierdetection``: Algorithms for outlier detection 
- New package in ``outlierdetection``: ``abslim`` (Absolute Limits)
- New package in ``outlierdetection``: ``doublediff`` (Double-differenced Time Series)

#### **CHANGES**
- Code: Refactored tab creation and implemented ``MVC`` for ``Outlier Detection > Double-differenced Time Series``.
- Code: Refactored tab creation and implemented ``MVC`` for ``Outlier Detection > Absolute Limits``.
- Code: Moved ``dfun`` (data functions) to ``pkgs``
- Code: Filetypes are now generally defined in YAML files
- Code: Refactored filetype selection from YAML and dialog window
- Code: Refactored statsbox creation
- Code: Refactored tooltip generation
- Code: New conda environment
  
#### **REMOVALS**
- Removed incomplete methods from top menu:   
  - ``Outlier Detection > Seasonal Trend Decomposition (LOESS)``
  - ``Analyses > Before / After Event``
  - ``Analyses > Extreme Events Finder``
  - ``Analyses > Net GHG Balance``
  - ``Analyses > Running Linear Regression``
  - ``Gap-filling > Simple Linear Interpolation``
  - ``Gap-filling > Simple Linear Regression``
  - ``Create Variable > Flag``
  - ``Eddy Covariance > Off-season Uptake Correction``
  - ``Eddy Covariance > Ustar Detection``
  - ``Pipelines > ETH Flux QC``


## v0.21.0 | 20 Jun 2021

### Code Refactoring & Binning

![DIIVE v0.21.0](images/v0.21.0_binning_filetype_settings.png)

**NEW FEATURES**:
- `Create Var > Binning`: Convert data to x number of bins

**CHANGES**:

- Code: major refactoring of imports. Modboxes are now packages, e.g. `analyses` is now a package comprising 
all analyses modules. This is also one step closer to make packages available independently from and outside of DIIVE
- Code: Commonly used functions are now collected in a dedicated package `dfun` (data functions) 
- Code: The list of filetypes in `File > File Import Settings` is now using custom list widget items that
  allow for more flexibility
- Removed unneeded filetypes
- For `minutes`, abbreviation is now `MIN` (was `T` before)
- `Convert Units` was moved from `Modifications` to `Create Variable` and renamed to `Apply Gain`

**ADDITIONS**:
- GUI: `File > File Import Settings` now displays tags and was restructured a bit
- Several filetypes

**REACTIVATED**:
- Events are now again shown as shaded areas plus label in overview plots



### v0.20.0 | 17 Jun 2021

### Executable

**CHANGES**:
- Adjusted code to facilitate the creation of stand-alone executables using the newest version of the fman build system (`fbs Pro`, https://build-system.fman.io/).
- Removed `-alpha` from version number since it resulted in compilation errors using pyinstaller.
- `Eddy Covariance > EC Quality Control`: Removed sankey plot and added bar plots instead.

**ADDITIONS**:
- Added splash screen, shown during script start



## v0.19.0-alpha | 14 Jun 2021

### More Outlier Detection Methods

**NEW FEATURES**:
- `Outlier Detection > Trim`: Trims data based on upper/lower percentiles
- `Outlier Detection > Hampel Filter`

**ADDITIONS**:
- Overview plots: The histogram now shows mean and median

**CHANGES**:
- Renamed `Modifications > Set To Missing` to `Modifications > Remove Time Period`



## v0.18.0-alpha | 20 Apr 2021

### Lag Features And Improved Off-season Uptake Correction

![DIIVE v0.18.0-alpha](images/v0.18.0-alpha_off-season_uptake_correction.png)

**NEW FEATURES**:
- `Create Variable > Lag Features`: Create lagged variants of variables

**ADDITIONS**:
- `Eddy Covariance > Off-season Uptake Correction`:
  - Data can now be divided into classes of sonic temperature
  - The division into sonic temperature classes is done separately for daytime and nighttime
  - Results can be saved to a text file
  - Previous results can be loaded from a text file
- `Eddy Covariance > Flux Quality Control`: Results can now be saved to a text file
- Added file type: DIIVE csv in daily time resolution 

**CHANGES**:
- `Eddy Covariance > Flux Quality Control`: (z-d)/L stability check is no longer selected as default check
- The file extension of the DIIVE file format is now *.diive.csv (changed from *.amp.csv)
- Changed color of the histogram in overview plots (it is now, let's say, mint green)

**FRONTEND**:
- DIIVE should now start with the window maximized
- `File > File Import Settings`: Filetypes should now be shown in alphabetical order 
- `Plots > Heatmap`: Added header

**BUGFIXES**:
- `Analyses > Feature Selection`: Fixed axis labels for Boruta results




## v0.17.0-alpha | 30 Mar 2021
### Feature Selection, Improved Random Forest and more

![DIIVE v0.17.0-alpha](images/v0.17.0-alpha_overview_plots.png)

**SOMEWHAT IMPORTANT**
- Amp was renamed to **DIIVE**, who would have thought

**NEW FEATURES**:
- `Analyses > Feature Selection`: Allows to find out which variables are important. Currently
  implemented are `Boruta` and `Recursive Feature Elimination With Cross-Validation`
- `Modifications > Span Correction`: First version of span correction function. Compares a target
  variable to a reference variable calculates correction factors.  
- `Plots > Diel Cycles`: First version of diel cycle plotting, will be expanded in the next versions

**ADDITIONS**
- `Eddy Covariance > Off-season Uptake Correction`: implemented optimization algorithm for fraction
  parameter. Optimization aims to minimize the difference between a target (e.g. CO2 fluxes from
  an open path gas analyzer) and a reference variable (e.g. CO2 fluxes from an enclosed/closed path
  analyzer). 
- `Gap-filling > Random Forest`: added more parameters that can be adjusted via the settings panel,
  based on `scikit-learn`
- `Gap-filling > Random Forest`: refined usage of time-based variables
- `Gap-filling > Simple Running`: reactivated
- Added filetype: SUTRON meteo (.filetype)

**FRONTEND**
- Added tab icons, they even have different colors
- Current output directory is now shown next to statsbox `Output Dir`
- Currently selected file type is now shown next to statsbox `File Type`



## v0.16.0 | 21 Dec 2020

### Combine Columns and more

**NEW FEATURES**:
- Create Variable > Combine Columns: Allows to combine two columns by addition, subtraction and more.
- Modifications > Subset: Method to select (and keep) only a subset of the current dataset.
- Modifications > Set To Missing: Method to set selected variable to missing values, can be applied to the whole dataset.
- EC > Off-season Uptake Correction: Option to correct EC fluxes for open-path IRGAs, following WE's approach (R script)

**ADDITIONS**:
- Export > Export Dataset: Option to export different timestamps; one row header can now be exported without units, with variable names only
- Plot > Cumulative: Option to show multiple in same plot
- Outlier Detection > Double Differenced: Better nighttime detection, repeat until no more outliers found, refinements
- EC > Quality Control: Return flag details to main data. Since the overall quality flag (QCF) is the aggregate of multiple single flags, the results from each single flag are now returned to the dataset. 
- EC > Quality Control: Provide text info for values lost due to each quality check.

**FRONTEND**:
- Slightly improved representation of custom (modified) variables with symbols.

**OTHER**:
- Analyses: Implementation of first code for the seasonal trend decomposition using LOESS, also preparation to use it for outlier removal




## v0.15.0 | 16 Sep 2020

### Quality Control, Histogram, Quantiles and more

![v0.15.0_ECQualityControl](images/v0.15.0_ECQualityControl.jpg)


**This update includes v0.14.0 and v0.15.0**

**NEW FEATURES**:
- Plots > Histogram
- Plots > Quantiles
- EC QC > ECQualityControl: Powerful tool to create customized overall flags for EddyPro files. Contains the first dynamic plot in Amp in which you can drag and re-arrange elements.
- Modifications > Rename Variables: Tool to rename variables in the dataset, bulk renaming already possible for adding suffix/prefix to variable names or variable units
- Create Variable > Define Seasons

**ADDITIONS**:
- Plots > Heatmap: It is now possible to plot "flux stripes", which is useful especially for longer datasets spanning multiple years. This plots the daily average value for each DOY next to other years.
- Gap-Filling > Random Forest: Option to run different models for "groups", e.g. different gap-filling models for daytime and nighttime data
- Outlier Detection > Running: Added option to repeat outlier detection automatically until no more outliers are found
- StatsBoxes show if seasons are defined
- Dataset stats in StatsBoxes
- Several new filetypes

**CHANGES**:
- Modifications > Convert Units: First simple version of a unit conversion tool, formerly known as Apply Gain
- For ustar detection: refine code, nighttime detection, seasons, calculations
- Now in top menu: Create Variable > Add New Event
- Plots > Heatmap: Variable is now selected from list
- Analyses > Class Finder: Now in top menu
- Gap-Filling > Random Forest: Gap-filling is now done for all gaps for which all selected features are available, previously all features had to be available for all gaps. With this new approach the method now fills gaps "where possible".
- Outlier Detection > Absolute Limits: Reworked and now in top menu
- Outlier Detection > Running: Reworked and now in top menu
- Outlier Detection > IQR: Reworked and now in top menu
- Outlier Detection > Double Differenced Time Series: Reworked and now in top menu
- Analyses > Gap Finder: Reworked and now in top menu
- Create Variable > Time Since: Reworked and now in top menu
- The former methods Add Prefix / Suffix and Rename Variable have been combined in Modifications > Rename Variables
- Gap-filling > Lookup Table: Renamed from MDS, reworked and now in top menu


**FRONTEND**:
- Re-arranged main page, with stats now at the bottom, to give the main plots more space
- All buttons have been removed from the main area, they are now part of the methods in the top menu
- GUI adjustments to work better with high dpi scaling
- Some more colors

**BACKEND**:
- Major code adjustements to make my life easier in the future, more object-oriented, I assume it will now take longer to drive me crazy
- Harmonized and centralized plot legend creation

**DEACTIVATED (for now)**:
All these features are already implemented but are de-activated in this version. Not enough time I had.
- Analyses > ustar detection
- Analyses > Before / After Event
- Analyses > Net GHG Balance
- Analyses > Running Linear Regression
- Gap-filling > Running
- Gap-filling > Simple Linear Interpolation
- Gap-filling > Simple Linear Regression
- Pipelines
- Overview plots show seasons





## v0.13.0 | 9 Feb 2020

### Correlation Matrix and Wind Sectors

- NEW FEATURE: Correlation Matrix plot
- NEW FEATURE: Wind sector plots, shows e.g. mean of CO2 flux from defined wind sectors.
- NEW FEATURE: Cumulative plots now support multipanels.
- NEW: Settings for options in top menu now open in a pop-up window, where necessary.
- CHANGED: Several plotting options were moved to the top menu.
- CHANGED: Option to cut off the dataset range was moved to the top menu.
- CHANGED: Option to export variables to files was moved to the top menu and renamed to "Export Dataset".
- CHANGED: Option to create an event was moved to the top menu.
- CHANGED: Option to add a string prefix/suffix was moved to the top menu.
- CHANGED: Modified variables are now added with the prefix ">" in the variable list (instead of the dot ".").
- CHANGED: Tab names can now be displayed in 2 rows.
- IMPROVED: Files that have more header columns than data columns can now be loaded into Amp. Header columns for the missing headers are now automatically generated with "missing" and a timestamp.
- IMPROVED: Amp now remembers the last opened folder when a file was loaded.
- IMPROVED: Top menu is now a class.
- IMPROVED: Code for Ustar Threshold Plots (FLUXNET) now runs in tab/obj instances.
- IMPROVED: Code for Hexbin plots now runs in tab/obj instances.
- IMPROVED: Code how already existing tabs are checked, this check is now done versus a list e.g. ['Name1','Name2'] instead of a string such as 'Name1 Name2'.
- IMPROVED: Reduced the number of settings required in .filetype files, it is now easier to specify and include new file types.
- UPDATED: The conda environment was updated to include Matplotlib 3.1.2 (this version was needed for the correct display of the Correlation Matrix). Current conda env is AMPENV3.
- ADDED: .filetype for FLUXNET FullSet files @30T resolution.
- ADDED: .filetype for ReddyProc files @30T resolution.
- ADDED: New icons for open in new tab and for open in new window.
- FIXED: Some smaller bugs.

![Correlation Matrix](images/v0.13.0_correlation_matrix_20200209.jpg)

![Wind Sectors](images/v0.13.0_wind_sectors_20200209.jpg)



## v0.12.0 | 28 Jan 2020

### Multi-panel Plots and Middle Timestamp

- NEW FEATURE: Multi-panel plots
- CHANGED: Timestamp now shows middle of record
- CHANGED: Improved code to detect the timestamp middle at different time resolutions
- ADDED: Infobox in .filetype dialog

![v0.12.0_multipanel_plots.png](images/v0.12.0_multipanel_plots.png)



## v0.11.0 | 21 Jan 2020

### The Random Forest Update

- NEW FEATURE: implemented first machine learning algorithm into Amp: random forest (found in Gap-filling)
- NEW FEATURE: for the overview plots, it is now possible to restrict the shown data to a previously selected time range, e.g. 1-31 August 2019, with options in the upper right of the VARIABLES tab.
- NEW FEATURE: Amp now uses .filetype files that store settings for reading data files, this way new filetypes can easily be added by adding a new .filetype file to a specific folder.
- NEW FEATURE: the project output directory can now be selected via the new top menu. If no output folder was selected prior to exporting data, a default folder will be used instead.
- CHANGED: output directory is not immediately created by default, but only when something is exported
- CHANGED: added new menu drop-down menus in top menu bar that contains Load File, Add File and File Settings buttons
- CHANGED: limit lines, e.g. for showing outlier removal limits, are now again displayed as lines without markers
- IMPROVED: the window to select filetypes shows info about the filetypes in tooltips
- IMPROVED: the import of several Events should now work as expected. Each event is imported as new (boolean) column.
- IMPROVED: it is now possible to add a custom prefix to the output file name
- IMPROVED: sanitized the code here and there b/c else I go crazy
- IMPROVED: the SearchBox now also searches in units, not only variable names
- FIXED: several bugs were removed, and I am sure several others introduced
- PYTHON ENV: new conda env is AMP2, with updated versions of scikit and the pydot package



## v0.10.0 | 21 Dec 2019

### Features and Fixes

* NEW FEATURE: Cumulative Plots
* NEW FEATURE: Option button to calculate "Days Since" variable
* NEW FEATURE: Multiple tabs of the same type can be open at the same time
* NEW FEATURE: Option button to rename columns, similar to the pre/suffix option
* ADDED: Implement the option to export data as csv without unit row
* ADDED: More heatmap plot options, colormaps can now be changed in the GUI
* ADDED: Heatmap special button to export heatmap plots without axes to be used in blender as heightmap and texture
* CHANGED: Default file extension for Amp csv files (2 header rows with variables and units) is now *.amp.csv
* REMOVED: Export options for compressed files is now deactivated (for the moment)
* IMPROVED CODE: each heatmap tab is now its own instance, running independently from other instances
* IMPROVED CODE: Instead of returning many variables, the variables inside ModBoxes QualityControl, OutlierDetection, Export, Plots and GapFilling are now directly accessed as instance variables.
* FIXED: running median control buttons: cannot add markers to time series



## v0.9.0 | 14 Nov 2019

### ustar Extended

* NEW FEATURE: ustar auto detection was improved and now outputs two thresholds: overall and per season (seasons are currently based on months).
* NEW FEATURE: Included logic to assign seasons to dataset, e.g. four 3-month seasons with the first month being December. This feature is currently only used during the 
auto ustar detection.
* NEW FEATURE: Added new option to add prefix/suffix to variable name/units.
* IMPROVED: Improved and simplified code structure for the ustar auto detection.
* IMPROVED: general ustar check and refinements
* IMPROVED: Instead of returning many variables, the variables inside ModBox Analyses and ModBox Modifications are now directly accessed as instance variables. 



## v0.8.0 | 1 Nov 2019

### MDS Update

* ADDED: During MDS gap-filling, 2 additional plots are shown that give more info about the process.
* CHANGED: The data basis for MDS gap-filling is now instanced data
* IMPROVED: In the code, MDS variables are now accessed as instance variables. This will also be implemented for all other classes.
* IMPROVED: Fonts sizes on different screen resolutions should work a bit better now, but still work to do.
* IMPROVED: Colors in the MDS plots were adjusted to work better with the Light Theme.
* IMPROVED: Drop-down lists now show more variables. How nice.
* IMPROVED: MDS implementation was checked and refined, where needed.
* TESTED: MDS gap-filling was tested with CH-OE2 flux and meteo data. It went well.
* FIXED: 3 minor bugs



## v0.7.0 | 25 Oct 2019

### Pipelines Preparations

* NEW FEATURE: implemented first version of "Pipelines", a category that allows the combination of e.g. several QC flags into one single flag
* ADDED: LogBox now shows info during data file(s) import
* ADDED: info text on exported plots
* IMPROVED: better implementation of color palettes
* CHANGED: date selection now allows all dates, i.e. also dates that are not found in the data, necessary so that editing works as expected
* FIXED: 3 bugs



## v0.6.0 | 13 Oct 2019

**New Features**:

- **Pipelines:** added new category, used to automate and chain-process e.g. combinations of quality flags & checks etc… 
- **Modifications:LimitTimeRange:** limit all variables to selected time period, i.e. constrain time period
- **Modifications:CreateEvent:** Events can now be created within the GUI, and their range can be defined
- INACTIVE ~~**Export:ProfileReport:** added option to output stats to a HTML file using pandas_profiling~~

**Improvements & Changes**:

- Moved call to markers in respective separate methods in ``main.py``
- Global variables (variable identifiers) are now imported with import * (and without the need of a sub-function).
- added first version of Amp icon
- **Analyses:BeforeAfterEvent:** before / after time periods are now shown as broken horizontal bar plot, which is just a fancy word for colored background
- **Code:** adjusted code structure to make it compatible w/ fbs for creating standalone executable files for app sharing.
- **Code:** added local version control (git)
- **Code:** created new .yml and requirements files for (updated) environment AMP06
- **Code:** changed how data_df is created, now separated into multiple methods
- **Code/GUI:** all icons are now imported in code (.setIcon), i.e. no longer via CSS file, required for ``fbs`` GUI programming to show button icons
- **Code/GUI:** improved Light Theme
- **Plots/GUI:** implementation of central color management to change plot appearance
- **Plots:Overview:** improved the way dates are shown on the x-axis
- **stats:LinearRegression**: changed dependency from to ``import statsmodels.api as sm`` to ``import statsmodels.formula.api as smf``. This is compatible to creating fbs for creating a standalone executable file, e.g. exe on Windows.
- **GapFilling:LinearRegressionSimple:** improved shown results, there is now also a short logging text output to logbox
- importing Events from csv file is deactivated for now
- output folder is now default one folder up from the file (main.py, .exe, …) the app was started from
- INACTIVE: ~~new conda environment AMP06, contains pandas_profiling~~

**Bugs & Issues**:

- FIXED Events files caused crash on import due to freq

- FIXED **Analyses:BeforeAfterEvent:** problems with text that disappears when plotting

- FIXED **Analyses:BeforeAfterEvent:**  crashes when there is no before or after time period

- FIXED One folder up using ``.parents[1]`` doesnt work in standalone exe. For now I simplified the output folder part: ny default, the folder ``OUTPUT`` is created in the same folder as the folder from which the script was  started. It seems that this way also avoids permission errors when trying to create the subfolder for a a specific run output.

  does not work: ``dir_out_main = dir_start.parents[1] / 'OUTPUT' ``
  does work: ``dir_out_main = dir_start.parent / 'OUTPUT' ``



## v0.5.1-alpha | 26 Sep 2019

**Changes**:

- Implemented Light Theme as default
- Renamed class ``FluxBay`` to ``Amp``
- Improved code clarity when adding ModBoxes to GUI

**Bugs & Issues**:

- High-resolution files can now correctly be imported, but are downsampled to 1S if necessary



## v0.5.0-indev | 19 Sep 2019

**New Features**:

- **Analyses:Aggregator:** first version that can count occurrences and display additional vars next to it
- **Analyses:Aggregator:** added overall mean as reference period w/ deviations from reference plotted w/ bar plots
- **Analyses:ClassFinder:** now shows number of marked values
- **Analyses:NetGHGBudget:** first version for full CO2-eq budget co2 + n2o + ch4
- **Export**:**SaveFigure**: added option for basic export of overview plots on transparent background
- **Export:VariablesToFiles:** added option to export per year / month
- **Export:VariablesToFiles:** added option for resampling of output file(s)
- **FileTypes**: added template for TOA5 10min time resolution files
- **FileTypes**: added template for custom meteo 10min time resolution files
- **FileTypes:** added support for Amp CSV, e.g. ETH Level-2 output files (timestamp w/o seconds), basically a single uncompressed Amp CSV data file
- **I/O:** Data can now be loaded from multiple files, i.e. multiple files can now be selected in the file dialog. File data are automatically merged.
- **I/O:** Completely changed the way file formats and timestamp formats are loaded. All formats are now in their own respective class.
- **I/O:** implement 13_meteo_nabel (CH-DAV) file format as NABEL_1T
- **I/O:** when reading in files, it now checks for file type instead of timestamp type and reads the file accordingly to the file type
- **Logger:** added logging functionality, log is shown in GUI textbox
- **Modifications:CreateNewColumn:** added option to apply gain to focus column and insert as new column, in addition the units of the new column can be defined
- **Modifications:RemoveVar:** added option to remove selected var
- **Plots:Scatter:** now showing equally-sized bins, also implemented simple linear fit using the bins
- **Plot:Scatter:** implemented fit per dataset, year and month
- **Plot:Hexbins:** added new plot type

**Changes**:

- Folder *example_data* is now outside scripts folder and its path is now an instance variable
- Target frequency is now set whenever intial data are loaded, based on ‘freq’ in the file settings. Initial data are either data that are loaded automatically when the app starts, or data loaded by the user. If data from another file are later *added* to this initial data, these incoming data are resampled to the target frequency. (kudos SG)
- Renamed ModBox Resampling to Conversions
- Custom variables are now marked with a ```.``` at the beginning instead of the underscore
- Removed focus_df info section that was at the windows bottom
- **Export:VariablesToFiles:** Added suffix for aggregation and freq to file names, also added prefix for Year or Month
- **Export:VariablesToFiles:** export to files now uses shifted timestamps for year / month files, necessary because the file timestamp gives the *start* of the averaging period
- Instead of ``` requirements.txt```, the environment is now saved in the conda-specific ``environment.yml`` file.
- Changed name of ModBox ``Conversions`` to ``Modifications``
- Focus df is now automatically set after data loading by automatically selecting first var in loaded variable list on init
- **Plot:Scatter:** added zero line, added white border around bin points
- implemented fallback option in case the seconds are missing in the timestamp of AmpCSV_30T files
- Options that need the creation of a new tab are now called via their own method ``make_tab``
- Re-structured loading of example files w/ own method
- Re-structured GUI.py for clarity

**Bugs & Issues**:

- ~~for pandas .read_csv, check for the NotImplementedError when reading in EddyPro data files in which the ‘date’ and ‘time’ columns contain the string ‘not enough data’, I think can be solved w/ try / except date parser function~~ Error occurred b/c there were more data columns than header column names in the EddyPro 7.0.4 full_output file.
- Fixed an error during file merging: merging of EP full_output files generated an error b/c the time resolution of the files was not contained when trying to add data from another file. (kudos SG)
- **Plots:Scatter:** removed bug in scatter plot, occurred when after binning the number of values for x and y data was not equal, therefore raising an exception and showing an empty or not updated plot, .dropna() on the df solved the issue
- Fixed an error that caused a crash whenever the incoming focus_col was already in the df. This can happen when e.g. col1 is modified and added as .col1 to the df, but .col1 was already in the df e.g. from a previous analysis. Now .col1 is automatically renamed to ..col1 (two points)
- **Export:VariablesToFiles:** Fixed crash when trying to export in original time resolution
- BUG: removed error when merging DAV files, occurred during merging of files where the timestamp columns had the same format but differently given units in the unit section, row 2
- BUG: fixed wrong delimiter when loading .amp files
- BUG: fixed when adding n2o_flux filled data col name is .gf_filledmarker



## v0.4.0-indev | 8 Aug 2019

**Changes**:

- **Analyses**:**BeforeAfterEvents**: basic implementation of before / after stats
- **Analyses**:**GapFinder**: implement first version
- **Analyses**:**RunningLinReg**: moving regression in time windows
- **Analyses**:**uStarDetection**: generate ustar flag that can be used for variables df
- **Analyses**:**uStarDetection**: implemented Papale et al. (2006)
- **Corrections**:**StorageCorrection**: implemented storage correction
- **Controls**: add remove/keep marker points instead of the Apply button
- **Controls**: add Keep Marked to control buttons
- **Code Structure**: restructure ModBoxes
- **Export**: export modified df to file
- **Export**: generate to save to file format (*.amp) that can be directly used in the app 
- **Export**:**ToSeparateFiles**: data can now be exported to separate files for each month and year
- **FileTypes**: import events
- **FileTypes**: rename management to events and add .events extension
- **FileTypes**: read FLUXNET files
- **FileTypes**: add support for TOA5 filetypes, template added for 1min time resolution files
- **FileTypes**: resample on import to target frequency (the one from the main file, e.g. 30mins for EC data) when adding file
- **GapFilling**: add more options for rolling gf, e.g. mean
- **GapFilling**:**MDS**: implemented Reichstein et al (2005)
- **GapFilling**:**Linear Interpolation:** reworked logic to fill gaps of varying lengths
- **General:** created requirements.txt file for creation of conda environment to run on other computers
- **GUI**: implement search field to filter shown variables in variable list
- **GUI**: implement tabs
- **GUI**: make tabs closable and better hover effects
- **GUI**: add category button icons
- **GUI**: add icons for add / remove data buttons
- **OutlierDetection**:**Double-Differenced Time Series**: Papale et al. (2006)
- **Plots**: add plot navigation controls in new tab of scatter plot
- **Plots**: remove overview plotting option
- **Plots:Scatter**: in scatter plot z as color
- **Plots:Scatter**: in scatter normalize with percentiles
- **Plots:Scatter**: move scatter plot to tabs w/ controls
- **Plots:Scatter**: format empty scatter plot in new tab
- **Plots:Heatmap**: move heatmap percentile plots to tabs
- **Plots**:**Overview**: daily, weekly and monthly averages now use shifted timestamp index for correct assignment of data to days, weeks and months
- **Plots**:**Threshold Plots**: FLUXNET plots of different NEE etc. versions
- **Plots**:**Threshold Plots**: move threshold plot to tabs w/ controls
- **Plots**:**Threshold Plots**: threshold plot years all in one plot, cumulative data starting per year
- **QualityControl**: get eddypro flags to run
- **QualityControl**: integration (z-d)/L w/ manual limits (-2, 2), see ICOS pubs

**Bugs & Issues**:

- **General**:*CHECK*: are all dropdown lists updated after adding custom variable
- **Plots**:*ISSUE*: overview plots are sometimes deleted when clicking scatter
- **Plots**:**Scatter**:*BUG*: white background in scatter plot
- **Plots**:**Scatter**:*BUG* display z as colors et al not working
- **Plots**:**Scatter**:*BUG* update dropdowns in scatter tab after new variable was added, checked but seems to work
- **inout**:**DataFunctions**: check merging b/w high-res meteo and 30-min EC data, worked

